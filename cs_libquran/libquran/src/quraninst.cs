using System;
using System.IO;
using LibQuran.Sura;
using LibQuran.XMLParse;
using LibQuran.StaticData;

namespace LibQuran.Instance {
   public class QuranInst {
      private string filename;
      public string errmsg;
      public string translator;
      private SuraInst[] suralist;

      public bool init(string path){
         if (!File.Exists(path)){
            errmsg = "could not find the file: " + path;
            return false;
         }

         suralist = new SuraInst[114];
         QuranXML qxml = new QuranXML(this, path);
         qxml.parse();
         return true;
      }

      public void addSura(int suranum, string suraname){
         suralist[suranum-1] = new SuraInst(suranum, suraname);
      }
      
      public void addAyah(int suranum, int ayahnum, string ayahtext){
         suralist[suranum-1].addAyah(ayahnum, ayahtext);
      }

      public void addSearchText(int suranum, int ayahnum, string stext){
         suralist[suranum-1].addSearchText(ayahnum, stext);
      }

      public string getSuraName(int suranum){
         if (validSuraArg(suranum))
            return suralist[suranum-1].suraname;
         else return null;
      }

      public int getNumAyahs(int suranum){
         if (validSuraArg(suranum))
            return QuranData.NUMAYAHS[suranum-1];
         else return -1;
      }

      public string getAyah(int suranum, int ayahnum){
         if (validArgs(suranum, ayahnum))
            return suralist[suranum-1].getAyah(ayahnum);
         else return null;
      }
      
      public string getSearchText(int suranum, int ayahnum){
         if (validArgs(suranum, ayahnum))
            return suralist[suranum-1].getSearchText(ayahnum);
         else return null;
      }

      public int getLocationJuz(int suranum, int ayahnum){
         if (!validArgs(suranum, ayahnum))
            return -1;
         for (int i=0; i<QuranData.HEZBQUAD.Length/2; i+=8){
            if ((QuranData.HEZBQUAD[i+7,0] > suranum) || 
                ((QuranData.HEZBQUAD[i+7,0] == suranum) && 
                 (QuranData.HEZBQUAD[i+7,1] >= ayahnum)))
               return (i/8)+1;
         }
         return -1;
      }

      public bool isSajdah(int suranum, int ayahnum){
         if (!validArgs(suranum, ayahnum))
            return false;
         for (int i=0; i<QuranData.SAJDAHS.Length/2; i++)
            if ((QuranData.SAJDAHS[i,0]==suranum) &&
                  (QuranData.SAJDAHS[i,1]==ayahnum))
               return true;
         return false;
      }

      public bool hasSajdah(int suranum){
         if (!validSuraArg(suranum))
            return false;
         for (int i=0; i<QuranData.SAJDAHS.Length/2; i++)
            if (QuranData.SAJDAHS[i,0]==suranum)
               return true;
         return false;
      }

      public int[] getSajdahs(int suranum){
         int[] ret;
         if (!hasSajdah(suranum))
            return null;
         if (suranum == 22)
            ret = new int[2];
         else ret = new int[1];
         for (int i=0; i<QuranData.SAJDAHS.Length/2; i++){
            if (QuranData.SAJDAHS[i,0]==suranum){
               ret[0] = QuranData.SAJDAHS[i,1];
               if (suranum == 22)
                  ret[1] = QuranData.SAJDAHS[i+1,1];
               return ret;
            }
         }
         return null;
      }

      public bool validSuraArg(int suranum){
         errmsg = "";

         if ((suranum > 114) || (suranum < 1)){
            errmsg = "invalid surah number!";
            return false;
         }
         return true;
      }

      public bool validArgs(int suranum, int ayahnum){
         if (!validSuraArg(suranum))
            return false;
         else if ((QuranData.NUMAYAHS[suranum-1] < ayahnum) || (ayahnum < 1)){
            errmsg = "invalid ayah number!  ayah numbers for this " +
               "sura range from 1 to " + QuranData.NUMAYAHS[suranum-1];
            return false;
         }
         else return true;
      }
   }
}
