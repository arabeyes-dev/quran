using System;
using System.IO;

using LibQuran;
using LibQuran.Instance;

public class tester {
   public static void Main(string[] argv){
      QuranInfo qi = new QuranInfo();
      if (argv.Length != 1){
         Console.WriteLine("requires an argument of directory " +
                           "containing the .quranrc file.");
         return;
      }
      
      if (!qi.setDataFile(argv[0] + Path.DirectorySeparatorChar + 
               ".quranrc")){
         Console.WriteLine("error! " + qi.getError());
         return;
      }
      qi.init();
      string[] t = null;
      string[] t1 = null;
      Console.WriteLine("language codes and names");
      t = qi.getLanguageCodes();
      t1 = qi.getLanguageNames();
      for (int c=0; c<t.Length; c++)
         Console.WriteLine(t1[c] + " (" + t[c] + ")");
      Console.WriteLine("\navailable texts under english:");
      t = qi.getLanguageTexts("en");
      for (int c=0; c<t.Length; c++)
         Console.WriteLine("\t" + t[c]);
      QuranInst eq = null;
      if (t == null){ Console.WriteLine("no english translations found!"); }
      else eq = qi.openText("en", t[0]);
      if (eq == null)
         Console.WriteLine("error opening english translation: {0}",
               eq.errmsg);
      else {
         Console.WriteLine("\nsuccesfully opened english translation!");
         Console.WriteLine("running some tests...");
         Console.WriteLine("{0} - {1}", eq.getSuraName(1), eq.getNumAyahs(1));
         for (int i=1; i<= eq.getNumAyahs(1); i++)
            Console.WriteLine("{0}: {1}", i, eq.getAyah(1, i));
         Console.WriteLine("");
         Console.WriteLine("juz number of some surahs:");
         Console.WriteLine("last ayah of surat al-nas: {0}",
                            eq.getLocationJuz(114, 6));
         Console.WriteLine("first ayah of surat al-fatiha: {0}",
                            eq.getLocationJuz(1, 1));
         Console.WriteLine("");
         Console.WriteLine("some sajdah information:");
         Console.WriteLine("sura 96 has a sajdah: {0}",
                            eq.hasSajdah(96));
         Console.WriteLine("sura 10 has a sajdah: {0}",
                            eq.hasSajdah(10));
         Console.WriteLine("sura 96 ayah 3 is a sajdah: {0}",
                            eq.isSajdah(96, 3));
         Console.WriteLine("sura 96 ayah 19 is a sajdah: {0}",
                            eq.isSajdah(96, 19));
         int[] tmp = eq.getSajdahs(10);
         Console.Write("sajdahs in surah 10: ");
         if (tmp == null) Console.WriteLine("none found..");
         else Console.WriteLine("error!  this shouldn't print!\n");
         tmp = eq.getSajdahs(27);
         Console.Write("sajdahs in surah 27: ");
         if ((tmp == null) || (tmp.Length != 1))
            Console.WriteLine("error!  this shouldn't print!\n");
         else Console.WriteLine(tmp[0]);
      }
      Console.WriteLine("\n--\n");
      QuranInst tl = null;
      t = qi.getLanguageTexts("tl");
      if (t == null) Console.WriteLine("no transliteration texts found!");
      else tl = qi.openText("tl", t[0]);
      if (tl == null) Console.WriteLine("warning, unable to open " +
            "transliteration..." + tl.errmsg);
      else {
         Console.WriteLine("successfully opened transliteration data!");
         Console.WriteLine("running some tests...");
         Console.WriteLine("{0} - {1}", tl.getSuraName(114),
                                        tl.getNumAyahs(114));
         for (int i=1; i<=tl.getNumAyahs(114); i++)
            Console.WriteLine("{0} {1}", i, tl.getAyah(114, i));

         Console.WriteLine("");
         Console.WriteLine("juz number of some surahs:");
         Console.WriteLine("ayah 141 of surat al-baqarah: {0}",
                            tl.getLocationJuz(2, 141));
         Console.WriteLine("ayah 142 of surat al-baqarah: {0}",
                            tl.getLocationJuz(2, 142));
         Console.WriteLine("");
         Console.WriteLine("some sajdah information:");
         Console.WriteLine("sura 13 has a sajdah: {0}",
                            tl.hasSajdah(13));
         Console.WriteLine("surah 13 ayah 1 is a sajdah: {0}",
                            tl.isSajdah(13, 1));
         Console.WriteLine("surah 13 ayah 15 is a sajdah: {0}",
                            tl.isSajdah(13, 15));
         int[] tmp = tl.getSajdahs(15);
         Console.Write("sajdahs in surah 15: ");
         if (tmp == null) Console.WriteLine("none found..");
         else Console.WriteLine("error!  this shouldn't print!\n");
         tmp = tl.getSajdahs(22);
         Console.Write("sajdahs in surah 22: ");
         if ((tmp == null) || (tmp.Length != 2))
            Console.WriteLine("error!  this shouldn't print!\n");
         else Console.WriteLine("{0} and {1}", tmp[0], tmp[1]);
      }
   }
}
