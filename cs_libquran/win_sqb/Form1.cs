using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using LibQuran;
using LibQuran.Instance;

namespace sqb
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class SQBForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private QuranInst aq;
		private int ayah, sura;
		private bool err;
		private System.Windows.Forms.Button prevButton;
		private System.Windows.Forms.Button nextButton;
		private System.Windows.Forms.GroupBox frameBox;
		private System.Windows.Forms.RichTextBox textBox;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SQBForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			ayah = 1;
			sura = 1;

			QuranInfo qi = new QuranInfo();
			if (!qi.setDataFile("." + Path.DirectorySeparatorChar + ".quranrc"))
			{
				Console.WriteLine("error! " + qi.getError());
				err = true;
			}
			
			aq = null;
			if (!qi.init())
				err = true;
			else 
			{
				aq = qi.openText("ar", "quran.ar.xml");
				if (aq == null) err = true;
				else err = false;
			}
			jumpTo(sura, ayah);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.prevButton = new System.Windows.Forms.Button();
			this.nextButton = new System.Windows.Forms.Button();
			this.frameBox = new System.Windows.Forms.GroupBox();
			this.textBox = new System.Windows.Forms.RichTextBox();
			this.frameBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem3});
			this.menuItem1.Text = "File";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "Jump To";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "Exit";
			this.menuItem3.Click += new System.EventHandler(this.exit_Clicked);
			// 
			// prevButton
			// 
			this.prevButton.Location = new System.Drawing.Point(0, 256);
			this.prevButton.Name = "prevButton";
			this.prevButton.Size = new System.Drawing.Size(152, 23);
			this.prevButton.TabIndex = 0;
			this.prevButton.Text = "Previous";
			this.prevButton.Click += new System.EventHandler(this.prev_Click);
			// 
			// nextButton
			// 
			this.nextButton.Location = new System.Drawing.Point(152, 256);
			this.nextButton.Name = "nextButton";
			this.nextButton.Size = new System.Drawing.Size(144, 23);
			this.nextButton.TabIndex = 1;
			this.nextButton.Text = "Next";
			this.nextButton.Click += new System.EventHandler(this.next_Click);
			// 
			// frameBox
			// 
			this.frameBox.Controls.AddRange(new System.Windows.Forms.Control[] {
																				   this.textBox});
			this.frameBox.Name = "frameBox";
			this.frameBox.Size = new System.Drawing.Size(296, 248);
			this.frameBox.TabIndex = 2;
			this.frameBox.TabStop = false;
			this.frameBox.Text = "Quran";
			// 
			// textBox
			// 
			this.textBox.AutoSize = true;
			this.textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.textBox.Location = new System.Drawing.Point(8, 16);
			this.textBox.Name = "textBox";
			this.textBox.ReadOnly = true;
			this.textBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.textBox.Size = new System.Drawing.Size(280, 232);
			this.textBox.TabIndex = 0;
			this.textBox.Text = "";
			// 
			// SQBForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(296, 281);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.frameBox,
																		  this.nextButton,
																		  this.prevButton});
			this.MaximumSize = new System.Drawing.Size(304, 328);
			this.Menu = this.mainMenu1;
			this.MinimumSize = new System.Drawing.Size(304, 328);
			this.Name = "SQBForm";
			this.Text = "Simple Quran Browser";
			this.frameBox.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new SQBForm());
		}

		public void jumpTo(int sura, int ayah)
		{
			if (!err)
			{
				if ((sura < 1) || (sura > 114))
					return;
				if (ayah > aq.getNumAyahs(sura))
					return;

				this.sura = sura;
				this.ayah = ayah;
				textBox.Text = aq.getAyah(sura, ayah);
		
				frameBox.Text = "Sura: " + sura + " (" + aq.getSuraName(sura) +
					") Ayah: " + ayah + "     Juz: " + 
					aq.getLocationJuz(sura, ayah) + 
					(aq.isSajdah(sura, ayah)? "     Sajda" : "");
			}
			else textBox.Text = "error... could not find .quranrc or arabic xml file";
		}

		private void next_Click(object sender, System.EventArgs e)
		{
			if (err) return;
			ayah++;
			if (ayah > aq.getNumAyahs(sura))
			{
				if (sura == 114) sura = 1;
				else sura++;

				ayah = 1;
			}
			jumpTo(sura, ayah);
		}

		private void prev_Click(object sender, System.EventArgs e)
		{
			if (err) return;
			ayah--;
			if (ayah == 0)
			{
				if (sura == 1) sura = 114;
				else sura--;
				ayah = aq.getNumAyahs(sura);
			}
			jumpTo(sura, ayah);
		}

		private void exit_Clicked(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			new JumpDialog(this);
		}
	}
}
