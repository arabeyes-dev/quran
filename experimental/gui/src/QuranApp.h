/***************************************************************************
$Id$
                          qtquran.h  -  declaration for QuranApp, audioThread
                             -------------------
    begin                : Fri Jul 12 15:53:41 EEST 2002
    copyright            :  (C) Copyright 2004, Arabeyes, Mohammed Yousif
    email                : mhdyousif@gmx.net

$Date$
$Author$
$Revision$
$Source$ 
 ***************************************************************************/

/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify                              *
 *   it under the terms of the GNU General Public License as published by                           *
 *   the Free Software Foundation; either version 2 of the License, or                                  *
 *   (at your option) any later version.                                                                              *
 *                                                                                                                                *
 ***************************************************************************/

#ifndef QURANAPP_H
#define QURANAPP_H

//#include <qvariant.h>
//#include <qmainwindow.h>
//#include <quran.h>
//#include <qwidget.h>
//#include <qvaluelist.h>
//#include <qpixmap.h>
#include <quran.h>
// application specific includes
//#include "audiothread.h"
#include "MainWindow.h"

#define VERSION	"0.3"

class QuranPaper;

/**
  * This Class is the base class for your application. It sets up the main
  * window and providing a menubar, toolbar
  * and statusbar. For the main view, an instance of class QuranView is
  * created which creates your view.
  */
class QuranApp : public MainWindow {
    Q_OBJECT

public:
    QuranApp(QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    virtual ~QuranApp();
    static QuranApp *getInstance();
    
public slots:
    void onAbout();
    void onSearch();
    void onExit();
    void onNextPage();
    void onNextChapter();
    void onPrevPage();
    void onPrevChapter();
    void onRecite();
    void onExplain();
    void onTranslate();
    void onHelp();
    void onLicense();
    void onGoTo();
    void onIndex();
    void onOptions();
    void onSearchNext();
    void onCancel();
    void onDoSearch();
    void onDoExplain();
    void onDoTranslate();
    void onDoGoTo();
    void onDoRecite();
    void onOptionChanged();
    void onSelection();
    
private:
    void handleFrame(QFrame *frame);
    void QuranApp::loadQuranData(int lang, int file);
    
    QFrame *showedFrame;
    libquran_ctx m_libquran;
    quran *m_qurandata;
    static QuranApp *instance;
    QuranPaper *m_quranPaper;

protected:
    void showEvent( QShowEvent *e );
    void mouseReleaseEvent( QMouseEvent *e );
};

#endif // QURANAPP_H
