#include <qlabel.h>
#include <qtextbrowser.h>
#include <qtextcodec.h>

#include "QuranPaper.h"
#include "QuranApp.h"

QuranPaper::QuranPaper() : QObject(),
    m_chapterName(""),
    m_partNumber(0),
    m_hezbNumber(0),
    m_hezbQuad(0),
    m_quranApp(QuranApp::getInstance()),
    m_selectedParagraph(0), 
    m_amountParagraphs(0) {
    
    // TODO: Test this with arabic interface
  if ( ((QString)QTextCodec::locale()).startsWith("ar")) {
    //for RTL languages, mirror the UI
    //Notice that mirroring the UI will also reverse the order of
    //all images, thus they have to be reversed (reversing the reverse :-)

    QPixmap tempPixmap;
    tempPixmap = *m_quranApp->topleftcorner->pixmap();
    m_quranApp->topleftcorner->setPixmap(*m_quranApp->toprightcorner->pixmap());
    m_quranApp->toprightcorner->setPixmap(tempPixmap);
    tempPixmap = *m_quranApp->topleftcorner2->pixmap();
    m_quranApp->topleftcorner2->setPixmap(*m_quranApp->toprightcorner2->pixmap());
    m_quranApp->toprightcorner2->setPixmap(tempPixmap);
    
    tempPixmap = *m_quranApp->bottomleftcorner->pixmap();
    m_quranApp->bottomleftcorner->setPixmap(*m_quranApp->bottomrightcorner->pixmap());
    m_quranApp->bottomrightcorner->setPixmap(tempPixmap);
    tempPixmap = *m_quranApp->bottomleftcorner2->pixmap();
    m_quranApp->bottomleftcorner2->setPixmap(*m_quranApp->bottomrightcorner2->pixmap());
    m_quranApp->bottomrightcorner2->setPixmap(tempPixmap);
    
    tempPixmap = *m_quranApp->leftframe->paletteBackgroundPixmap();
    m_quranApp->leftframe->setPaletteBackgroundPixmap(*m_quranApp->rightframe->paletteBackgroundPixmap());
    m_quranApp->rightframe->setPaletteBackgroundPixmap(tempPixmap);
    
    tempPixmap = *m_quranApp->btnPrevPage->pixmap();
    m_quranApp->btnPrevPage->setPixmap(*m_quranApp->btnNextPage->pixmap());
    m_quranApp->btnNextPage->setPixmap(tempPixmap);
    tempPixmap = *m_quranApp->btnPrevChapter->pixmap();
    m_quranApp->btnPrevChapter->setPixmap(*m_quranApp->btnNextChapter->pixmap());
    m_quranApp->btnNextChapter->setPixmap(tempPixmap);
    
    qApp->setReverseLayout(TRUE);
    }
  
  //use the font 'ArabeyesQr' for Arabic
  QFont tbQuranText_font(  m_quranApp->tbQuranText->font() );
  tbQuranText_font.setFamily( "ArabeyesQr" );
  tbQuranText_font.setPointSize( 22 );
//  if (ctx->libquran.avail_langs[cblang->currentItem()].code != (QString)"ar")  { //if not arabic, then use the default font
//    tbQuranText_font.setFamily( QFont().family());
//    tbQuranText_font.setPointSize( 13 );
//  }
  m_quranApp->tbQuranText->setFont( tbQuranText_font );

}

QuranPaper::~QuranPaper() {
}

void QuranPaper::setChapterName(QString chapterName) {    
    m_chapterName=chapterName;
    m_quranApp->lblChapterName->setText(tr("Chapter: ") + m_chapterName);
}

QString QuranPaper::getChapterName() {
    return m_chapterName;    
}

void QuranPaper::setPartNumber(int partNumber) {
    m_quranApp->lblPartNr->setText(tr("Part: ") + QString::number(partNumber));
    m_partNumber=partNumber;
}

int QuranPaper::getPartNumber() {
    return m_partNumber;
}

void QuranPaper::setHezbNumber(int number, int quad) {
    m_hezbNumber=number;
    m_hezbQuad=quad;
}

int QuranPaper::getHezbNumber() {
    return m_hezbNumber;
}

int QuranPaper::getHezbQuad() {
    return m_hezbQuad;
}

void QuranPaper::goToNextPage() {   
    // Scroll to the next page
    m_quranApp->tbQuranText->scrollBy(0,m_quranApp->tbQuranText->height());
    
    // Find the first paragraph visible on screen
    int paragraph=findParagraphBackward(m_quranApp->tbQuranText->contentsY()-1 , m_quranApp->tbQuranText->contentsY()-100);
    paragraph++;
    
    // Select the first paragraph visible on screen
    highlightParagraph(paragraph);
    m_selectedParagraph=paragraph;
}

void QuranPaper::goToPreviousPage() {
    // Scroll to the previous page
    m_quranApp->tbQuranText->scrollBy(0,0-m_quranApp->tbQuranText->height());
    
    // Find the first paragraph visible on screen
    int paragraph=0;
    if (m_quranApp->tbQuranText->contentsY()>0) {
	paragraph=findParagraphBackward(m_quranApp->tbQuranText->contentsY()-1 , m_quranApp->tbQuranText->contentsY()-100);
	paragraph++;
    }
    
    // Select the first paragraph visible on screen
    highlightParagraph(paragraph);
    m_selectedParagraph=paragraph;
}

void QuranPaper::goToParagraph(int number) {
    // Check the boundaries
    m_selectedParagraph=number;
    if (m_selectedParagraph>=m_amountParagraphs) m_selectedParagraph=m_amountParagraphs-1;
    if (m_selectedParagraph<0) m_selectedParagraph=0;
       
    // Scroll to the paragraph
    m_quranApp->tbQuranText->scrollToAnchor(QString::number(m_selectedParagraph));
    // Select the paragraph
    highlightParagraph(m_selectedParagraph);    
 
}

void QuranPaper::goToNextParagraph() {
    // Go to next paragraph
    m_selectedParagraph++;
    if (m_selectedParagraph>=m_amountParagraphs) m_selectedParagraph--;

    // View and select next paragraph
    ensureParagraphVisibility(m_selectedParagraph);
    highlightParagraph(m_selectedParagraph);    
}

void QuranPaper::highlightParagraph(int number) {
    
    QString text = m_quranApp->tbQuranText->text();
    
    // Dehighlight the old paragraph
    text.remove("<font color=\"#ff0000\">");
    text.remove("</font>");
    
    // Highlight the new paragraph
    QString anchor="<a name=\"" + QString::number(number) + "\">";
    int beginIndex = text.find(anchor) + QString(anchor).length();
    text.insert(beginIndex, "<font color=\"#ff0000\">");
    int endIndex = text.find("</a>", beginIndex);
    text.insert(endIndex, "</font>");
    m_quranApp->tbQuranText->setText(text);
}

void QuranPaper::setText(QString text, int amountParagraphs) {
    m_quranApp->tbQuranText->setText(text);
    m_amountParagraphs=amountParagraphs;
    m_selectedParagraph=0;
}

QString QuranPaper::getText() {    
    return m_quranApp->tbQuranText->text();
}

int QuranPaper::findParagraphForward( int begin, int end) {
   
    int width=m_quranApp->tbQuranText->visibleWidth();
    
    // Prohibit searching unexisting area
    if (begin<0) begin=0;  
    if (end>m_quranApp->tbQuranText->contentsHeight()) end=m_quranApp->tbQuranText->contentsHeight();
 
    // Search for the paragraph
    QPoint point(0, begin);
    bool ok;
    for (; point.y()<end; point.ry()++) {
	point.setX(0);
	for (;  point.x()<width; point.rx()++) {
	    if (m_quranApp->tbQuranText->anchorAt(point, Qt::AnchorName)!=QString::null) {
		return m_quranApp->tbQuranText->anchorAt(point, Qt::AnchorName).toInt( &ok, 10 );
	    }
	}
    }
    
    return -1;
}

int QuranPaper::findParagraphBackward( int begin, int end) {
    
    int width=m_quranApp->tbQuranText->visibleWidth();
    
    // Prohibit searching unexisting area
    if (end<0) end=0;
    if (begin>m_quranApp->tbQuranText->contentsHeight ()) begin=m_quranApp->tbQuranText->contentsHeight ();
    
    // Search for the paragraph
    QPoint point(width-1, begin);
    bool ok;
    for (; point.y()>end; point.ry()--) {
	point.setX(width-1);
	for (;  point.x()>=0; point.rx()--) {
	    if (m_quranApp->tbQuranText->anchorAt(point, Qt::AnchorName)!=QString::null) {
		return m_quranApp->tbQuranText->anchorAt(point, Qt::AnchorName).toInt( &ok, 10 );
	    }
	}
    }
    
    return -1;
}

void QuranPaper::ensureParagraphVisibility(int paragraph) {
    
    int width=m_quranApp->tbQuranText->visibleWidth();
    int moveY=0;
    QPoint point(0, m_quranApp->tbQuranText->contentsY());    
    bool ok;    

    // Find the paragraph
    while ((moveY==0) && (point.ry() < m_quranApp->tbQuranText->contentsHeight()) ) {
	for (;  point.x()<width; point.rx()+=10) {
	    
	    if (m_quranApp->tbQuranText->anchorAt(point, Qt::AnchorName).toInt( &ok, 10 ) > paragraph)  {
		moveY = point.ry() - m_quranApp->tbQuranText->contentsY() - m_quranApp->tbQuranText->height();
		break;
	    }
	}	
	point.ry()+=5;
	point.setX(0);
    }

    // Scroll to the paragraph if it is invisible
    if (moveY>0) {
	m_quranApp->tbQuranText->scrollBy(0,moveY);
    } 
}
