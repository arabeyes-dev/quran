#ifndef QURANPAPER_H
#define QURANPAPER_H

#include <qstring.h>
#include <qobject.h>

class QuranApp;

class QuranPaper : public QObject {
    Q_OBJECT

public:
    QuranPaper();
    virtual ~QuranPaper();
    void setChapterName(QString chapterName);
    QString getChapterName();
    void setPartNumber(int partNumber);
    int getPartNumber();
    void setHezbNumber(int number, int quad);
    int getHezbNumber();
    int getHezbQuad();
    void goToNextPage();
    void goToPreviousPage();
    void goToParagraph(int number);
    void goToNextParagraph();
    void highlightParagraph(int number);
    void setText(QString text, int amountParagraphs);
    QString getText();
    
private:
    int findParagraphForward( int begin, int end);    
    int findParagraphBackward( int begin, int end);
    void ensureParagraphVisibility(int paragraph);
    
private:
    QString m_chapterName;
    int m_partNumber;
    int m_hezbNumber;
    int m_hezbQuad;
    QuranApp *m_quranApp;
    int m_selectedParagraph;
    int m_amountParagraphs;

protected:
};

#endif // QURANPAPER_H
