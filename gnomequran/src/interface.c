/* gnomequran- Holy Quran 
 * (C) 2002 Mohammad DAMT
 *
 * $Id$
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"

static GnomeUIInfo mn_file_menu_uiinfo[] =
{
	GNOMEUIINFO_MENU_EXIT_ITEM (on_mn_exit_activate, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo mn_edit_menu_uiinfo[] =
{
	GNOMEUIINFO_MENU_CUT_ITEM (on_mn_cut_activate, NULL),
	GNOMEUIINFO_MENU_COPY_ITEM (on_mn_copy_activate, NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM (on_mn_paste_activate, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo mn_settings_menu_uiinfo[] =
{
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (on_mn_preferences_activate, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo mn_help_menu_uiinfo[] =
{
	GNOMEUIINFO_HELP ("gnomequran"),
	GNOMEUIINFO_MENU_ABOUT_ITEM (on_mn_about_activate, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo menubar1_uiinfo[] =
{
	GNOMEUIINFO_MENU_FILE_TREE (mn_file_menu_uiinfo),
	GNOMEUIINFO_MENU_EDIT_TREE (mn_edit_menu_uiinfo),
	GNOMEUIINFO_MENU_SETTINGS_TREE (mn_settings_menu_uiinfo),
	GNOMEUIINFO_MENU_HELP_TREE (mn_help_menu_uiinfo),
	GNOMEUIINFO_END
};

GtkWidget*
create_frm_main (void)
{
	GtkWidget *frm_main;
	GtkWidget *dock1;
	GtkWidget *toolbar1;
	GtkWidget *tmp_toolbar_icon;
	GtkWidget *btn_new_quran;
	GtkWidget *btn_close_quran;
	GtkWidget *btn_aya_prev;
	GtkWidget *btn_aya_next;
	GtkWidget *hbox3;
	GtkWidget *label3;
	GtkWidget *aya_no;
	GtkWidget *btn_go;
	GtkWidget *box_aya;
	GtkWidget *status_bar;

	frm_main = gnome_app_new ("GnomeQuran", _("Al Quraan"));
	gtk_object_set_data (GTK_OBJECT (frm_main), "frm_main", frm_main);
	gtk_window_set_policy (GTK_WINDOW (frm_main), TRUE, TRUE, FALSE);

	dock1 = GNOME_APP (frm_main)->dock;
	gtk_widget_ref (dock1);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "dock1", dock1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (dock1);

	gnome_app_create_menus (GNOME_APP (frm_main), menubar1_uiinfo);

	gtk_widget_ref (menubar1_uiinfo[0].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_file",
	                          menubar1_uiinfo[0].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_file_menu_uiinfo[0].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_exit",
	                          mn_file_menu_uiinfo[0].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (menubar1_uiinfo[1].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_edit",
	                          menubar1_uiinfo[1].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_edit_menu_uiinfo[0].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_cut",
	                          mn_edit_menu_uiinfo[0].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_edit_menu_uiinfo[1].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_copy",
	                          mn_edit_menu_uiinfo[1].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_edit_menu_uiinfo[2].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_paste",
	                          mn_edit_menu_uiinfo[2].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (menubar1_uiinfo[2].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_settings",
	                          menubar1_uiinfo[2].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_settings_menu_uiinfo[0].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_preferences",
	                          mn_settings_menu_uiinfo[0].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (menubar1_uiinfo[3].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_help",
	                          menubar1_uiinfo[3].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	gtk_widget_ref (mn_help_menu_uiinfo[1].widget);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "mn_about",
	                          mn_help_menu_uiinfo[1].widget,
	                          (GtkDestroyNotify) gtk_widget_unref);

	toolbar1 = gtk_toolbar_new ();
	gtk_widget_ref (toolbar1);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "toolbar1", toolbar1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (toolbar1);
	gnome_app_add_toolbar (GNOME_APP (frm_main), GTK_TOOLBAR (toolbar1), "toolbar1",
	                              0,
	                              0, 1, 0, 0);
	gtk_container_set_border_width (GTK_CONTAINER (toolbar1), 1);

	btn_new_quran = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar1), GTK_STOCK_OPEN,_("Opens a Quran"),NULL,
				  			G_CALLBACK(on_btn_new_quran_clicked),NULL,-1);
	gtk_widget_ref (btn_new_quran);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_new_quran", btn_new_quran,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_new_quran);

	btn_close_quran = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar1), GTK_STOCK_CLOSE,_("Closes a Quran"),NULL,
				  			G_CALLBACK(on_btn_close_quran_clicked),NULL,-1);
	gtk_widget_ref (btn_close_quran);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_close_quran", btn_close_quran,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_close_quran);
	gtk_widget_set_sensitive (btn_close_quran, FALSE);


	btn_aya_prev = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar1), GTK_STOCK_GO_BACK,_("Back to previous verse"),NULL,
				  			G_CALLBACK(on_btn_aya_prev_clicked),NULL,-1);
	gtk_widget_ref (btn_aya_prev);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_aya_prev", btn_aya_prev,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_aya_prev);
	gtk_widget_set_sensitive (btn_aya_prev, FALSE);

	btn_aya_next = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar1), GTK_STOCK_GO_FORWARD,_("Go to next verse"),NULL,
				  			G_CALLBACK(on_btn_aya_next_clicked),NULL,-1);
	gtk_widget_ref (btn_aya_next);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_aya_next", btn_aya_next,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_aya_next);
	gtk_widget_set_sensitive (btn_aya_next, FALSE);

	hbox3 = gtk_hbox_new (FALSE, 2);
	gtk_widget_ref (hbox3);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "hbox3", hbox3,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox3);
	gnome_app_add_docked (GNOME_APP (frm_main), hbox3, "hbox3",
	                              0,
	                              0, 3, 0, 0);

	label3 = gtk_label_new (_("Go To Aya #:"));
	gtk_widget_ref (label3);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "label3", label3,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label3);
	gtk_box_pack_start (GTK_BOX (hbox3), label3, FALSE, FALSE, 0);

	aya_no = gtk_entry_new_with_max_length (3);
	gtk_entry_set_width_chars(GTK_ENTRY(aya_no),3);
	gtk_widget_ref (aya_no);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "aya_no", aya_no,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (aya_no);
	gtk_box_pack_start (GTK_BOX (hbox3), aya_no, TRUE, TRUE, 0);
	gtk_widget_set_sensitive (aya_no, FALSE);

	btn_go = gtk_button_new_with_label (_("Go"));
	gtk_widget_ref (btn_go);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "btn_go", btn_go,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_go);
	gtk_box_pack_start (GTK_BOX (hbox3), btn_go, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (btn_go, FALSE);


	g_signal_connect (GTK_OBJECT (btn_go), "clicked",
	                    G_CALLBACK(on_btn_go_clicked),
	                    aya_no);
	box_aya = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (box_aya);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "box_aya", box_aya,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (box_aya);
	gnome_app_set_contents (GNOME_APP (frm_main), box_aya);
	gtk_widget_set_size_request(box_aya,50, 100);

	status_bar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
	gtk_widget_ref (status_bar);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "status_bar", status_bar,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (status_bar);
	gnome_app_set_statusbar (GNOME_APP (frm_main), status_bar);

	gnome_app_install_menu_hints (GNOME_APP (frm_main), menubar1_uiinfo);
	return frm_main;
}

GtkWidget*
create_frm_about (void)
{
	const gchar *authors[] = {
	  "Mohammad DAMT",
	  "<mdamt@bisnisweb.com>",
	  NULL
	};

	const gchar *documenters[] = {
	  "Mohammad DAMT",
	  "<mdamt@bisnisweb.com>",
	  NULL
	};
	
	GtkWidget *frm_about;

	if (gtk_object_get_data(GTK_OBJECT(frm_main),"frm_about")) return NULL;

	frm_about = gnome_about_new ("GnomeQuran", VERSION,
	                      _("(c) 2002"),
	                      _("Holy Quran for GNOME. \nhttp://oss.mdamt.net/gnomequran/\nuses libquran library\nXML files provided by Arab Eyes Project http://www.arabeyes.org"),
	                      authors,documenters,_("your name here (translator name)"),
	                      NULL);
	gtk_object_set_data (GTK_OBJECT (frm_main), "frm_about", frm_about);

	return frm_about;
}

GtkWidget*
create_dlg_preferences (void)
{
	GtkWidget *dlg_preferences;
	GtkWidget *dialog_vbox1;
	GtkWidget *frame1;
	GtkWidget *hbox1,*hbox2;
	GtkWidget *dialog_action_area1;
	GtkWidget *btn_ok;
	GtkWidget *btn_apply;
	GtkWidget *btn_cancel;
	GtkWidget *opt_sound, *opt_sound_item, *sound_item;
	GtkWidget *temp;
	GtkWidget *f;
	int i, j=0, build_driver_list, driver_index=0;
	ao_info **driver_list;

	dlg_preferences = gnome_dialog_new (_("Preferences"), NULL);
	gtk_object_set_data (GTK_OBJECT (dlg_preferences), "dlg_preferences", dlg_preferences);
	gtk_window_set_policy (GTK_WINDOW (dlg_preferences), FALSE, FALSE, FALSE);

	dialog_vbox1 = GNOME_DIALOG (dlg_preferences)->vbox;
	gtk_object_set_data (GTK_OBJECT (dlg_preferences), "dialog_vbox1", dialog_vbox1);
	gtk_widget_show (dialog_vbox1);

	frame1 = gtk_frame_new (_("Preferences"));
	gtk_widget_ref (frame1);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "frame1", frame1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (frame1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), frame1, TRUE, TRUE, 0);

	hbox1 = gtk_vbox_new (FALSE, 5);
	gtk_widget_ref (hbox1);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "hbox1", hbox1,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox1);
	gtk_container_add (GTK_CONTAINER (frame1), hbox1);

	
	hbox2 = gtk_hbox_new (FALSE, 5);
	gtk_widget_ref (hbox2);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "hbox2", hbox2,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox2);
	gtk_container_add (GTK_CONTAINER (hbox1), hbox2);

	temp = gtk_label_new(_("Sound Driver:"));
	gtk_widget_ref(temp);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "temp", temp,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (temp);
	gtk_box_pack_start (GTK_BOX(hbox2), temp, FALSE, FALSE, 0);
	opt_sound = gtk_option_menu_new ();
	gtk_widget_ref (opt_sound);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "opt_sound", opt_sound,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_sound);
	gtk_box_pack_start (GTK_BOX(hbox2), opt_sound, FALSE, FALSE, 0);
	opt_sound_item = gtk_menu_new ();

	driver_list = (ao_info**)ao_driver_info_list(&num_driver);
	if (g_list_length(driver_short_list)<1) build_driver_list = 1; else build_driver_list = 0;
	active_driver = gconf_client_get_string(gconf,g_strdup_printf("%s/sound_driver",GNOMEQURAN_GCONF_PATH),NULL);
	for (i=0;i<num_driver;i++) {
		if (driver_list[i]->type==AO_TYPE_FILE) continue;
		gchar *sound_title = g_strdup_printf("%s",driver_list[i]->name);
		gchar *sound_short_title = g_strdup_printf("%s",driver_list[i]->short_name);
		if (active_driver) {
			if (strcmp(sound_short_title,active_driver)==0) driver_index = j;
		}
		if (build_driver_list)
		driver_short_list = g_list_append(driver_short_list,(gpointer)sound_short_title);
		sound_item = gtk_menu_item_new_with_label(sound_title);
		g_free(sound_title);
		gtk_menu_append(GTK_MENU(opt_sound_item),sound_item);
		gtk_widget_show(sound_item);
		j++;
	}
	
	gtk_option_menu_set_menu (GTK_OPTION_MENU (opt_sound), opt_sound_item);

	if (active_driver) {
		gtk_option_menu_set_history(GTK_OPTION_MENU(opt_sound),driver_index);
	}
	
	hbox2 = gtk_hbox_new (FALSE, 5);
	gtk_widget_ref (hbox2);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "hbox2", hbox2,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox2);
	gtk_container_add (GTK_CONTAINER (hbox1), hbox2);

	temp = gtk_label_new(_("Font:"));
	gtk_widget_ref(temp);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "temp", temp,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (temp);
	gtk_box_pack_start (GTK_BOX(hbox2), temp, FALSE, FALSE, 0);

	f=gnome_font_picker_new();
	if (active_font) {
		gnome_font_picker_set_font_name(f,active_font);
	}
	gnome_font_picker_set_mode(f,GNOME_FONT_PICKER_MODE_FONT_INFO);
	gtk_widget_show(f);
	gtk_object_set_data_full (GTK_OBJECT (frm_main), "font_selection", f,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_box_pack_start(GTK_BOX(hbox2), f, FALSE, FALSE, 0);
	g_signal_connect (GTK_OBJECT (f), "font-set",
	                    G_CALLBACK(on_font_selected),
	                    NULL);

	
	dialog_action_area1 = GNOME_DIALOG (dlg_preferences)->action_area;
	gtk_object_set_data (GTK_OBJECT (dlg_preferences), "dialog_action_area1", dialog_action_area1);
	gtk_widget_show (dialog_action_area1);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area1), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area1), 8);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_preferences), GNOME_STOCK_BUTTON_OK);
	btn_ok = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_preferences)->buttons)->data);
	gtk_widget_ref (btn_ok);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "btn_ok", btn_ok,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_ok);
	GTK_WIDGET_SET_FLAGS (btn_ok, GTK_CAN_DEFAULT);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_preferences), GNOME_STOCK_BUTTON_APPLY);
	btn_apply = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_preferences)->buttons)->data);
	gtk_widget_ref (btn_apply);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "btn_apply", btn_apply,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_apply);
	GTK_WIDGET_SET_FLAGS (btn_apply, GTK_CAN_DEFAULT);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_preferences), GNOME_STOCK_BUTTON_CANCEL);
	btn_cancel = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_preferences)->buttons)->data);
	gtk_widget_ref (btn_cancel);
	gtk_object_set_data_full (GTK_OBJECT (dlg_preferences), "btn_cancel", btn_cancel,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_cancel);
	GTK_WIDGET_SET_FLAGS (btn_cancel, GTK_CAN_DEFAULT);

	g_signal_connect (GTK_OBJECT (btn_ok), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_ok_clicked),
	                    opt_sound);
	g_signal_connect (GTK_OBJECT (btn_apply), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_apply_clicked),
	                    opt_sound);
	g_signal_connect (GTK_OBJECT (btn_cancel), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_cancel_clicked),
	                    NULL);

	return dlg_preferences;
}

GtkWidget*
create_dlg_cl (void)
{
	GtkWidget *dlg_cl;
	GtkWidget *dialog_vbox2;
	GtkWidget *hbox4;
	GtkWidget *label4;
	GtkWidget *opt_lang;
	GtkWidget *opt_lang_text;
	GtkWidget *dialog_action_area2;
	GtkWidget *btn_cl_ok;
	GtkWidget *btn_cl_cancel;

	dlg_cl = gnome_dialog_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (dlg_cl), "dlg_cl", dlg_cl);
	gtk_window_set_modal (GTK_WINDOW (dlg_cl), TRUE);
	gtk_window_set_policy (GTK_WINDOW (dlg_cl), FALSE, FALSE, FALSE);

	dialog_vbox2 = GNOME_DIALOG (dlg_cl)->vbox;
	gtk_object_set_data (GTK_OBJECT (dlg_cl), "dialog_vbox2", dialog_vbox2);
	gtk_widget_show (dialog_vbox2);

	hbox4 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox4);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "hbox4", hbox4,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox4);
	gtk_box_pack_start (GTK_BOX (dialog_vbox2), hbox4, TRUE, TRUE, 0);

	label4 = gtk_label_new (_("Choose Language: "));
	gtk_widget_ref (label4);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "label4", label4,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label4);
	gtk_box_pack_start (GTK_BOX (hbox4), label4, FALSE, FALSE, 0);

	opt_lang = gtk_combo_new ();
	gtk_widget_ref (opt_lang);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "opt_lang", opt_lang,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_lang);
	gtk_box_pack_start (GTK_BOX (hbox4), opt_lang, FALSE, FALSE, 0);
	gtk_widget_set_usize (opt_lang, 50, -2);

	opt_lang_text = GTK_COMBO (opt_lang)->entry;
	gtk_widget_ref (opt_lang_text);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "opt_lang_text", opt_lang_text,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_lang_text);
	gtk_entry_set_editable (GTK_ENTRY (opt_lang_text), FALSE);

	dialog_action_area2 = GNOME_DIALOG (dlg_cl)->action_area;
	gtk_object_set_data (GTK_OBJECT (dlg_cl), "dialog_action_area2", dialog_action_area2);
	gtk_widget_show (dialog_action_area2);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area2), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area2), 8);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_cl), GNOME_STOCK_BUTTON_OK);
	btn_cl_ok = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_cl)->buttons)->data);
	gtk_widget_ref (btn_cl_ok);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "btn_cl_ok", btn_cl_ok,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_cl_ok);
	GTK_WIDGET_SET_FLAGS (btn_cl_ok, GTK_CAN_DEFAULT);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_cl), GNOME_STOCK_BUTTON_CANCEL);
	btn_cl_cancel = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_cl)->buttons)->data);
	gtk_widget_ref (btn_cl_cancel);
	gtk_object_set_data_full (GTK_OBJECT (dlg_cl), "btn_cl_cancel", btn_cl_cancel,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_cl_cancel);
	GTK_WIDGET_SET_FLAGS (btn_cl_cancel, GTK_CAN_DEFAULT);

	gtk_signal_connect (GTK_OBJECT (btn_cl_ok), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_cl_ok_clicked),
	                    NULL);
	gtk_signal_connect (GTK_OBJECT (btn_cl_cancel), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_cl_cancel_clicked),
	                    NULL);

	return dlg_cl;
}

GtkWidget*
create_dlg_packages (void)
{
	GtkWidget *dlg_packages;
	GtkWidget *dialog_vbox2;
	GtkWidget *hbox4;
	GtkWidget *label4;
	GtkWidget *opt_package;
	GtkWidget *opt_package_text;
	GtkWidget *dialog_action_area2;
	GtkWidget *btn_packages_ok;
	GtkWidget *btn_packages_cancel;

	dlg_packages = gnome_dialog_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (frm_main), "dlg_packages", dlg_packages);
	gtk_window_set_modal (GTK_WINDOW (dlg_packages), TRUE);
	gtk_window_set_policy (GTK_WINDOW (dlg_packages), FALSE, FALSE, FALSE);

	dialog_vbox2 = GNOME_DIALOG (dlg_packages)->vbox;
	gtk_object_set_data (GTK_OBJECT (dlg_packages), "dialog_vbox2", dialog_vbox2);
	gtk_widget_show (dialog_vbox2);

	hbox4 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox4);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "hbox4", hbox4,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox4);
	gtk_box_pack_start (GTK_BOX (dialog_vbox2), hbox4, TRUE, TRUE, 0);

	label4 = gtk_label_new (g_strdup_printf(_("Choose audio package from language '%s': "),picked_lang));
	gtk_widget_ref (label4);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "label4", label4,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label4);
	gtk_box_pack_start (GTK_BOX (hbox4), label4, FALSE, FALSE, 0);

	opt_package = gtk_combo_new ();
	gtk_widget_ref (opt_package);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "opt_package", opt_package,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_package);
	gtk_box_pack_start (GTK_BOX (hbox4), opt_package, FALSE, FALSE, 0);

	opt_package_text = GTK_COMBO (opt_package)->entry;
	gtk_widget_ref (opt_package_text);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "opt_package_text", opt_package_text,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (opt_package_text);
	gtk_entry_set_editable (GTK_ENTRY (opt_package_text), FALSE);

	dialog_action_area2 = GNOME_DIALOG (dlg_packages)->action_area;
	gtk_object_set_data (GTK_OBJECT (dlg_packages), "dialog_action_area2", dialog_action_area2);
	gtk_widget_show (dialog_action_area2);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area2), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area2), 8);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_packages), GNOME_STOCK_BUTTON_OK);
	btn_packages_ok = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_packages)->buttons)->data);
	gtk_widget_ref (btn_packages_ok);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "btn_packages_ok", btn_packages_ok,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_packages_ok);
	GTK_WIDGET_SET_FLAGS (btn_packages_ok, GTK_CAN_DEFAULT);

	gnome_dialog_append_button (GNOME_DIALOG (dlg_packages), GNOME_STOCK_BUTTON_CANCEL);
	btn_packages_cancel = GTK_WIDGET (g_list_last (GNOME_DIALOG (dlg_packages)->buttons)->data);
	gtk_widget_ref (btn_packages_cancel);
	gtk_object_set_data_full (GTK_OBJECT (dlg_packages), "btn_packages_cancel", btn_packages_cancel,
	                          (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (btn_packages_cancel);
	GTK_WIDGET_SET_FLAGS (btn_packages_cancel, GTK_CAN_DEFAULT);

	gtk_signal_connect (GTK_OBJECT (btn_packages_ok), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_packages_ok_clicked),
	                    NULL);
	gtk_signal_connect (GTK_OBJECT (btn_packages_cancel), "clicked",
	                    GTK_SIGNAL_FUNC (on_btn_packages_cancel_clicked),
	                    NULL);

	return dlg_packages;
}

