package quran.lib;

/**
 * this class represents an instance of an ayah.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class AyahInst implements LibQuran {

    /**
     * the <code>ayahnum</code> variable holds the number of
     * this current ayah.
     *
     */
    private int ayahnum;

    /**
     * the <code>ayahtext</code> varaible holds the actual text
     * of the given ayah.
     *
     */
    private String ayahtext;

    /**
     * the <code>searchtext</code> variable holds the arabic text
     * without any tashkeel (or null if this doesn't exist).
     *
     */
    private String searchtext;

    /**
     * creates a new <code>AyahInst</code> instance.
     *
     * @param ayahnum an <code>int</code> for the number of this ayah
     */
    public AyahInst(int ayahnum){
      this.ayahnum = ayahnum;
      ayahtext = "";
      searchtext = "";
   }
   
    /**
     * the <code>addAyahText</code> method sets the ayah text
     * for the given ayah.
     *
     * @param ayahtext a <code>String</code> value of the text to add
     */
    public void addAyahText(String ayahtext){
      this.ayahtext = ayahtext;
   }

    /**
     * the <code>addSearchText</code> method sets the search text
     * for the given ayah.
     *
     * @param searchtext a <code>String</code> value of the text to add
     */
    public void addSearchText(String searchtext){
      this.searchtext = searchtext;
   }

    /**
     * the <code>getAyahText</code> method returns the ayah text.
     *
     * @return a <code>String</code> value containing the ayah text
     */
    public String getAyahText(){
      return ayahtext;
   }

    /**
     * the <code>getSearchText</code> method returns the search text.
     *
     * @return a <code>String</code> value containing the search text
     */
    public String getSearchText(){
      return searchtext;
   }
}
