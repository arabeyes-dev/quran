package quran.lib;

import org.xml.sax.*;
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;

public class QuranConfXML extends DefaultHandler {

   
   /**
    * <code>status</code> of what is being examined (text/audio)
    *
    */
   private int status;

   
   /**
    * <code>curtext</code> is the type of text being examined
    *
    */
   private int curtext;

   
   /**
    * <code>curstr</code> is the current text
    *
    */
   private String curstr;

   
   /**
    * <code>ql</code> is the current instance of the quran language
    *
    */
   private QuranLang ql;

   
   /**
    * Creates a new <code>QuranConfXML</code> instance.
    *
    * @param ql a <code>QuranLang</code> value of the quran language
    */
   public QuranConfXML(QuranLang ql){
      status = 0;
      curtext = 0;
      curstr = "";
      this.ql = ql;
   }

   
   /**
    * <code>startElement</code> method when an xml element starts
    *
    * @param ns_uri a <code>String</code> value
    * @param local_name a <code>String</code> value
    * @param q_name a <code>String</code> value
    * @param attrs an <code>Attributes</code> value
    */
   public void startElement(String ns_uri, String local_name,
                            String q_name, Attributes attrs){
      if (q_name.equals("language")){
         ql.setCode(attrs.getValue(0));
         ql.setName(attrs.getValue(1));
      }
      if (q_name.equals("text")){ ql.newText(); status = 1; }
      if (q_name.equals("audio")){ ql.newAudio(); status = 2; }
      if (q_name.equals("author")) curtext = 1;
      if (q_name.equals("copyright")) curtext = 2;
      if (q_name.equals("filename")) curtext = 3;
   }

   
   /**
    * <code>endElement</code> method when an xml element ends
    *
    * @param ns_uri a <code>String</code> value
    * @param local_name a <code>String</code> value
    * @param q_name a <code>String</code> value
    */
   public void endElement(String ns_uri, String local_name, String q_name){
      if (curstr.length() == 0) return;
      if (curtext == 1)
         if (status == 1)
            ql.addTextAuthor(curstr);
         else ql.addAudioAuthor(curstr);
      if (curtext == 2)
         if (status == 1)
            ql.addTextCopyright(curstr);
         else ql.addAudioCopyright(curstr);
      if (curtext == 3)
         if (status == 1)
            ql.addTextFilepath(curstr);
         else ql.addAudioDir(curstr);

      curstr = "";
      curtext = 0;
   }

   
   /**
    * <code>characters</code> method when text is encountered
    *
    * @param ch a <code>char[]</code> value
    * @param start an <code>int</code> value
    * @param length an <code>int</code> value
    */
   public void characters(char[] ch, int start, int length){
      int flag = 0;

      for (int i=0; i<length; i++){
         if ((flag == 0) &&
             (ch[start+i]=='\n' || (ch[start+i]=='\t') || (ch[start+i]==' ')))
             continue;
         flag = 1;
         if (ch[start+i]=='\n') continue;
         curstr += ch[start+i];
      }
   }
}