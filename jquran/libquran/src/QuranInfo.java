package quran.lib;
import java.io.*;

public class QuranInfo implements LibQuran {
   
   /**
    * <code>errmsg</code> is the latest error message encountered
    *
    */
   private String errmsg;

   
   /**
    * <code>datapath</code> is the path to the quran data
    *
    */
   private String datapath;

   
   /**
    * <code>langs</code> are the available quran languages
    *
    */
   private QuranLang[] langs;

   /**
    * Creates a new <code>QuranInfo</code> instance.
    *
    */
   public QuranInfo(){
      errmsg = "";
      datapath = "";
   }
   
   /**
    * <code>getDataPath</code> method returns the datapath.
    *
    * @return a <code>String</code> value of the datapath
    */
   public String getDataPath(){ return datapath; }

   
   /**
    * <code>getError</code> method returns the error message.
    *
    * @return a <code>String</code> value
    */
   public String getError(){ return errmsg; }

   
   /**
    * <code>setDataPath</code> method sets the path to the quran data files
    *
    * @param path a <code>String</code> value with the path
    */
   public void setDataPath(String path){
      datapath = path;
   }

   
   /**
    * <code>setDataFile</code> method sets the path to the quranrc
    *
    * @param quranrc a <code>String</code> value
    * @return a <code>boolean</code> value
    */
   public boolean setDataFile(String quranrc){
      try {
         File f = new File(quranrc);
         if (!f.exists()) return false;

         FileInputStream fr = new FileInputStream(f);
         InputStreamReader ir = new InputStreamReader(fr);
         BufferedReader br = new BufferedReader(ir);
         datapath = br.readLine();
         return true;
      }
      catch (Exception e){
         errmsg = "error!  no such .quranrc file as " + quranrc;
         return false;
      }
   }

   
   /**
    * <code>init</code> method initializes class after setDataFile or
    * setDataPath has been called to set the location of data.
    *
    * @return a <code>boolean</code> value
    */
   public boolean init(){
      if (datapath==null){
         errmsg = "error!  you must set the datapath first!";
         return false;
      }

      File f = new File(datapath);
      String[] dirs = f.list();
      if (dirs.length == 0){
         errmsg = "no files found in given datapath of: " + datapath;
         return false;
      }

      int cnt = 0;
      for (int i=0; i<dirs.length; i++){
         String fp = datapath + File.separator +
            dirs[i] + File.separator + "conf.xml";
         File tmp = new File(fp);
         if (!tmp.exists())
            dirs[i] = null;
         else cnt++;
         tmp = null;
      }
      if (cnt == 0){
         errmsg = "couldn't find conf.xml file in any of the potential"
            + " quran data directories under datapath of: " + datapath;
         return false;
      }

      langs = new QuranLang[cnt];
      for (int i=0, j=0; i<dirs.length; i++)
         if (dirs[i]!=null)
            langs[j++] = new QuranLang(datapath + File.separator + dirs[i]);
      for (int i=0; i<langs.length; i++)
         langs[i].init();
      return true;
   }

   
   /**
    * <code>getLanguageCodes</code> method returns available language codes
    *
    * @return a <code>String[]</code> value of the language codes
    */
   public String[] getLanguageCodes(){
      String[] lcodes = new String[langs.length];
      for (int i=0; i<langs.length; i++)
         lcodes[i] = langs[i].getCode();
      return lcodes;
   }

   
   /**
    * <code>getLanguageNames</code> method returns available language names
    *
    * @return a <code>String[]</code> value of the language names
    */
   public String[] getLanguageNames(){
      String[] lnames = new String[langs.length];
      for (int i=0; i<langs.length; i++)
         lnames[i] = langs[i].getName();
      return lnames;
   }

   
   /**
    * <code>getLanguageTexts</code> method returns texts available for a
    * given language
    *
    * @param language a <code>String</code> value of the language
    * @return a <code>String[]</code> value of the text names
    */
   public String[] getLanguageTexts(String language){
      for (int i=0; i<langs.length; i++){
         if (language.compareTo(langs[i].getName())==0)
            return langs[i].getTextList();
      }
      for (int i=0; i<langs.length; i++)
         if (language.compareTo(langs[i].getCode())==0)
            return langs[i].getTextList();
      return null;
   }

   
   /**
    * <code>getLanguageAudio</code> method returns audio available for a
    * given language.
    *
    * @param language a <code>String</code> value of the language
    * @return a <code>String[]</code> value of the audio for the language
    */
   public String[] getLanguageAudio(String language){
      for (int i=0; i<langs.length; i++){
         if (language.compareTo(langs[i].getName())==0)
            return langs[i].getAudioList();
      }
      for (int i=0; i<langs.length; i++)
         if (language.compareTo(langs[i].getCode())==0)
            return langs[i].getAudioList();
      return null;
   }

   
   /**
    * <code>openText</code> opens and initializes a quran text for use
    *
    * @param language a <code>String</code> value of the language
    * @param textname a <code>String</code> value of the text file name
    * @return a <code>QuranInst</code> value of the quran instance
    */
   public QuranInst openText(String language, String textname){
      for (int i=0; i<langs.length; i++){
         if (language.compareTo(langs[i].getName())==0)
            return langs[i].openText(textname);
      }
      for (int i=0; i<langs.length; i++)
         if (language.compareTo(langs[i].getCode())==0)
            return langs[i].openText(textname);
      return null;
   }

   
   /**
    * <code>playFile</code> method plays an audio file (experimental)
    *
    * @param language a <code>String</code> value of the language
    * @param audioname a <code>String</code> value of the audio name
    * @param sura an <code>int</code> value of the sura number
    * @param ayah an <code>int</code> value of the ayah number
    * @param decomp_path a <code>String</code> value of tmp path to write
    * the temporary decompressed file while its being played
    */
   public void playFile(String language, String audioname, int sura,
                             int ayah, String decomp_path){
      String s = null;
      for (int i=0; i<langs.length; i++){
         if (language.compareTo(langs[i].getName())==0)
            s = langs[i].playFile(audioname, sura, ayah, decomp_path);
      }
      for (int i=0; i<langs.length; i++)
         if (language.compareTo(langs[i].getCode())==0)
            s = langs[i].playFile(audioname, sura, ayah, decomp_path);
      errmsg = s;
      return;
   }
}