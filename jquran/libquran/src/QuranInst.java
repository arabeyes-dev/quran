package quran.lib;

import java.io.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;


/**
 * the <code>QuranInst</code> class allows access of a quran file.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class QuranInst implements LibQuran {

    /**
     * <code>filepath</code> holds the directory in which the file is
     *
     */
    private String filepath;


    /**
     * <code>errmsg</code> is the error message, if any.
     *
     */
    private String errmsg = "";


    /**
     * <code>translator</code> is the name of the translator (or null).
     *
     */
    private String translator = "";


    /**
     * <code>suralist</code> stores the instances of the suras.
     *
     */
    private SuraInst[] suralist;
  
    /**
     * this <code>init</code> method opens the given xml file if its
     * path is valid and parses the xml and stores the data internally.
     *
     * @param filepath a <code>String</code> value of the file path
     * @return a <code>boolean</code> value based on success or failure
     */
    public boolean init(String filepath){
      try {
         this.filepath = filepath;
         File f = new File(filepath);
         if (!f.exists()){
            errmsg = "could not find the file: " + filepath;
            return false;
         }

         suralist = new SuraInst[114];
         DefaultHandler handler = new QuranXML(this);
         SAXParserFactory factory = SAXParserFactory.newInstance();
         SAXParser parser = factory.newSAXParser();
         parser.parse(f, handler);
         errmsg = "";
         return true;
      }
      catch (Exception e){
         errmsg = e.getMessage();
         return false;
      }
   }


    /**
     * the <code>addSura</code> method adds a sura to the suralist.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param suraname a <code>String</code> value of the sura name
     */
    public void addSura(int suranum, String suraname){
      suralist[suranum-1] = new SuraInst(suranum, suraname);
   }


    /**
     * the <code>addAyah</code> method adds an ayah to the given sura.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @param ayahtext a <code>String</code> value of the ayah text
     */
    public void addAyah(int suranum, int ayahnum, String ayahtext){
      suralist[suranum-1].addAyah(ayahnum, ayahtext);
   }


    /**
     * the <code>addSearchText</code> method adds the given search text
     * to the given ayah (typically, this is used in arabic quran file
     * to hold the non-tashkeeled text)
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @param stext a <code>String</code> value of the text
     */
    public void addSearchText(int suranum, int ayahnum, String stext){
      suralist[suranum-1].addSearchText(ayahnum, stext);
   }


    /**
     * the <code>addTranslator</code> method adds the translator.
     *
     * @param translator a <code>String</code> value of the translator's name
     */
    public void addTranslator(String translator){
      this.translator = translator;
   }


    /**
     * the <code>getTranslator</code> method returns the translator.
     *
     * @return a <code>String</code> value of the translator
     */
    public String getTranslator(){
      return translator;
   }


    /**
     * the <code>getSuraName</code> method returns a sura's name.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @return a <code>String</code> value of the sura's name
     */
    public String getSuraName(int suranum){
      if (validSuraArg(suranum))
         return suralist[suranum-1].getName();
      else return null;
   }


    /**
     * the <code>getNumAyahs</code> returns the number of ayahs in a sura
     *
     * @param suranum an <code>int</code> value of the sura number
     * @return an <code>int</code> value of the number of ayahs in the sura
     */
    public int getNumAyahs(int suranum){
      if (validSuraArg(suranum))
         return NUMAYAHS[suranum - 1];
      else return -1;
   }
  

    /**
     * the <code>getAyah</code> method gets a particular ayah.
     * note that this is the same as the getAyahText method.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return a <code>String</code> value of the ayah text
     */
    public String getAyah(int suranum, int ayahnum){
      return getAyahText(suranum, ayahnum);
   }


    /**
     * the <code>getAyahText</code> method gets a particular ayah.
     * note that the getAyah method returns the value from this method.
     *
     * @param suranum an <code>int</code> value
     * @param ayahnum an <code>int</code> value
     * @return a <code>String</code> value
     */
    public String getAyahText(int suranum, int ayahnum){
      if (validArgs(suranum, ayahnum))
         return suralist[suranum-1].getAyah(ayahnum);
      else return null;
   }


    /**
     * the <code>getSearchText</code> method returns the search text.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return a <code>String</code> value of the search text
     */
    public String getSearchText(int suranum, int ayahnum){
      if (validArgs(suranum, ayahnum))
         return suralist[suranum-1].getSearchText(ayahnum);
      else return null;
   }



    /**
     * the <code>getLocationJuz</code> method returns the juz number
     * that houses the given ayah.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return an <code>int</code> value of the juz number
     */
    public int getLocationJuz(int suranum, int ayahnum){
      if (!validArgs(suranum, ayahnum))
         return -1;
      for (int i=0; i<HEZBQUAD.length; i+=8){
            if ((HEZBQUAD[i+7][0] > suranum) || 
                ((HEZBQUAD[i+7][0] == suranum) && 
                 (HEZBQUAD[i+7][1] >= ayahnum)))
               return (i/8)+1;
      }

      errmsg = "library bug encountered.  please email the author...";
      return -1;
   }


    /**
     * the <code>isSajdah</code> method tells you if an ayah is a sajdah.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return a <code>boolean</code> of whether the ayah is a sajdah or not
     */
    public boolean isSajdah(int suranum, int ayahnum){
      if (!validArgs(suranum, ayahnum))
         return false;
      for (int i=0; i<SAJDAHS.length; i++)
         if ((SAJDAHS[i][0]==suranum) && (SAJDAHS[i][1]==ayahnum))
            return true;
      return false;
   }


    /**
     * the <code>hasSajdah</code> method tells if a sura has a sajdah in it.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @return a <code>boolean</code> of if there's a sajdah in this sura
     */
    public boolean hasSajdah(int suranum){
      if (!validSuraArg(suranum))
         return false;
      for (int i=0; i<SAJDAHS.length; i++)
         if (SAJDAHS[i][0]==suranum)
            return true;
      return false;
   }


    /**
     * the <code>getSajdahs</code> method finds the sajdah(s) in a sura.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @return an <code>int[]</code> of the sajdahs found.
     * please note that the size of the returned array will always be
     * one for all suras with a sajdah in them EXCEPT for sura 22, which
     * has 2 sajdahs in it.  if no sajdah is found, null is returned.
     */
    public int[] getSajdahs(int suranum){
      int ret[];
      if (!hasSajdah(suranum))
         return null;
      if (suranum == 22)
         ret = new int[2];
      else ret = new int[1];
      for (int i=0; i<SAJDAHS.length; i++){
         if (SAJDAHS[i][0]==suranum){
            ret[0] = SAJDAHS[i][1];
            if (suranum == 22)
               ret[1] = SAJDAHS[i+1][1];
            return ret;
         }
      }

      return null;
   }


    /**
     * the <code>getError</code> method returns the last error encountered.
     *
     * @return a <code>String</code> value of the error encountered.
     */
    public String getError(){
      return errmsg;
   }


    /**
     * the <code>validSuraArg</code> method ensures a sura number is valid.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @return a <code>boolean</code> value of whether or not its legal
     */
    public boolean validSuraArg(int suranum){
      errmsg = "";

      if ((suranum > 114) || (suranum < 1)){
         errmsg = "invalid surah number!";
         return false;
      }

      return true;
   }
   

    /**
     * <code>validArgs</code> checks the validity of sura and ayah numbers.
     *
     * @param suranum an <code>int</code> value of the sura number
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return a <code>boolean</code> value of whether or not they are valid
     */
    public boolean validArgs(int suranum, int ayahnum){
      if (!validSuraArg(suranum))
         return false;
      else if ((NUMAYAHS[suranum-1] < ayahnum) || (ayahnum < 1)){
         errmsg = "invalid ayah number!  ayah numbers for this " +
            "sura range from 1 to " + NUMAYAHS[suranum-1];
         return false;
      }
      else return true;
   }
}
