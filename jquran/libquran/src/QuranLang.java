package quran.lib;

import java.io.*;
import java.util.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;

public class QuranLang {
   
   /**
    * <code>audio</code> contains audio information.
    *
    */
   private Vector audio;

   
   /**
    * <code>texts</code> contains text information.
    *
    */
   private Vector texts;

   
   /**
    * <code>langcode</code> is the code of the particular language
    *
    */
   private String langcode;

   
   /**
    * <code>langname</code> is the name of the particular language
    *
    */
   private String langname;

   
   /**
    * <code>langpath</code> is the path for this language
    *
    */
   private String langpath;

   
   /**
    * <code>audio_ctr</code> is the number of audio directories found
    *
    */
   private int audio_ctr;

   
   /**
    * <code>texts_ctr</code> is the number of text directories found
    *
    */
   private int texts_ctr;

   
   /**
    * Creates a new <code>QuranLang</code> instance.
    *
    * @param path a <code>String</code> value of the path
    */
   public QuranLang(String path){
      langpath = path;
      langcode = "";
      langname = "";
      audio = new Vector();
      texts = new Vector();
   }

   
   /**
    * <code>init</code> method initializes the language
    *
    */
   public void init(){
      try {
         audio_ctr = -1;
         texts_ctr = -1;
         DefaultHandler handler = new QuranConfXML(this);
         SAXParserFactory factory = SAXParserFactory.newInstance();
         SAXParser parser = factory.newSAXParser();
         File confile = new File(langpath + File.separator + "conf.xml");
         parser.parse(confile, handler);

         audio.trimToSize();
         texts.trimToSize();
         return;
      }
      catch (Exception e){
         audio = new Vector();
         texts = new Vector();
         audio.trimToSize();
         texts.trimToSize();
         System.out.println("error encountered while parsing conf.xml!");
         System.out.println(e);
         return;
      }
   }

   
   /**
    * <code>toString</code> method for debugging info
    *
    * @return a <code>String</code> value of some class info
    */
   public String toString(){
      String str = "";
      str += "successfully added: " + langname + " (" + langcode + ")\n";
      str += "\npaths of added texts:\n";
      for (int i=0; i<texts.size(); i++)
         str += ((QuranText)(texts.elementAt(i))).getFilePath() + "\n";
      str += "\npaths of added audio: ";
      str += (audio.size()==0)? "none\n" : "\n";
      for (int i=0; i<audio.size(); i++)
         str += ((QuranAudio)(audio.elementAt(i))).getAudioDir() + "\n";
      str += "\n";
      return str;
   }

   
   /**
    * <code>newText</code> method adds a text
    *
    */
   public void newText(){
      texts.trimToSize();
      texts_ctr++;
      texts.add(new QuranText());
   }

   
   /**
    * <code>newAudio</code> method adds audio
    *
    */
   public void newAudio(){
      audio.trimToSize();
      audio_ctr++;
      audio.add(new QuranAudio());
   }

   
   /**
    * <code>addTextAuthor</code> method adds text author
    *
    * @param auth a <code>String</code> value of the text author
    */
   public void addTextAuthor(String auth){
      QuranText t = (QuranText)texts.elementAt(texts_ctr);
      t.setAuthor(auth);
   }

   
   /**
    * <code>addAudioAuthor</code> method adds audio author
    *
    * @param auth a <code>String</code> value of the audio author
    */
   public void addAudioAuthor(String auth){
      QuranAudio a = (QuranAudio)audio.elementAt(audio_ctr);
      a.setAuthor(auth);
   }

   
   /**
    * <code>addTextCopyright</code> method adds text copyright info
    *
    * @param cpinfo a <code>String</code> value of copyright info
    */
   public void addTextCopyright(String cpinfo){
      QuranText t = (QuranText)texts.elementAt(texts_ctr);
      t.setCopyright(cpinfo);
   }

   
   /**
    * <code>addAudioCopyright</code> method adds audio copyright info
    *
    * @param cpinfo a <code>String</code> value of copyright info
    */
   public void addAudioCopyright(String cpinfo){
      QuranAudio a = (QuranAudio)audio.elementAt(audio_ctr);
      a.setCopyright(cpinfo);
   }

   
   /**
    * <code>addTextFilepath</code> method adds the filepath to text
    *
    * @param fp a <code>String</code> value of the filepath
    */
   public void addTextFilepath(String fp){
      QuranText t = (QuranText)texts.elementAt(texts_ctr);
      t.setFilePath(fp, langpath + File.separator + fp);
   }

   
   /**
    * <code>addAudioDir</code> method adds the filepath to audio
    *
    * @param fp a <code>String</code> value of audio path
    */
   public void addAudioDir(String fp){
      QuranAudio a = (QuranAudio)audio.elementAt(audio_ctr);
      a.setAudioDir(fp, langpath + File.separator + fp);
   }

   
   /**
    * <code>getTextList</code> method gets a list of texts
    *
    * @return a <code>String[]</code> value of texts available
    */
   public String[] getTextList(){
      if (texts.size()==0) return null;
      String[] toret = new String[texts.size()];
      for (int i=0; i<texts.size(); i++)
         toret[i] = ((QuranText)(texts.elementAt(i))).getName();
      return toret;
   }

   
   /**
    * <code>getAudioList</code> method gets a list of audios
    *
    * @return a <code>String[]</code> value of audios available
    */
   public String[] getAudioList(){
      if (audio.size()==0) return null;
      String[] toret = new String[audio.size()];
      for (int i=0; i<audio.size(); i++)
         toret[i] = ((QuranAudio)(audio.elementAt(i))).getName();
      return toret;
   }

   
   /**
    * <code>openText</code> method opens a text for reading
    *
    * @param textname a <code>String</code> value of the text name
    * @return a <code>QuranInst</code> value of the quran instance
    */
   public QuranInst openText(String textname){
      if (texts.size()==0) return null;
      for (int i=0; i<texts.size(); i++)
         if (textname.compareTo(((QuranText)texts.elementAt(i)).getName())==0)
            return ((QuranText)texts.elementAt(i)).open();
      return null;
   }

   
   /**
    * <code>playFile</code> method plays an audio file
    *
    * @param audioname a <code>String</code> value of the audioname
    * @param ayah an <code>int</code> value of the sura number
    * @param sura an <code>int</code> value of the ayah number
    * @param path a <code>String</code> value of the path
    * @return a <code>String</code> value of any errors
    */
   public String playFile(String audioname, int sura, int ayah, String path){
      if (audio.size()==0) return "could not find given audioname "
         + audioname;
      for (int i=0; i<audio.size(); i++)
         if (audioname.compareTo((
                                  (QuranAudio)audio.elementAt(i)).
                                 getName())==0)
            return ((QuranAudio)audio.elementAt(i)).playfile(ayah, sura, path);
      return "could not find given audioname " + audioname;
   }
   
   public void setCode(String code){ langcode = code; }
   public String getCode(){ return langcode; }
   public void setName(String name){ langname = name; }
   public String getName(){ return langname; }
}