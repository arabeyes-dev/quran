package quran.lib;

public class QuranText {
   
   /**
    * <code>name</code> of the text file
    *
    */
   private String name;

   
   /**
    * <code>author</code> of the text file
    *
    */
   private String author;

   
   /**
    * <code>filepath</code> path of the text file
    *
    */
   private String filepath;

   
   /**
    * <code>copyright</code> copyright of the text file
    *
    */
   private String copyright;

   
   /**
    * Creates a new <code>QuranText</code> instance.
    *
    */
   public QuranText(){
      author = "";
      filepath = "";
      copyright = "";
   }

   
   /**
    * <code>open</code> opens a quran text
    *
    * @return a <code>QuranInst</code> value of the quran instance
    */
   public QuranInst open(){
      QuranInst qi = new QuranInst();
      qi.init(filepath);
      return qi;
   }

   
   /**
    * <code>setAuthor</code> method sets the author
    *
    * @param auth a <code>String</code> value of the author
    */
   public void setAuthor(String auth){ author = auth; }

   
   /**
    * <code>getAuthor</code> method gets the author
    *
    * @return a <code>String</code> value of the author
    */
   public String getAuthor(){ return author; }

   
   /**
    * <code>setFilePath</code> method sets the file path
    *
    * @param fn a <code>String</code> value of the file name
    * @param path a <code>String</code> value of the file path
    */
   public void setFilePath(String fn, String path){
      name = fn;
      filepath = path;
   }

   
   /**
    * <code>getName</code> method gets the file name
    *
    * @return a <code>String</code> value of the file name
    */
   public String getName(){ return name; }

   
   /**
    * <code>getFilePath</code> method gets the file path
    *
    * @return a <code>String</code> value of the file path
    */
   public String getFilePath(){ return filepath; }

   
   /**
    * <code>setCopyright</code> method sets the copyright info
    *
    * @param cr a <code>String</code> value of the copyright
    */
   public void setCopyright(String cr){ copyright = cr; }

   
   /**
    * <code>getCopyright</code> method gets the copyright info.
    *
    * @return a <code>String</code> value of the copyright
    */
   public String getCopyright(){ return copyright; }
}