package quran.lib;

import org.xml.sax.*;
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;


/**
 * the <code>QuranXML</code> class is responsible for dealing with xml files.
 * this handles the calls from the xml parser and adds suras and ayahs.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class QuranXML extends DefaultHandler {

    /**
     * the <code>sura</code> variable holds the sura number.
     *
     */
    private int sura;

    /**
     * the <code>ayah</code> variable holds the ayah number.
     *
     */
    private int ayah;

    /**
     * the <code>current</code> is a flag that denotes if we are looking
     * at quran text or at quran search text.
     *
     */
    private int current = 0;

    /**
     * the <code>curstr</code> variable is the ayah thusfar.
     *
     */
    private String curstr = "";

    /**
     * the <code>q</code> variable is an instance of the calling QuranInst
     * class.  this is used to add suras and ayahs as they are found.
     *
     */
    private QuranInst q;
   

    /**
     * creates a new <code>QuranXML</code> instance.
     *
     * @param q a <code>QuranInst</code> value of the calling class
     */
    public QuranXML(QuranInst q){
      this.q = q;
   }
   

    /**
     * the <code>startElement</code> method is called by the xml parser
     * when the start of an element is found.  this is where the sura,
     * its name, number, the translator, and whether or not the text
     * is quran text or search text is determined.  calls are made thusly.
     *
     * @param ns_uri a <code>String</code> value
     * @param local_name a <code>String</code> value
     * @param q_name a <code>String</code> value
     * @param attrs an <code>Attributes</code> value
     */
    public void startElement(String ns_uri, String local_name,
                            String q_name, Attributes attrs){
      if (q_name.equals("sura")){
         sura = Integer.parseInt(attrs.getValue(0));
         q.addSura(sura, attrs.getValue(1));
      }
      if (q_name.equals("qurantext"))
         current = 0;
      if (q_name.equals("searchtext"))
         current = 1;
      if (q_name.equals("aya"))
         ayah = Integer.parseInt(attrs.getValue(0));
      if (q_name.equals("translated"))
         q.addTranslator(attrs.getValue(0));
   }


    /**
     * the <code>endElement</code> method is called by the xml parser
     * when the end of an element is found.  this is where the ayah text
     * is added to the QuranInst.
     *
     * @param ns_uri a <code>String</code> value
     * @param local_name a <code>String</code> value
     * @param q_name a <code>String</code> value
     */
    public void endElement(String ns_uri, String local_name, String q_name){
      if (curstr.length()>0){
         if (current == 0) q.addAyah(sura, ayah, curstr);
         else q.addSearchText(sura, ayah, curstr);
      }
      curstr = "";
   }


    /**
     * the <code>characters</code> method is called by the xml parser
     * upon finding any text.  this text is gathered to form the ayahs
     * and are added to the QuranInst when the end of the element is found
     *
     * @param ch a <code>char[]</code> value
     * @param start an <code>int</code> value
     * @param length an <code>int</code> value
     */
    public void characters(char[] ch, int start, int length){
      int flag = 0;

      for (int i=0; i<length; i++){
         if ((flag == 0) &&
             (ch[start+i]=='\n' || (ch[start+i]=='\t') || (ch[start+i]==' ')))
             continue;
         flag = 1;
         if (ch[start+i]=='\n') continue;
         curstr += ch[start+i];
      }
   }
}
