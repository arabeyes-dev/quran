package quran.lib;


/**
 * the <code>SuraInst</code> class holds an instance of a sura.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class SuraInst implements LibQuran {

    /**
     * the variable <code>suranum</code> holds this sura's number.
     *
     */
    private int suranum;


    /**
     * the variable <code>suraname</code> holds the name of the sura.
     *
     */
    private String suraname;


    /**
     * the variable <code>ayahlist</code> holds a list of ayahs.
     *
     */
    private AyahInst[] ayahlist;


    /**
     * Creates a new <code>SuraInst</code> instance.
     *
     * @param number an <code>int</code> value of the sura number
     * @param name a <code>String</code> value of the sura name
     */
    public SuraInst(int number, String name){
      this.suranum = number;
      this.suraname = name;

      ayahlist = new AyahInst[NUMAYAHS[number-1]];
      for (int i=1; i<=NUMAYAHS[number-1]; i++)
         ayahlist[i-1] = new AyahInst(i);
   }
   

    /**
     * the <code>addAyah</code> method adds an ayah to the sura.
     *
     * @param ayahnum an <code>int</code> value of the ayah number
     * @param ayahtext a <code>String</code> value of the ayah text
     */
    public void addAyah(int ayahnum, String ayahtext){
      ayahlist[ayahnum-1].addAyahText(ayahtext);
   }


    /**
     * the <code>addSearchText</code> method adds searchtext to the sura.
     *
     * @param ayahnum an <code>int</code> value of the ayah number
     * @param searchtext a <code>String</code> value of the search text
     */
    public void addSearchText(int ayahnum, String searchtext){
      ayahlist[ayahnum-1].addSearchText(searchtext);
   }


    /**
     * the <code>getName</code> method returns the sura's name.
     *
     * @return a <code>String</code> value of the sura's name
     */
    public String getName(){
      return suraname;
   }


    /**
     * the <code>getAyah</code> method returns a given ayah.
     *
     * @param ayahnum an <code>int</code> value of the ayah number
     * @return a <code>String</code> value of the ayah text
     */
    public String getAyah(int ayahnum){
      return ayahlist[ayahnum-1].getAyahText();
   }


    /**
     * the <code>getSearchText</code> method returns the search text.
     *
     * @param ayahnum an <code>int</code> value of the given ayah
     * @return a <code>String</code> value of the search text
     */
    public String getSearchText(int ayahnum){
      return ayahlist[ayahnum-1].getSearchText();
   }
}
