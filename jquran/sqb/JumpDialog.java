import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
 * the <code>JumpDialog</code> class shows the jump dialog.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class JumpDialog implements ActionListener {

    /**
     * the <code>sqb</code> variable holds the instance of the calling class.
     *
     */
    private SQB sqb;

    /**
     * the <code>tmp</code> jframe displays this dialog.
     *
     */
    private JFrame tmp;

    /**
     * the <code>suratf</code> is the sura textfield.
     *
     */
    private JTextField suratf;


    /**
     * the <code>ayahtf</code> is the ayah textfield.
     *
     */
    private JTextField  ayahtf;
   

    /**
     * creates a new <code>JumpDialog</code> instance.
     *
     * @param qb a <code>SQB</code> value of the calling class
     */
    public JumpDialog(SQB qb) {
      this.sqb = qb;
      drawDialog();
   }


    /**
     * the <code>drawDialog</code> method draws the gui.
     *
     */
    public void drawDialog(){
      tmp = new JFrame("jump to...");
      tmp.setSize(200, 100);
      tmp.getContentPane().setLayout(new BorderLayout());
      tmp.setLocation(100, 100);
      
      JPanel jp = new JPanel();
      jp.setLayout(new GridLayout(2, 2));
      JLabel tlabel = new JLabel("sura:");
      jp.add(tlabel);
      suratf = new JTextField();
      jp.add(suratf);
      tlabel = new JLabel("ayah:");
      jp.add(tlabel);
      ayahtf = new JTextField();
      jp.add(ayahtf);

      tmp.getContentPane().add(jp, BorderLayout.CENTER);
      JButton submit = new JButton("go!");
      submit.addActionListener(this);
      tmp.getContentPane().add(submit, BorderLayout.SOUTH);
      tmp.show();
   }


    /**
     * the <code>actionPerformed</code> method detects when 
     * an action is performed.
     *
     * @param e an <code>ActionEvent</code> value
     */
    public void actionPerformed(ActionEvent e){
      if (e.getActionCommand().equals("go!")){
         try {
            int i = Integer.parseInt(suratf.getText());
            int j = Integer.parseInt(ayahtf.getText());
            sqb.jumpTo(i, j);
            tmp.hide();
            tmp.dispose();
         }
         catch (Exception ex) {
            tmp.hide();
            tmp.dispose();
         }
      }
   }
   
}
