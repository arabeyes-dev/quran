import quran.lib.*;

import java.io.*;

/**
 * the <code>QuranBackend</code> class connects the gui with libquran.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class QuranBackend {

    /**
     * <code>maxAyahs</code> is the number of ayahs for the given sura.
     *
     */
    private int maxAyahs;

    /**
     *  the <code>currentSura</code> variable holds the current sura.
     *
     */
    private int currentSura;

    /**
     * the <code>currentAyah</code> variable holds the current ayah.
     *
     */
    private int currentAyah;
    
    /**
     * the <code>arabic</code> variable holds an arabic quran instance.
     *
     */
    private QuranInst arabic;
   
    /**
     * the <code>english</code> variable holds an english quran instance.
     *
     */
    private QuranInst english;
   
    /**
     * the <code>sqb</code> variable holds an instance to the calling class.
     *
     */
    private SQB sqb;


    /**
     * creates a new <code>QuranBackend</code> instance.
     *
     * @param qb a <code>SQB</code> value which is the calling class
     */
    public QuranBackend(SQB qb){
      sqb = qb;
      currentAyah = 1;
      currentSura = 1;
      maxAyahs = 7;
   }


    /**
     * the <code>initialize</code> method opens and inits the quran instances.
     *
     * @param rcdir a <code>String</code> value of the directory with
     * the .quranrc file.
     * @return a <code>String</code> value which is an error or an empty str
     */
    public String initialize(String rcdir){
      QuranInfo qi = new QuranInfo();
      if (!qi.setDataFile(rcdir + File.separator + ".quranrc"))
         return qi.getError();
      qi.init();
      String[] tmp;
      tmp = qi.getLanguageTexts("ar");
      if (tmp == null)
         return "no arabic texts found!";
      arabic = qi.openText("ar", tmp[0]);
      if (arabic == null)
         return "error opening arabic text!";
      tmp = qi.getLanguageTexts("en");
      if (tmp == null)
         return "no english translations found!";
      english = qi.openText("en", tmp[0]);
      if (english == null)
         return "error opening english translation!";
      return "";
   }


    /**
     * the <code>nextAyah</code> method goes to the next ayah.
     *
     */
    public void nextAyah(){
      if (currentAyah < maxAyahs)
         setLocation(currentSura, currentAyah+1);
      else if ((currentAyah == maxAyahs) && (currentSura != 114))
         setLocation(currentSura+1, 1);
      else setLocation(1,1);
   }


    /**
     * the <code>prevAyah</code> method goes to the previous ayah.
     *
     */
    public void prevAyah(){
      if (currentAyah > 1)
         setLocation(currentSura, currentAyah-1);
      else if ((currentAyah==1) && (currentSura != 1))
         setLocation(currentSura-1, arabic.getNumAyahs(currentSura-1));
      else setLocation(114, 6);
   }
   

    /**
     * the <code>getCurrentSura</code> method gets the current sura.
     *
     * @return an <code>int</code> value of the sura number
     */
    public int getCurrentSura(){
      return currentSura;
   }


    /**
     * the <code>getCurrentAyah</code> method gets the current ayah.
     *
     * @return an <code>int</code> value of the ayah number
     */
    public int getCurrentAyah(){
      return currentAyah;
   }


    /**
     * the <code>getCurrentVerse</code> method receives the current verse.
     *
     * @return a <code>String</code> value of the text of the ayah
     */
    public String getCurrentVerse(){
      String s =  "<p align=\"right\">" +
         arabic.getAyah(currentSura, currentAyah) + "</p><br><br>" +
         english.getAyah(currentSura, currentAyah);
      return s;
   }


    /**
     * the <code>getLabelInfo</code> method gets information for the label.
     * this includes sura number, ayah number, and if we're at a sajdah.
     *
     * @return a <code>String</code> value of the label's text
     */
    public String getLabelInfo(){
      String s = "sura: " + currentSura + " ayah: " 
                + currentAyah + "     ";
      s += "juz: " + arabic.getLocationJuz(currentSura, currentAyah);
      if (arabic.isSajdah(currentSura, currentAyah))
         s += "     sajdah!";
      return s;
   }


    /**
     * the <code>setLocation</code> method jumps to a different place
     * in the quran if they are valid arguments.
     *
     * @param sura an <code>int</code> value which is the sura number
     * @param ayah an <code>int</code> value which is the ayah number
     */
    public void setLocation(int sura, int ayah){
      if (arabic.getAyah(sura, ayah)!=null){
         currentAyah = ayah;
         currentSura = sura;
         maxAyahs = arabic.getNumAyahs(sura);
         sqb.redraw();
      }
   }
}
