import java.io.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;


/**
 * the <code>SQB</code> class is the main class for the simple quran browser.
 *
 * @author <a href="mailto:ahmedre@cc.gatech.edu">ahmed el-helw</a>
 * @version 1.0
 */
public class SQB implements ActionListener {

    /**
     * <code>jf</code> is the frame for the display .
     *
     */
    private JFrame jf;

    /**
     * <code>jl</code> is the label that holds current information.
     *
     */
    private JLabel jl;

    /**
     * <code>quranText</code> is the place where the quran text is.
     *
     */
    private JEditorPane quranText;

    /**
     * <code>qb</code> is an instance of the QuranBackend model.
     * it is used to interface with LibQuran.
     *
     */
    private QuranBackend qb;


    /**
     * creates a new <code>SQB</code> instance.
     *
     */
    public SQB(String path){

      qb = new QuranBackend(this);
      String tmp = qb.initialize(path);
      if (tmp.length() > 0){
         System.out.println(tmp);
         System.exit(0);
      }
      displayUI();
   }


    /**
     * the <code>displayUI</code> method displays the user interface.
     *
     */
    public void displayUI(){
      quranText = new JEditorPane("text/html", "");
      quranText.setEditable(false);

      jl = new JLabel();
      
      jf = new JFrame("simple quran browser");
      jf.setSize(400, 400);
      jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      jf.getContentPane().setLayout(new BorderLayout());
      jf.setLocation(50, 50);


      redraw();
      JScrollPane scroll = new JScrollPane(quranText, 
                                 JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                 JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      
      jf.getContentPane().add(scroll, BorderLayout.CENTER);
      jf.getContentPane().add(jl, BorderLayout.NORTH);

      JPanel jp = new JPanel();
      jp.setLayout(new GridLayout(1, 2));
      ImageIcon ic = new ImageIcon("./images/prev.png");
      JButton prev = new JButton("prev", ic);
      prev.setBorder(new LineBorder(Color.BLACK, 0));
      ic = new ImageIcon("./images/next.png");
      JButton next = new JButton("next", ic);
      next.setBorder(new LineBorder(Color.BLACK, 0));

      JMenuBar menu = new JMenuBar();
      JMenu filemenu = new JMenu("file");
      JMenuItem jump = new JMenuItem("jump");
      JMenuItem quit = new JMenuItem("quit");
      filemenu.add(jump);
      filemenu.add(quit);
      menu.add(filemenu);

      jf.setJMenuBar(menu);

      jump.addActionListener(this);
      quit.addActionListener(this);
      
      prev.addActionListener(this);
      next.addActionListener(this);
      
      jp.add(prev);
      jp.add(next);
      jf.getContentPane().add(jp, BorderLayout.SOUTH);

      jf.show();
   }
 

    /**
     * <code>redraw</code> updates the text and label on ayah switch.
     *
     */
    public void redraw(){
      String str = qb.getCurrentVerse();
      quranText.setText(str);
      jl.setText(qb.getLabelInfo());
   }


    /**
     * <code>jumpTo</code> method allows the user to jump to another place.
     *
     * @param sura an <code>int</code> value of the sura number
     * @param ayah an <code>int</code> value of the ayah number
     */
    public void jumpTo(int sura, int ayah){
      qb.setLocation(sura, ayah);
   }
   

    /**
     * <code>actionPerformed</code> method handles actions.
     *
     * @param e an <code>ActionEvent</code> value of the action
     */
    public void actionPerformed(ActionEvent e){
      if (e.getActionCommand().equals("next"))
         qb.nextAyah();
      if (e.getActionCommand().equals("prev"))
         qb.prevAyah();
      if (e.getActionCommand().equals("quit"))
         System.exit(0);
      if (e.getActionCommand().equals("jump"))
         new JumpDialog(this);
   }


    /**
     * the <code>main</code> method executes the program.
     *
     * @param argv a <code>String[]</code> value
     */
    public static void main(String[] argv){
      if (argv.length == 0){
         System.out.println("error.. please pass in the directory " +
                            "which contains the .quranrc as a paramater.");
         return;
      }
      System.out.println("please wait while the program loads...");
      new SQB(argv[0]);
   }
}
