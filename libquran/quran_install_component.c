/* libquran - Holy Quran library
 * Copyright (C) 2002, 2005 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: This source code file is very badly written, so don't try to
 *       modify or maintain it, instead a PERL script that does its
 *       job should be written.
 *
 * This tool automates the task of making language packages and installing
 * them. It assumes that any language package component (text file, complete
 * audio recitation for a single reciter...etc) uses this convention:
 *     COMPONENT_NAME.tbz2 inside it:
 *        language               (The name of the language i.e. "English")
 *        language_code          (The language code i.e. "en")
 *        author                 (The author of the component)
 *        copyright              (component copyright data)
 *        filename               (component filename i.e. "quran.ar.xml")
 *        type                   (component type i.e. "text" or "audio")
 *    and lastly the actual component file or directory
 *    (i.e. the actual quran.ar.xml file)
 */



#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <init.h>
#include <pwd.h>

#define CONF_XML "conf.xml"

#define LANGUAGE_CODE "language_code"
#define LANGUAGE "language"
#define TYPE "type"
#define AUTHOR "author"
#define COPYRIGHT "copyright"
#define FILENAME "filename"

char * get_value_from_file(char *, char *);
char * get_file_contents(char *);
char * create_lang_dir(char *);

int main(int argc, char * argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: quran_install_component COMPONENT_NAME\n");
		fprintf(stderr, "To install the component quran_ar_xml.tbz2: quran_install_component quran_ar_xml\n");
		fprintf(stderr, "You can get components from http://www.arabeyes.org\n");
		return 1;
	}
	

	char * dearchive_command_base = "tar -jxvf %s.tbz2";
	char * dearchive_command = (char *)malloc((strlen(argv[1])
	                           + strlen(dearchive_command_base)
	                           - 1  /* '%s' */
	                           + 1)  /* '\0'  */
	                           * sizeof(char));
	sprintf(dearchive_command, dearchive_command_base, argv[1]);
	system(dearchive_command);
	free(dearchive_command);
	dearchive_command = NULL;
	


	char * package = argv[1];
	char * language_code = get_value_from_file(LANGUAGE_CODE, package);
	char * language = get_value_from_file(LANGUAGE, package);
	char * data_dir;
	while ((data_dir = create_lang_dir(language_code)) == NULL)
		;

	struct stat conf_xml_stat;
	char * conf_xml_filename = (char *)malloc((strlen(data_dir) + 1 /* '/' */
	                           + strlen(language_code) + 1 /* '/' */
	                           + 8  /* "conf.xml" */
	                           + 1) /* '\0' */
	                           * sizeof(char));
	sprintf(conf_xml_filename, "%s/%s/%s", data_dir, language_code, CONF_XML);
	int r = stat(conf_xml_filename, &conf_xml_stat);
	if (r != 0) {
		if (errno == ENOENT) { /* file doesn't exist */
			char * base_conf_xml_text = "<quran>\n\t<language code=\"%s\" name=\"%s\">\n\t</language>\n</quran>\n";
			/* create it */
			FILE * conf_xml_file = fopen(conf_xml_filename, "w");
			if (conf_xml_file == NULL) {
				fprintf(stderr, "2Couldn't open file %s for writing.\n", conf_xml_filename);
				return 1;
			}
			fprintf(conf_xml_file, base_conf_xml_text,
			        language_code, language);
			fclose(conf_xml_file);
			conf_xml_file = NULL;
			r = stat(conf_xml_filename, &conf_xml_stat);
		} else {	/* error */
			fprintf(stderr, "1Couldn't open file %s for writing.\n", conf_xml_filename);
			return 1; /* bail out */
		}
	}
	
	if (S_ISREG (conf_xml_stat.st_mode) == 0) {
		fprintf(stderr, "Error: file %s is not a regular file.\n", conf_xml_filename);
		return 1;
	}

	char * type = get_value_from_file(TYPE, package);
	char * author = get_value_from_file(AUTHOR, package);
	char * copyright = get_value_from_file(COPYRIGHT, package);
	char * filename = get_value_from_file(FILENAME, package);
	
	

	char * conf_xml_block_empty = "\t\t<%s>\n\t\t\t<author>%s</author>\n\t\t\t<copyright>%s</copyright>\n\t\t\t<filename>%s</filename>\n\t\t</%s>\n";
	
	char * conf_xml_block = (char *)malloc((strlen(conf_xml_block_empty)
	                        - 10  /* "%s"s */
	                        + strlen(type) * 2
	                        + strlen(author)
	                        + strlen(copyright)
	                        + strlen(filename)
	                        + 1)  /* '\0'  */
	                        * sizeof(char));
	sprintf(conf_xml_block, conf_xml_block_empty, type, author, copyright, filename, type);



	// TODO: open conf.xml and put conf_xml_block on it
	char * old_conf_xml_contents = get_file_contents(conf_xml_filename);

	if (old_conf_xml_contents == NULL) {
		return 1;
	}


	/* FIXME: use regular expressions to seek to this line, since it's not always
	          formatted like this */
	char * append_after_base = "<language code=\"%s\" name=\"%s\">";
	char * append_after = (char *)malloc((strlen(append_after_base)
	                       - 4  /* "%s"s */
	                       + strlen(language_code)
	                       + strlen(language)
	                       + 1)  /* '\0'  */
	                       * sizeof(char));
	sprintf(append_after, append_after_base, language_code, language);

	/* If append_after is there, append after it, if not, append after <quran> */
	char * pos = strstr(old_conf_xml_contents, append_after);

	if (pos != NULL) { // a match
		pos += strlen(append_after) - 1;
		if (*(pos + 1) == '\n')
			pos++;
		char * first_half = (char *)malloc((pos - old_conf_xml_contents + 1
	                       + 1)  /* '\0'  */
	                       * sizeof(char));
		strncpy(first_half, old_conf_xml_contents, pos - old_conf_xml_contents + 1);
		first_half[pos - old_conf_xml_contents + 1] = '\0';

//printf(first_half);

		char * second_half = (char *)malloc((strlen(old_conf_xml_contents) - (pos + 1 - old_conf_xml_contents)
	                       + 1)  /* '\0'  */
	                       * sizeof(char));
		strncpy(second_half, pos + 1, strlen(old_conf_xml_contents) - (pos + 1 - old_conf_xml_contents));
		second_half[strlen(old_conf_xml_contents) - (pos + 1 - old_conf_xml_contents)] = '\0';

		FILE * conf_xml_file = fopen(conf_xml_filename, "w");
		if (conf_xml_file == NULL) {
			fprintf(stderr, "Couldn't open file %s for writing.\n", conf_xml_filename);
			return 1;
		}
		fprintf(conf_xml_file, "%s%s%s",
		        first_half, conf_xml_block, second_half);
		fclose(conf_xml_file);
		
		free(second_half);
		second_half = NULL;
		
		free(first_half);
		first_half = NULL;
	}
	free(append_after);
	append_after = NULL;
	
	free(conf_xml_block);
	conf_xml_block = NULL;
	

	/* now copy package/filename to data_dir/language_code */

	char * copy_command_base = "cp -r %s/%s %s/%s/";
	char * copy_command = (char *)malloc((strlen(package) + 1  /* '/'  */
	                      + strlen(copy_command_base)
	                      + strlen(filename)
	                      + strlen(data_dir) + 1  /* '/'  */
	                      + strlen(language_code) + 1  /* '/'  */
	                      - 8  /* "%s"s */
	                      + 1)  /* '\0'  */
	                      * sizeof(char));
	sprintf(copy_command, copy_command_base, package, filename, data_dir, language_code);
	system(copy_command);
	free(copy_command);
	copy_command = NULL;


	char * remove_command_base = "rm -r %s";
	char * remove_command = (char *)malloc((strlen(remove_command_base)
	                      + strlen(argv[1])
	                      - 1  /* '%s' */
	                      + 1)  /* '\0'  */
	                      * sizeof(char));
	sprintf(remove_command, remove_command_base, argv[1]);
	system(remove_command);
	free(remove_command);
	remove_command = NULL;

	
	free(language_code);
	language_code = NULL;
	free(language);
	language = NULL;
	free(filename);
	filename = NULL;
	free(copyright);
	copyright = NULL;
	free(author);
	author = NULL;
	free(type);
	type = NULL;
	
	free(conf_xml_filename);
	conf_xml_filename = NULL;
	
	printf("\n\nSuccessfully installed component %s.\n\n", argv[1]);
	return 0;
}

char * get_value_from_file(char * filename, char * package)
{
	char * value = NULL;
	char * filename_wpath = (char *)malloc((strlen(package) + 1 /* '/' */
	                        + strlen(filename)
	                        + 1) /* '\0' */
	                        * sizeof(char));
	sprintf(filename_wpath, "%s/%s", package, filename);
	
	return get_file_contents(filename_wpath);
}


char * get_file_contents(char * filename)
{
	char * contents = NULL;
	struct stat file_stat;
	if (stat(filename, &file_stat) != 0) {
		return NULL;
	}
	
	contents = (char *)malloc((file_stat.st_size + 1) * sizeof(char));
	
	FILE * file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "Couldn't open file %s for reading.\n", filename);
		return NULL;
	}

	fread(contents, file_stat.st_size, 1, file);
	contents[file_stat.st_size] = '\0';
	fclose(file);
	return contents;
}



char * create_lang_dir(char * language_code)
{
	struct passwd * current_user;
	char * data_directory;
	int c;
	libquran_ctx ctx;
	ctx.initialized = 0;
	c = quran_init(&ctx);

	switch (c) {
	case LIBQURAN_NO_CONF:
		current_user = (struct passwd *)getpwuid(getuid());
		char * home_config_file = (char *)malloc(strlen(current_user->pw_dir) + 10 /* /.quranrc\0 */ );
		sprintf(home_config_file, "%s/%s", current_user->pw_dir, "/.quranrc");
		char * config_filename = current_user->pw_uid == 0 ? "/etc/quran" : home_config_file;

		char * default_data_dir_user = (char *)malloc(strlen(current_user->pw_dir) + 12 /* /quran_data\0 */ );
		sprintf(default_data_dir_user, "%s/%s", current_user->pw_dir, "/quran_data");
		char * data_dir = current_user->pw_uid == 0 ? "/usr/share/quran_data" : default_data_dir_user;

		printf("There is no ./.quranrc, ~/.quranrc or /etc/quran configuration file found.\n");
		printf("This file should contain the path to the quran data directory that contain text and audio files for the Qur'an.\n");

		printf("One will be created for you in %s\n\n", config_filename);
		printf("What's the directory you wish to store quran data in (max = 1023 chars)\ne.g. %s?  ", data_dir);
		char buffer[1024] = "";
		scanf("%s", buffer);
		data_dir = buffer;
		
		FILE * file = fopen(config_filename, "w");
		if (file == NULL) {
			fprintf(stderr, "Couldn't open file %s for writing.\n", config_filename);
			exit(1);
		}
		fprintf(file, buffer);
		fclose(file);

		free(default_data_dir_user);
		default_data_dir_user = NULL;
		free(home_config_file);
		home_config_file = NULL;
		return NULL;

	case LIBQURAN_NO_DATADIR:
		if (mkdir(ctx.libquran.data_directory, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) < 0) {
			fprintf(stderr, "Couldn't make directory %s.", ctx.libquran.data_directory);
			exit(1);
		}
		return NULL;

	case LIBQURAN_NO_LANGS:
		{
			char * lang_dir = (char *)malloc((strlen(ctx.libquran.data_directory)
			                  + 1 /* '/' */
			                  + strlen(language_code) + 1 /* '\0' */) * sizeof(char));

			sprintf(lang_dir, "%s/%s", ctx.libquran.data_directory, language_code);
			if (mkdir(lang_dir, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) < 0) {
				fprintf(stderr, "Couldn't make directory %s.", lang_dir);
				return NULL;
			}
			free(lang_dir);
			lang_dir = NULL;
		}
		return ctx.libquran.data_directory;

	case LIBQURAN_SUCCEEDED:
		return ctx.libquran.data_directory;
	default:
		fprintf(stderr, "Couldn't initialize libquran, it returned error code: %d", c);
		exit(1);
	}

	// Should never get here
	return NULL;
}
