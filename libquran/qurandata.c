/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#include <stdio.h>
#include <expat.h>
#define _GNU_SOURCE
#include <string.h>

#include "quran.h"
#include "globals.h"

#include "qurandata.h"


/**************************************** Data ******************************************/


/* contains the places where Sajda is encountered in Qur'an */
const SuraAya sajda[14] = { {13,15}, {16,50}, {17,109},{19,58}, {22,18}, {22,77}, {25,60},
		      {27,26}, {32,15}, {38,24}, {41,38}, {53,62},  {84,21}, {96,19} };  //counting sura from 1 to 114, and aya from 1

/* an array of aya numbers */
const SuraAya hezb_quad[240] = {{2,25}, {2,43}, {2,59}, {2,74}, {2,91}, {2,105}, {2,123}, {2,141},   //Juzz 1 = hezb1 + hezb2
				{2,157}, {2,176}, {2,188}, {2,202}, {2,218}, {2,232}, {2,242}, {2,252},   //Juzz 2 = hezb3 + hezb4
				{2,262}, {2,271}, {2,282}, {3,14}, {3,32}, {3,51}, {3,74}, {3,92},   //Juzz 3 = hezb5 + hezb6
				{3,112}, {3,132}, {3,152}, {3,170}, {3,185}, {3,200}, {4,11}, {4,23},   //Juzz 4 = hezb7 + hezb8
				{4,35}, {4,57}, {4,73}, {4,87}, {4,99}, {4,113}, {4,134}, {4,147},   //Juzz 5 = hezb9 + hezb10
				{4,162}, {4,176}, {5,11}, {5,26}, {5,40}, {5,50}, {5,66}, {5,81},   //Juzz 6 = hezb11 + hezb12
				{5,96}, {5,108}, {6,12}, {6,35}, {6,58}, {6,73}, {6,94}, {6,110},   //Juzz 7 = hezb13 + hezb14
				{6,126}, {6,140}, {6,150}, {6,165}, {7,30}, {7,46}, {7,64}, {7,87},   //Juzz 8 = hezb15 + hezb16
				{7,116}, {7,141}, {7,155}, {7,170}, {7,188}, {7,206}, {8,21}, {8,40},   //Juzz 9 = hezb17 + hezb18
				{8,60}, {8,75}, {9,18}, {9,33}, {9,45}, {9,59}, {9,74}, {9,92},   //Juzz 10 = hezb19 + hezb20
				{9,110}, {9,121}, {10,10}, {10,25}, {10,52}, {10,70}, {10,89}, {11,5},   //Juzz 11 = hezb21 + hezb22
				{11,23}, {11,40}, {11,60}, {11,83}, {11,107}, {12,6}, {12,29}, {12,52},   //Juzz 12 = hezb23 + hezb24
				{12,76}, {12,100}, {13,4}, {13,18}, {13,34}, {14,9}, {14,27}, {14,52},   //Juzz 13 = hezb25 + hezb26
				{15,48}, {15,99}, {16,29}, {16,50}, {16,74}, {16,89}, {16,110}, {16,128},   //Juzz 14 = hezb27 + hezb28
				{17,22}, {17,49}, {17,69}, {17,98}, {18,16}, {18,31}, {18,50}, {18,74},   //Juzz 15 = hezb29 + hezb30
				{18,98}, {19,21}, {19,58}, {19,98}, {20,54}, {20,82}, {20,110}, {20,135},   //Juzz 16 = hezb31 + hezb32
				{21,28}, {21,50}, {21,82}, {21,112}, {22,18}, {22,37}, {22,59}, {22,78},   //Juzz 17 = hezb33 + hezb34
				{23,35}, {23,74}, {23,118}, {24,20}, {24,34}, {24,52}, {24,64}, {25,20},   //Juzz 18 = hezb35 + hezb36
				{25,52} ,{25,77} ,{26,51} ,{26,110} ,{26,180} ,{26,227} ,{27,26} ,{27,55},   //Juzz 19 = hezb37 + hezb38
				{27,81}, {28,11}, {28,28}, {28,50}, {28,75}, {28,88}, {29,25}, {29,45},   //Juzz 20 = hezb39 + hezb40
				{29,69}, {30,30}, {30,53}, {31,21}, {32,10}, {32,30}, {33,17}, {33,30},   //Juzz 21 = hezb41 + hezb42
				{33,50}, {33,59}, {34,9}, {34,23}, {34,45}, {35,14}, {35,40}, {36,27},   //Juzz 22 = hezb43 + hezb44
				{36,59}, {37,21}, {37,82}, {37,144}, {38,20}, {38,51}, {39,7}, {39,31},   //Juzz 23 = hezb45 + hezb46
				{39,52}, {39,75}, {40,20}, {40,40}, {40,65}, {41,8}, {41,24}, {41,46},   //Juzz 24 = hezb47 + hezb48
				{42,12}, {42,26}, {42,50}, {43,23}, {43,56}, {44,16}, {45,11}, {45,37},   //Juzz 25 = hezb49 + hezb50
				{46,20}, {47,9}, {47,32}, {48,17}, {48,29}, {49,13}, {50,26}, {51,30},   //Juzz 26 = hezb51 + hezb52
				{52,23}, {53,25}, {54,8}, {54,55}, {55,78}, {56,74}, {57,15}, {57,29},   //Juzz 27 = hezb53 + hezb54
				{58,13}, {59,10}, {60,6}, {61,14}, {63,3}, {64,18}, {65,12}, {66,12},   //Juzz 28 = hezb55 + hezb56
				{67,30}, {68,52}, {70,18}, {71,28}, {73,19}, {74,56}, {76,18}, {77,50},   //Juzz 29 = hezb57 + hezb58
				{79,46}, {81,29}, {83,36}, {86,17}, {89,30}, {93,11}, {100,8}, {114,6},   //Juzz 30 = hezb59 + hezb60
				};

/* an array of aya numbers, that indicates how many ayas every sura contains */
const int aya_numbers[AMOUNT_SURAS]= {7, 286, 200, 176, 120, 165, 206, 75, 129, 109,
				123, 111, 43, 52, 99, 128, 111, 110, 98, 135, 112, 78, 118, 64, 77, 227,
				93, 88, 69, 60, 34, 30, 73, 54, 45, 83, 182, 88, 75, 85, 54, 53, 89, 59,
				37, 35, 38, 29, 18, 45, 60, 49, 62, 55, 78, 96, 29, 22, 24, 13, 14, 11,
				11, 18, 12, 12, 30, 52, 52, 44, 28, 28, 20, 56, 40, 31, 50, 40, 46, 42,
				29, 19, 36, 25, 22, 17, 19, 26, 30, 20, 15, 21, 11, 8, 8, 19, 5, 8, 8,
				11, 11, 8, 3, 9, 5, 4, 7, 3, 6, 3, 5, 4, 5, 6};


/********************************* Published functions **********************************/


/**
 * Gets quran by languages.
 * Initializes data structure and loads the quran from an xml file
 * to it and returns it.
 *
 * \param lang quran language to open
 * \param file filename of the xml to open for that language
 * \param libquran_ctx quran context
 *
 * \return NULL when failed opening quran XML file, otherwise quran handle
 *
 * \sa quran_open_index(), quran_read_verse(), quran_close()
 **/
quran *quran_open(const char * lang, const char * text_file, libquran_ctx *ctx) {

	int lang_index;
	int text_file_index;

	/* Check the required variables */
	if ( (ctx->initialized!=1)) {
		ctx->error_code = LIBQURAN_NOT_INITIALIZED;
	 	return NULL;
	}
	if (!lang || !text_file ) {
		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	lang_index = langcode_to_index(lang, ctx);
	if (lang_index == LIBQURAN_NO_SUCH_LANG) {
		ctx->error_code = lang_index;
	    return NULL;
	}
	text_file_index = text_file_to_index(lang_index, text_file, ctx);
	if (text_file_index == LIBQURAN_NO_SUCH_LANG  ||  text_file_index == LIBQURAN_NO_SUCH_TEXT) {
		ctx->error_code = text_file_index;
	    return NULL;
	}
	return quran_open_index(lang_index, text_file_index, ctx);
		
}

/**
 * Gets quran by languages.
 * Initializes data structure and loads the quran from an xml file
 * to it and returns it.
 *
 * \param lang quran language index to open
 * \param file index of the filename of the xml to open for that language
 * \param libquran_ctx quran context
 *
 * \return NULL when failed opening quran XML file, otherwise quran handle
 *
 * \sa quran_open(), quran_read_verse(), quran_close()
 **/
quran * quran_open_index(const int lang, const int text, libquran_ctx *ctx) {

	quran *temp;
	char *location;

	/* Check the required variables */
	if (ctx->initialized!=1) {
		ctx->error_code = LIBQURAN_NOT_INITIALIZED;
		return NULL;
	}
	
	if ((lang<0) || (text<0)) {
		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	if ( (ctx->libquran.num_langs<1) ||
			 (ctx->libquran.avail_langs[lang].num_texts<(text+1)) ||
			 (!(temp=(quran *)malloc(sizeof(quran)))) ) {

		ctx->error_code = LIBQURAN_ERROR_INVALID_ARGUMENTS;
		return NULL;
	}

	/* Copy language */
	temp->lang = strdup(ctx->libquran.avail_langs[lang].avail_texts[text].filename);

	location = (char *)malloc(strlen(ctx->libquran.data_directory) +
				  strlen(ctx->libquran.avail_langs[lang].code) +
				  strlen(ctx->libquran.avail_langs[lang].avail_texts[text].filename) +
				  2  /* 2 slashes */
				 );


	if (!location) {
		/* Failed allocating */
		free(temp);
		temp=NULL;
		ctx->error_code = LIBQURAN_NO_MEM;
		return NULL;
	}

	/* Quran XML file path */
#if defined(_WIN32) && defined(_MINGW)
	sprintf(location,"%s\\%s\\%s", ctx->libquran.data_directory,
#else
	sprintf(location,"%s/%s/%s", ctx->libquran.data_directory,
#endif
		ctx->libquran.avail_langs[lang].code,
		ctx->libquran.avail_langs[lang].avail_texts[text].filename
	       );
	/* Open the XML file */
	if (!(temp->file_handle=fopen(location,"r"))) {
		/* Could not open the XML file */
		free(location);
		free(temp);
		temp=NULL;
		ctx->error_code = LIBQURAN_FILE_OPEN_FAILED;
		return NULL;
	}
	/* Set properties */
	temp->credits = NULL;
	temp->has_search_tag = 0;

	/* Load Quran from the opended file */
	build_sura(temp);

	fclose(temp->file_handle);
	free(location);

	return temp;
}

/**
 * Get the text of an aya by sura number and aya number.
 *
 * NOTE: that user must free the returned value
 *
 * \param q quran handle
 * \param sura_no sura index (1-114)
 * \param aya_no aya index (>1)
 * \param libquran_ctx quran context
 *
 * \return NULL by invalid sura or aya, otherwise aya text
 *
 * \sa quran_open(), quran_close()
 **/
char *quran_read_verse(const quran *q, int sura_no, int aya_no, libquran_ctx *ctx) {

	int i;
	char *aya_read=NULL;
	aya *a;

	/* Check the required variables */
	if ( (ctx->initialized!=1) ||
			 (q==NULL) ) {

		return NULL;
	}

	/* Convert to C numbers order (from '0' and not from '1') */
	sura_no--;

	/* Check the required variables */
	if ( (sura_no<0 || sura_no>113) ||
			 (aya_no<0 || (aya_no>aya_numbers[sura_no])) ) {

		return NULL;
	}

	/* Get sura search text */
	a=q->sura[sura_no];

	/* Convert to C numbers order (from '0' and not from '1') */
	aya_no--;

		/* Get the wanted aya from the current (wanted) sura */
	if (aya_no>0) {
		for (i=0;i<aya_no;i++) {
			a = a->next;
		}
	}

		/* Get aya text */
	if (a!=NULL) {
		aya_read=strdup(a->text);
	}

	return aya_read;
}

/**
 * Close quran handle and frees memory.
 *
 * \param q quran handle
 * \param libquran_ctx quran context
 *
 * \sa quran_read_verse(), quran_close()
 **/
void quran_close(quran *q, libquran_ctx *ctx) {

	int i;

	/* Check the required variables */
	if ( (ctx->initialized!=1) ||
			 (!q) ){
				 
		return;
	}

	/* Freeing all memory used by all ayahs */
	for (i=0;i<AMOUNT_SURAS;i++) {
		free_aya(q->sura[i]);
		if (q->sura_titles[i])
			free(q->sura_titles[i]);
	}

	/* Freeing all memory used by the search text */
	if (q->has_search_tag) {
		for (i=0;i<AMOUNT_SURAS;i++) {
			free_aya(q->sura_search[i]);
		}
	}

	/* free memory used by credits */
	if (q->credits!=NULL) {
		 free(q->credits);
 	}

	/* free memory used by language */
	free(q->lang);

	/* free the quran itself */
	free(q);
}

/**
 * Gets information about a sura by sura number
 *
 * NOTE: that user must free the returned value
 *
 * \param sura_no sura index (1-114)
 *
 * \return NULL by invalid sura number, otherwise the sura_info data structure.
 *
 * \sa quran_verse_info()
 **/
sura_info *quran_sura_info(int sura_no) {
	
	sura_info *temp = (sura_info*) malloc(sizeof(sura_info));

	if (!temp) {
		/* Failed allocating */
		return NULL;
	}
	
	if ( sura_no<1 || sura_no>AMOUNT_SURAS ) {
		/* invalid sura number */
		return NULL;
	}
  /* Set properties */
	temp->max_aya = aya_numbers[sura_no-1];

	return temp;
}

/**
 * Gets information about an aya by sura number and aya number
 *
 * NOTE: that user must free the returned value
 *
 * \param sura_no sura index (1-114)
 * \param aya_no aya index (>1)
 *
 * \return NULL by invalid sura number or aya number, otherwise the ayainfo data structure.
 *
 * \sa quran_sura_info()
 **/
ayainfo * quran_verse_info(int sura_no, int aya_no) {
	
	int count = 0;
	int tempcalc = 0;

	ayainfo * temp = malloc(sizeof(ayainfo));

	/* Check the required variables */
	if ( (sura_no<1 || sura_no>114) ||
			 (aya_no<1 || (aya_no>aya_numbers[sura_no-1])) ) {
		 return NULL;
	}

	/* Determine if this aya has a sajda */
	temp->has_sajda = 0;
	for (count=0; count<15; count++) {
		if ((sajda[count].sura_no == sura_no) && (sajda[count].aya_no == aya_no)) {
			temp->has_sajda = 1;
		}
	}

	temp->juz_number = 0;
	temp->hezb_number = 0;
	temp->hezb_quad = 0;

	for (count=0; count<240; count++) {
		if ( (sura_no > hezb_quad[count].sura_no) ||
		     ( (aya_no >= aya_numbers[hezb_quad[count].sura_no-1]) && ( hezb_quad[count].aya_no > aya_no) ) 
		   ) {
			/* Current hezb quad sura/aya number is lower than the wanted
				 sura/aya number, so ignore current */
			continue;
		}

		/* Set juzz number */
		if ((count+1)<= 8) {
			temp->juz_number = 1;
		} else {
			if ((count+1)%8) {
				temp->juz_number = ((count+1)/8) + 1;
			} else {
				temp->juz_number = (count+1)/8;
			}
		}

		/* Set the hebz number and quad number */
		if ((count+1)<= 4) {
			temp->hezb_number = 1;
			temp->hezb_quad = count + 1;
		} else {
			if ((count+1)%4) {
				temp->hezb_number = ((count+1)/4) + 1;
				temp->hezb_quad = (count+1)%4;
			} else {
				temp->hezb_number = (count+1)/4;
				temp->hezb_quad = 4;
			}
		}
		break;
	}
	return temp;
}


/********************************** Internal functions **********************************/


/**
 * Internal function: It builds sura datastructure from XML file.
 * all quran data will be loaded to memory by this function.
 *
 * \param *q quran handle where all the quran data will be saved to
 *
 * \return 0 on success and -1 on error
 **/
int build_sura(quran *q) {

	XML_Parser p;
	q_temp *q_;

	/* Initializes the members of the q_temp structure.
	   This structure is used to remember during the parsing process the quran handle
	   and what kind of tag we're currently handling */
	q_ = malloc(sizeof(q_temp));
        q_->text = NULL;
	q_->sura_no = 0;
	q_->aya_no = 1;
	q_->q = q;

	/* Initializes the parser */
	p = XML_ParserCreate(NULL);
	XML_SetElementHandler(p, callback_start, callback_end);
	XML_SetCharacterDataHandler(p, callback_cdata);
	XML_SetUnknownEncodingHandler(p, callback_enc, NULL);
	XML_SetUserData(p, q_);

	for (;;) {
		int bytes_read;

		/* Get the buffer of the parser */
		void *buff = XML_GetBuffer(p, BUFF_SIZE);
		if (buff == NULL) {
			return -1;
		}

		/* Read data from file to the buffer of the parser */
		bytes_read = fread(buff, 1, BUFF_SIZE, q->file_handle);
		if (bytes_read < 0) {
			return -1; /* Error reading file */
		}

		/* Parse the data in the parser buffer */
		if (! XML_ParseBuffer(p, bytes_read, bytes_read == 0)) {
			return -1;
		}

		if (bytes_read == 0) {
			break; /* End Of File */
		}
	}
	
	q_->q=NULL;
	free(q_);
	XML_ParserFree(p);
	
	return 0;
}

/**
 * Internal function: This callback handles character encoding other than US-ASCII, UTF-8,
 * UTF-16, and ISO-8859-1. TODO: this function only handles turkish
 * (ISO-8859-9)
 *
 * \param *enc_data custom user data (currently NULL)
 * \param *name encoding name (ie 'iso-8859-9')
 * \param *info data structure contains information on how to convert the encoding
 *
 * \return 0 on error and 1 on success
 **/
int callback_enc(void *enc_data, const XML_Char *name, XML_Encoding *info) {
	int i;
	
	if (strcasecmp(name,"ISO-8859-9") == 0 || strcasecmp(name,"ISO8859-9") == 0) {
		if (info) {
			/* turkish iso-8859-9 is single byte charset */
			for (i=0;i<256;i++) {
				info->map[i] = i;
			}
			return 1;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}


/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML start tag.
 * It handles the known tags and saves its data to the quran handle
 *
 * \param *user_data user data
 * \param *name tag name
 * \param **attrs attributes
 *
 * \sa callback_cdata(), callback_end()
 **/
void callback_start(void *user_data, const XML_Char *name, const XML_Char **attrs) {

	q_temp *q_ = (q_temp *)user_data;

        if (q_->text != NULL) {
          free(q_->text);
	  q_->text = NULL;
	}

	/* saving translators names */
	if (strncmp((char*)name,"translatedby",12)==0) {
		while (attrs && *attrs) {
			if (strncmp(attrs[0],"name",4)==0) {
				char *credits=strdup(attrs[1]);
				q_->q->credits = credits;
			}
			attrs += 2;
		}
	}

	/* saving sura names */
	else if (strncmp((char*)name,"sura",4)==0) {
		while (attrs && *attrs) {
			if (strncmp(attrs[0],"name",4)==0) {
				char *sura_title=strdup(attrs[1]);
				q_->q->sura_titles[q_->sura_no] = sura_title;
			}
			attrs += 2;
		}
	}
}

/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML CData.
 * It handles the data by its tag and saves it to the quran handle
 *
 * \param *user_data user data
 * \param *data data string
 * \param len length of the data string
 *
 * \sa callback_start(), callback_end()
 **/
void callback_cdata(void *user_data, const XML_Char *data, int len) {

	q_temp *q_ = (q_temp *)user_data;
	quran *q;
        int flag = 0;
        int count=0;
        for (count=0; count<len; count++)
          if (!isspace(data[count])) {
            flag = 1;
            break;
          }
       if (flag == 0)
         return;

       /* If q_->text is set to NULL, then there is no text to append to */
       if (q_->text == NULL)
         q_->text = (char *)strndup(data,len);
       
       /* If q_->text already has some text, append the new text to that text */
       else {
         int currentTextLen = strlen(q_->text);
         /* Save the current text in a new temporary pointer */
         char * currentText = q_->text;
         /* Allocate memory for the current text plus the new text */
         q_->text = (char *)malloc(currentTextLen + len + 1);
         /* Copy the current text to the new allocated space */
         strncpy(q_->text, currentText, currentTextLen);
         free(currentText);
         currentText = NULL;
         q_->text[currentTextLen] = '\0';
         /* Finally, append the new text to the current text */
         strncat(q_->text, data, len);
       }
}

/**
 * Internal function: This callback function is used when parsing the xml data.
 * Called when the parser finds an XML end tag.
 *
 * \param *user_data user data
 * \param *name tag name
 *
 * \sa callback_start(), callback_cdata()
 **/
void callback_end(void *user_data, const XML_Char *name) {
	quran * q;
	q_temp *q_ = (q_temp *)user_data;
	
	if (strncmp((char*)name,"sura",4)==0) {
		q_->aya_no = 1;

		if (q_->sura_no>AMOUNT_SURAS) {
			/* There are not more than 114 sura's */
			fprintf(stderr,"wrong number of sura %d\n", q_->sura_no);
			return;
		} else {
			/* Increase the sura number */
			q_->sura_no++;
		}
	}
	
	/* increase aya number of this sura */
	else if (strncmp((char*)name,"aya",3)==0) {
		q_->aya_no++;
	}

	else if (strncmp((char*)name,"qurantext",9)==0) {
		aya *a,*last_aya;
		/* Quran handle */
		q = q_->q;

		/* Aya handle */
		a = (aya*)malloc(sizeof(aya));
		a->next = NULL;

		/* Take the quran text and reset the current text to NULL for the next verse */
		a->text= q_->text;
		q_->text = NULL;

		if (q_->aya_no>1) {
			/* next aya, save it as a next aya of it's previous */
			last_aya=q_->last_aya;
			last_aya->next = a;
		} else {
			/* first aya of the sura, save it directly in the quran handle */
			q->sura[q_->sura_no] = a;
		}

		q_->last_aya = a;
	}

	/* and text between <searchtext></searchtext> tags */
	else if (strncmp((char*)name,"searchtext",10)==0) {

		aya *a,*last_search;
		/* Quran handle */
		q = q_->q;

		/* Aya handle */
		a = (aya*)malloc(sizeof(aya));
		a->next = NULL;

		/* Take the search text and reset the current text to NULL for the next verse */
		a->text= q_->text;
		q_->text = NULL;

		if (q_->aya_no>1) {
			/* next aya, save it as a next aya of it's previous */
			last_search=q_->last_search;
			last_search->next = a;
		} else {
			/* first aya of the sura, save it directly in the quran handle */
			q->sura_search[q_->sura_no] = a;
		}

		q_->last_search = a;
		q_->q->has_search_tag = 1;
	}

}

/**
 * Internal function: Frees all memory used by ayas of a sura
 *
 * \param a aya location
 **/
void free_aya(aya *a) {

	if (a==NULL) {
		return;
	}

	/* Free the aya's recursively */
	if (a->next!=NULL) {
		free_aya(a->next);
	}
	
	free(a->text);
	free(a);
}
