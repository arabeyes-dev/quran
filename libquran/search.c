/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#include <stdio.h>
#include <expat.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#define _GNU_SOURCE
#include <string.h>

#include "quran.h"
#include "qurandata.h"
#include "globals.h"

#include "search.h"

/********************************* Published functions **********************************/


/**
 * Searches the whole quran for a part of text.
 * You can get the results of the search by calling quran_get_search_results()
 *
 * \param q quran handle
 * \param search_string the string to search for
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return amount of results found (>=0), otherwise error (<0)
 * Errors: LIBQURAN_ERROR_INVALID_ARGUMENTS, LIBQURAN_ERROR_OUT_OF_RANGE, LIBQURAN_NOT_INITIALIZED
 *
 * \sa searchTypes, EXACT_MATCH, quran_get_search_results(), quran_free_search_results(),
 *     quran_search_sura(), quran_search_juzz(), quran_search_hezb(), quran_search_part()
 **/
int quran_search_quran(quran *q, const char *search_string, int type, int similarity, libquran_ctx *ctx) {

	int begin_sura, begin_aya, end_sura, end_aya;

	/* Calculate position of the the whole quran */
	begin_sura = 1;
	begin_aya = 1;
	end_sura = AMOUNT_SURAS;
	end_aya = aya_numbers[AMOUNT_SURAS-1];

	/* Search this part */
	return quran_search_part(q, search_string, begin_sura, begin_aya, end_sura, end_aya, type , similarity, ctx);
}

/**
 * Searches a whole sura for a part of text.
 * You can get the results of the search by calling quran_get_search_results()
 *
 * \param q quran handle
 * \param search_string the string to search for
 * \param sura_no index number of the sura (1..AMOUNT_SURAS)
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return amount of results found (>=0), otherwise error (<0)
 * Errors: LIBQURAN_ERROR_INVALID_ARGUMENTS, LIBQURAN_ERROR_OUT_OF_RANGE, LIBQURAN_NOT_INITIALIZED
 *
 * \sa searchTypes, EXACT_MATCH, quran_get_search_results(), quran_free_search_results(),
 *     quran_search_quran(), quran_search_juzz(), quran_search_hezb(), quran_search_part()
 **/
int quran_search_sura(quran *q, const char *search_string, int sura_no, int type, int similarity, libquran_ctx *ctx) {

	int begin_sura, begin_aya, end_sura, end_aya;

	/* Check the required variables */
	if (sura_no < 1 || sura_no > AMOUNT_SURAS) {
		return LIBQURAN_ERROR_OUT_OF_RANGE;
	}

	/* Calculate position of the sura */
	begin_sura = sura_no;
	begin_aya = 1;
	end_sura = sura_no;
	end_aya = aya_numbers[sura_no-1];

	/* Search the calculated part */
	return quran_search_part(q, search_string, begin_sura, begin_aya, end_sura, end_aya, type , similarity, ctx);
}

/**
 * Searches a whole juzz for a part of text.
 * You can get the results of the search by calling quran_get_search_results()
 *
 * \param q quran handle
 * \param search_string the string to search for
 * \param juzz_no index number of the hezb (1..AMOUNT_JUZZ)
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return amount of results found (>=0), otherwise error (<0)
 * Errors: LIBQURAN_ERROR_INVALID_ARGUMENTS, LIBQURAN_ERROR_OUT_OF_RANGE, LIBQURAN_NOT_INITIALIZED
 *
 * \sa searchTypes, EXACT_MATCH, quran_get_search_results(), quran_free_search_results(),
 *     quran_search_quran(), quran_search_sura(), quran_search_hezb(), quran_search_part()
 **/
int quran_search_juzz(quran *q, const char *search_string, int juzz_no, int type, int similarity, libquran_ctx *ctx) {

	int begin_sura, begin_aya, end_sura, end_aya;

	/* Check the required variables */
	if (juzz_no < 1 || juzz_no > AMOUNT_JUZZ) {
		return LIBQURAN_ERROR_OUT_OF_RANGE;
	}

	/* Calculate position of the juzz */
	if (juzz_no==1) {
	    begin_sura = 1;
	    begin_aya = 1;
	} else {
	    begin_sura = hezb_quad[ (juzz_no - 1) * 8 ].sura_no;
	    begin_aya = hezb_quad[ (juzz_no - 1) * 8 ].aya_no;
	}
	end_sura = hezb_quad[ juzz_no * 8 ].sura_no;
	end_aya = hezb_quad[ juzz_no * 8 ].aya_no;

	/* Search the calculated part */
	return quran_search_part(q, search_string, begin_sura, begin_aya, end_sura, end_aya, type , similarity, ctx);
}

/**
 * Searches a whole hezb for a part of text.
 * You can get the results of the search by calling quran_get_search_results()
 *
 * \param q quran handle
 * \param search_string the string to search for
 * \param hezb_no index number of the hezb (1..AMOUNT_HEZBS)
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return amount of results found (>=0), otherwise error (<0)
 * Errors: LIBQURAN_ERROR_INVALID_ARGUMENTS, LIBQURAN_ERROR_OUT_OF_RANGE, LIBQURAN_NOT_INITIALIZED
 *
 * \sa searchTypes, EXACT_MATCH, quran_get_search_results(), quran_free_search_results(),
 *     quran_search_quran(), quran_search_sura(), quran_search_juzz(), quran_search_part()
 **/
int quran_search_hezb(quran *q, const char *search_string, int hezb_no, int type, int similarity, libquran_ctx *ctx) {

	int begin_sura, begin_aya, end_sura, end_aya;
	
	/* Check the required variables */
	if (hezb_no < 1 || hezb_no > AMOUNT_HEZBS) {
		return LIBQURAN_ERROR_OUT_OF_RANGE;
	}

	/* Calculate position of the hezb */
	if (hezb_no==1) {
	    begin_sura = 1;
	    begin_aya = 1;
	} else {
	    begin_sura = hezb_quad[ ( hezb_no - 1 ) * 4 ].sura_no;
	    begin_aya = hezb_quad[ ( hezb_no - 1 ) * 4 ].aya_no;
	}
	end_sura = hezb_quad[ hezb_no * 4 ].sura_no;
	end_aya = hezb_quad[ hezb_no * 4 ].aya_no;

	/* Search the calculated part */
	return quran_search_part(q, search_string, begin_sura, begin_aya, end_sura, end_aya, type , similarity, ctx);
}

/**
 * Searches a part of quran for a part of text.
 * You can get the results of the search by calling quran_get_search_results()
 *
 * \param q quran handle
 * \param search_string the string to search for
 * \param begin_sura the sura where the quran part begins where to search in
 * \param aya_sura the aya where the quran part begins where to search in
 * \param begin_end the sura where the quran part ends where to search in
 * \param aya_end the aya where the quran part ends where to search in
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return amount of results found (>=0), otherwise error (<0)
 * Errors: LIBQURAN_ERROR_INVALID_ARGUMENTS, LIBQURAN_ERROR_OUT_OF_RANGE, LIBQURAN_NOT_INITIALIZED
 *
 * \sa searchTypes, EXACT_MATCH, quran_get_search_results(), quran_free_search_results(),
 *     quran_search_quran(), quran_search_sura(), quran_search_juzz(), quran_search_hezb()
 **/
int quran_search_part(quran *q, const char *search_string, int begin_sura, int begin_aya, int end_sura, int end_aya, int type, int similarity, libquran_ctx *ctx) {

	int match_count = 0;
	int current_aya_no = 0;
	int i, j;
	aya *current_aya;
	results_struct *results_root = NULL, *results_current = NULL;
	int result_simularity;

	/* Check the required variables */
	if ( (begin_sura < 1 || end_sura > AMOUNT_SURAS) ||
		(begin_aya < 1 || end_aya > aya_numbers[end_sura-1]) ){

		return LIBQURAN_ERROR_OUT_OF_RANGE;
	}

	/* Check if libuquran is initialized */
	if (ctx->initialized!=1) {
		return LIBQURAN_NOT_INITIALIZED;
	}

	/* Check the required variables */
	if ( (q == NULL) ||
		(search_string == NULL) ||
		(strlen(search_string) <= 0) ||
		(type < ALL_WORDS || type > COMPLETE_PHRASE) ||
		(similarity < 0 || similarity > 100) ) {

		return LIBQURAN_ERROR_INVALID_ARGUMENTS;
	}

	// TODO: What if the search string is seperated on more ayas

	for (i = begin_sura; i <= end_sura; i++) {

		/* Get the first aya of a sura */
		current_aya = (q->has_search_tag && (type!=PHONETICAL)) ? q->sura_search[i-1]: q->sura[i-1];

		current_aya_no = 1;


		/* Get the begin aya by the begin sura */
/*		if (i == begin_sura) {
			for (j = 1; j < begin_aya; j++) {
				current_aya = current_aya->next;
			}
		}
*/
		while (current_aya && current_aya_no<=aya_numbers[i-1]) {

			if (result_simularity = match(search_string, current_aya->text, type, similarity)) {

				match_count++;

				/* Create results_struct object */
				if (!results_current) {
					/* First aya */
					results_current = (results_struct *)malloc(sizeof(results_struct));
					results_root = results_current;
				} else {
					/* Not the first aya */
					results_current->next = (results_struct *)malloc(sizeof(results_struct));
					results_current = results_current->next;
				}
				results_current->next = NULL;

				/* save the position */
				results_current->sura_no = i;
				results_current->aya_no = current_aya_no;
				results_current->simularity = result_simularity;

			}

			/*Go to the next aya*/
			current_aya = current_aya->next;
			current_aya_no++;
		}
	}

	/* Free ond results */
	if (ctx->results) {
	 quran_free_search_results(ctx);
	}

	/* Convert the linked list to a dynamic array */
	if (match_count>0) {
		ctx->results = (results_struct **)malloc(sizeof(results_struct *) * match_count);
		results_current = results_root;
		for (i = 0; i < match_count; i++) {
			ctx->results[i] = results_current;
			results_current = results_current->next;
		}
	}

	return match_count;
}

/**
 * Gets the search results, that where generated by a search function.
 *
 * NOTE: Don't forget to free the search result by calling quran_free_search_results()!
 *
 * \return a dynamic array that contains the search results
 * \param libquran_ctx quran context
 *
 * \sa quran_free_search_results(), quran_search_quran(), quran_search_sura(),
 *     quran_search_juzz(), quran_search_hezb(), quran_search_part()
 **/
results_struct **quran_get_search_results(libquran_ctx *ctx) {

	return ctx->results;
}

/**
 * Frees the search results.
 *
 * \param libquran_ctx quran context
 *
 * \sa quran_get_search_results(), quran_search_quran(), quran_search_sura(),
 *     quran_search_juzz(), quran_search_hezb(), quran_search_part()
 **/
void quran_free_search_results(libquran_ctx *ctx) {

	results_struct *results_current, *results_delete;

	if (ctx->results) {

		/* Get the begin point of the linked list */
		results_current = ctx->results[0];

		/* Navigate the linked list and free every element in it */
		while(results_current) {
			results_delete = results_current;
			results_current = results_current->next;
			free(results_delete);
		}

		/* Free the dynamic array */
		free(ctx->results);
		ctx->results = NULL;
	}

}


/********************************** Internal functions **********************************/


/**
 * Internal function: Checks if the search string matches a part of an aya
 * using the giving search type and similarity
 *
 * \param search_string the string to search for
 * \param aya_end the aya where to search in
 * \param type search type: ALL_WORDS, ANY_WORD or COMPLETE_PHRASE
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return 1 if they matches, otherwise 0
 **/
int match(const char *search_string, const char *aya_text, int type, int similarity) {

	int return_value = (type==ALL_WORDS) ? 1: 0;
	char *word, *phrase;

	if (type==COMPLETE_PHRASE) {
	/* Search for a complete phrase */

		return_value = compare_text(search_string, aya_text, similarity);

	} else if (type==PHONETICAL) {
	/* phonetical search */

		return_value = compare_phonetical(search_string, aya_text, similarity);

	} else {
	/* Search for all words/any word */

		/* words found counter */
		int words_found = 0;
		int result_similarity;

		phrase = strdup(search_string);

		/* Seperate words for search */
		word = strtok(phrase, DELIMITER);

		while(word) {

			if (type==ANY_WORD) {

				/* Search for any word */
				if ( (return_value=compare_word(word, aya_text, similarity)) != 0 ) {
					/* One of the words is found, break the search */
					words_found++;
					break;
				}

			} else if (type==ALL_WORDS) {

				/* Search for all word */
				if ( ( result_similarity = compare_word(word, aya_text, similarity)) != 0 ) {
					return_value += result_similarity;
					words_found++;
				} else {
					/* One of the words is not found, break the search */
					return_value = 0;
					break;
				}

			}
			/* Next word to compare */
			word = strtok(NULL, DELIMITER);
		}

		free(phrase);

		/* Calculate the average similarity */
		if (return_value!=0) {
			return_value /= words_found;
		}
	}

	return return_value;
}

/**
 * Internal function: Checks if the phonetical sound of a search string matches a part
 * of an aya using similarity
 *
 * \param search_string the string to search for
 * \param aya_end the aya where to search in
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return 1 if they matches, otherwise 0
 **/
int compare_phonetical(const char *search_string, const char *aya_text, int similarity) {

	// TODO:
	// 1. remove all vocals from search_string
	// 2. remove all normal characters that have a vocal function but have no
	// vocals on it from aya_text (like wa, alif, ya')
	// 3. remove all vocals from aya_text
	// 4. convert the remaining characters from aya_text to alfabet
	// (If this turn to be very slow, we can generate the converted aya_text (2,3,4)
	// in de xml file)
	// I think by similarity of approximately 75%, good results will be found

	return compare_text(search_string, aya_text, similarity);
}

/**
 * Internal function: Checks if the search string matches a part of an aya
 * using similarity
 *
 * \param search_string the string to search for
 * \param aya_end the aya where to search in
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return the similarity percentage if found, otherwise 0
 **/
int compare_text(const char *search_string, const char *aya_text, int similarity) {

	int return_value = 0;

	/* To save the buffer for strtok_r */
	char *buffer;
	char *aya_part;
	char *aya_word;

	/* Set the strings to low case for (levenshtein) comparing */
	char *lsearch_string = lcase(strdup(search_string));
	char *laya_text = lcase(strdup(aya_text));

	/* Count how many words are in the search string */
	int amount_words = 0;
	char *tokstr = strdup(lsearch_string);
	char *search_word = strtok_r(tokstr, DELIMITER, &buffer);

	while (search_word) {
		amount_words++;
		search_word = strtok_r(NULL, DELIMITER, &buffer);
	}
	free(tokstr);

	/* Create a string that contains a part of the aya with the same amount of
	   words as the search string */
	aya_part = malloc(strlen(laya_text) + 1);
	aya_part[0] = 0;
	aya_word = strtok_r(laya_text, DELIMITER, &buffer);

	while( aya_word!=NULL && amount_words>0 ) {
		amount_words--;
		/* Add next word */
		if (strlen(aya_part)>0) {
			strcpy(aya_part + strlen(aya_part), " \0");
		}
		strcpy(aya_part + strlen(aya_part), aya_word);
		/* Stop getting tokens, when the required amount of words for aya part is reached */
		if (amount_words>0) {
			aya_word = strtok_r(NULL, DELIMITER, &buffer);
		}
	}

	/* Only for this to start the while lus */
	aya_word = (char *)1;

	while(aya_word!=NULL) {		
		if (similarity==EXACT_MATCH) {
			/* Exact match (100%) means no similarity. Just compare */
			if (strcmp(lsearch_string, aya_part)==0) {
				return_value = EXACT_MATCH;
				break;
			}

		} else {
			
			/* The maximal distance is the amount of replacing the all characters of the
			   source string and adding all the missing characters to the source string or
			   removing all the extra characters */
			int max_distance = (strlen(lsearch_string)>strlen(aya_part)) ? strlen(lsearch_string): strlen(aya_part);

			/* Calculate the distance between source and target */
			int distance = levenshtein_distance(lsearch_string, aya_part);

			/* Calculate the similarity from the distance */
			int similarity_percentage = (1 - (double)distance / (double)max_distance) * 100;

			/* Does it match the similarity */
			if (similarity_percentage >= similarity) {
				return_value = similarity_percentage;
				break;
			}

		}
		/* Get the next word of aya */
		aya_word = strtok_r(NULL, DELIMITER, &buffer);
		
		if (aya_word!=NULL) {
			
			if (strchr(aya_part, ' ')) {
			    // Remove the first word
			    strcpy(aya_part, strchr(aya_part, ' ') + 1);
			    // Push back the new word
			    strcpy(aya_part + strlen(aya_part), " \0");
			    strcpy(aya_part + strlen(aya_part), aya_word);
			}
		}

	}


	/* Almost forgat these. They have to be freed */
	free(laya_text);
	free(lsearch_string);
	free(aya_part);

	return return_value;
}

/**
 * Internal function: Checks if the search word matches a word in an aya
 * using similarity
 *
 * \param search_string the string to search for
 * \param aya_end the aya where to search in
 * \param similarity percentage of similarity (0 - 100) or EXACT_MATCH (=100%)
 *
 * \return the similarity percentage if found, otherwise 0
 **/
int compare_word(const char *search_word, const char *aya_text, int similarity) {

	int return_value = 0;

	/* To save the buffer for strtok_r */
	char *buffer;

	/* Set the strings to low case for (levenshtein) comparing */
	char *lsearch_word = lcase(strdup(search_word));
	char *laya_text = lcase(strdup(aya_text));

	/* Seperate all words of the aya, so they can be compared */
	char *aya_word = strtok_r(laya_text, DELIMITER, &buffer);

	while(aya_word!=NULL) {

		if (similarity==EXACT_MATCH) {
			/* Exact match (100%) means no similarity. Just compare */
			if (strcmp(lsearch_word, aya_word)==0) {
				return_value = EXACT_MATCH;
				break;
			}
		} else {

			/* The maximal distance is the amount of replacing the all characters of the
			   source string and adding all the missing characters to the source string or
			   removing all the extra characters */
			int max_distance = (strlen(lsearch_word)>strlen(aya_word)) ? strlen(lsearch_word): strlen(aya_word);

			/* Calculate the distance between source and target */
			int distance = levenshtein_distance(lsearch_word, aya_word);

			/* Calculate the similarity from the distance */
			int similarity_percentage = (1 - (double)distance / (double)max_distance) * 100;

			if (similarity_percentage >= similarity) {
				return_value = similarity_percentage;
				break;
			}

		}
		/* Get the next word of aya */
		aya_word = strtok_r(NULL, DELIMITER, &buffer);
	}

	/* Almost forgat these. They have to be freed */
	free(laya_text);
	free(lsearch_word);

	return return_value;
}

/**
 * Internal function: Computes levenshtein distance between s and t.
 *
 * From http://www.merriampark.com/ldc.htm by Lorenzo Seidenari
 *
 * \param s source string
 * \param t target string
 *
 * \return the distance between the 2 strings
 **/
int levenshtein_distance(char *s,char*t) {
	
	int *d; // matrix
	int i; // iterates through s
	int j; // iterates through t
	int n; // length of s
	int m; // length of t
	int k; // iterator
	int cost; // cost
	int distance; // Distance
	
	//Step 1
	n=strlen(s);
	m=strlen(t);
	if(n!=0&&m!=0) {
		d=malloc((sizeof(int))*(m+1)*(n+1));
		m++;
		n++;
		//Step 2
		for(k=0;k<n;k++) {
			d[k]=k;
		}
		for(k=0;k<m;k++) {
			d[k*n]=k;
		}
		//Step 3 and 4
		for(i=1;i<n;i++) {
			for(j=1;j<m;j++) {
				//Step 5
				if(s[i-1]==t[j-1]) {
					cost=0;
				} else {
					cost=1;
				}
				//Step 6
				d[j*n+i]=minimum(d[(j-1)*n+i]+1,d[j*n+i-1]+1,d[(j-1)*n+i-1]+cost);
			}
		}
		distance=d[n*m-1];
		free(d);
		return distance;
	} else {
		return -1; //a negative return value means that one or both strings are empty.
	}
}

/**
 * Internal function: Gets the minimum of three values.
 *
 * Used by levenshtein_distance().
 * From http://www.merriampark.com/ldc.htm by Lorenzo Seidenari
 *
 * \param a 1e value
 * \param b 2e value
 * \param c 3e value
 *
 * \return the minimum value
 **/
int minimum(int a,int b,int c) {

	int min=a;

	if(b<min) {
		min=b;
	}

	if(c<min) {
		min=c;
	}

	return min;
}
