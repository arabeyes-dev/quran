/* libquran - Holy Quran library
 * Copyright (C) 2002, 2003 Arabeyes, Mohammad DAMT
 * $Id$
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA  02111-1307, USA.
 *
 * NOTE: Holy Quran data is not released in GPL
 */

#ifndef __QURAN_SEARCH_H__
#define __QURAN_SEARCH_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __QURAN_H__
#include "quran.h"
#endif

#define DELIMITER " \t\r\n,.;?:"


/* prototypes for Internal functions */
int match(const char *, const char *, int, int);
int compare_word(const char *, const char *, int);
int compare_text(const char *, const char *, int);
int compare_phonetical(const char *, const char *, int);

#ifdef __cplusplus
}
#endif

#endif  // !__QURAN_SEARCH_H__
