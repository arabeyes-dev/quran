2005-12-29 Mohammed Yousif  <mhdyousif@gmx.net>
	* 0.1 Release
	
2004-06-08 Mohammed Yousif  <mhdyousif@gmx.net>
	* qtquran/src/audiothread.h
	  qtquran/src/audiothread.cpp: Fixed an audio sample buffer underrun that was occuring
	                                         while in the "Display By Sura" reciting mode.
                                                 It was causing the audio files of some verses on the larger
                                                 chapters (i.e. Al-Baqara) to be read with an annoying periodic
                                                 repitition of some samples.
                                                 

2004-06-07 Mohammed Yousif  <mhdyousif@gmx.net>
	* qtquran/src/audiothread.h
	  qtquran/src/audiothread.cpp: Using ALSA instead of the Open Sound System interface.
	                               Fixed bug#9 (if /dev/dsp busy qtquran exits), the user
				       is now notified when the audio device is busy instead of
				       blocking. (AFAIK, this wasn't possible with OSS and that's
				       why most applications using OSS block)
				       
	* qtquran/Makefile.am: Link against the ALSA Libraries.
	
2003-12-07 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp,
	  qtquran/src/searchform.cpp,
	  qtquran/src/searchwindow.ui,
	  qtquran/src/main.cpp: QtQuran doesn't use Tahoma anymore, the default font instead.

2003-07-23 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp,
	  qtquran/src/qtquran.h: Fixed some crashes related to the audio thread and moved
				 it to a new file.
	* qtquran/src/audioThread.cpp,
	  qtquran/src/audioThread.h: Cleaned the audio related code a bit and added a new
				     feature, Basmalla is now recited before any chapter
				     except Al-Fateha and Al-Tawba.

2003-07-21 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/main.cpp,
	  qtquran/src/mainwindow.ui,
	  qtquran/src/qtquran.cpp,
	  qtquran/src/qtquran.h,
	  qtquran/src/searchform.cpp,
	  qtquran/src/searchform.h,
	  qtquran/lng/qtquran_template.ts,
	  qtquran/lng/qtquran_ar.ts: Synched with the current libQuran API.
				     Support for multiple translations for one langauge.
				     Fixed direction and key accelerators inconsistency.

2003-06-30 Mohammed Yousif  <mhdyousif@gmx.net>

        * qtquran/src/qtquran.cpp,
	  qtquran/src/main.cpp: Fixed a bug that was causing it not to reverse the layout if the locale
	                           was more than 2 letters.

2003-06-12 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp: Fixed a bug that was causing double quotes to confuse the text container.

2003-02-01 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp: Replaced a call to a Qt 3.1 function - remove(const QString &)
				   for Qt 3.0.x compatibility.

2003-02-01 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp,
          qtquran/src/qtquran.h: Added the ability to click on the verse text to make it the current one.

2003-01-30 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp,
          qtquran/src/qtquran.h: Added the ability to browsing by chapter and use the red color to trace the current verse (still to be enhanced later).
	* configure.in: Incremented version to 0.08.

2003-01-19 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/configure.in,
	  qtquran/src/Makefile.am,
	  qtquran/src/main.cpp,
	  qtquran/src/qtquran.cpp: Implemented i18n.
	* qtquran/src/lng/qtquran_template.ts: Added a template for translators.
	* qtquran/src/lng/qtquran_ar.ts: Added an arabic translation.

2003-01-12 Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/mainwindow.ui,
	  qtquran/src/qtquran.cpp,
	  qtquran/src/qtquran.h,
	  qtquran/src/Makefile.am: Migrated Mohammed Aser's patch for the new gui design.
	* qtquran/src/qtquran.cpp,
	  qtquran/src/searchform.cpp,
	  qtquran/src/main.cpp,
	  qtquran/src/lng/Makefile.am,
	  qtquran/src/Makefile.am: Prepared for i18n.
	* qtquran/confiure.in: Fixed a compatibility issue with autoconf.

2002-12-09  Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.h,
	  qtquran/src/qtquran.cpp: Fixed a bug that caused the program to crash after reading a verse.
				   Fixed a bug that was causing the Read button to be disabled while nothing was played.
	* qtquran/src/searchfrom.h: fixed compatibility issue with gcc 3.x (Thanks to Mohammed Sameer).
	
2002-12-06  Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/src/qtquran.cpp,
	   qtquran/src/qtquran.h: More information about the current verse (Juzz, hezb and has_sajda) is now shown.
	   				    Using qthread.h rather than pthread.h.
					    Audio is now played using directly to /dev/dsp.
					    btnRead and btnStop are only enabled if there is at least one recitor available.
	* qtquran/configure.in: Incrementing version to 0.04.

2002-12-04  Mohammed Yousif  <mhdyousif@gmx.net>

	* qtquran/Makefile.am,
	   qtquran/configure.in,
	   qtquran/src/Makefile.am: Renamed quran to qtquran and moved quran/quran to /quran/src, quran.cpp to qtquran.cpp and quran.h to qtquran.h.
	   					Requiring libquran, libexpat, libspeex and libogg.
						xmlwrapp and libxml2 is not required anymore.
						Incrementing VERSION to 0.03.
	* qtquran/src/qtquran.cpp,
	   qtquran/src/qtquran.h,
	   qtquran/src/searchform.cpp,
	   qtquran/src/searchform.h: Using libquran, and removing xmlwrapp stuff.

2002-10-27  Mohammed Yousif  <mhdyousif@gmx.net>

	* quran/quran/searchform.h,
	  quran/quran/searchform.cpp: searching by multiple keywords is now possible.
				      the ability to search in a specific 'Sura' has been added.
				      added some options for matching words (exact, similar), Levenshtein Distance is planned to be added later.

2002-09-25  Mohammed Yousif  <mhdyousif@gmx.net>

	* quran/quran/text/quran.en.xml: added english translation of the Qur'an, Thanks to Shaykh Ahmad Darwish <mosque@mosque.com>

2002-09-21  Mohammed Yousif  <mhdyousif@gmx.net>

	* quran/quran/quran.h: removed prototype of SearchByWord()
	* quran/quran/quran.cpp: the search button now opens a Search Form
	* quran/quran/searchform.h: added a new class 'searchForm'
	* quran/quran/searchform.cpp: a new search form has been added, searching now works.

2002-08-07  Mohammed Yousif  <mhdyousif@gmx.net>

	* quran/quran/quran.h: moved two includes to quran.cpp and added some variables
	* quran/quran/quran.cpp: the program now reads its configurations from an external xml file,
				 added the ability to use different translations of the Holy Qur'an,
				 added the ability to use audio files in WAV format (using the appropriate player)
				 added the ability to customize the name of the audio files to use	 	
	* quran/quran/Makefile.in: added some translations for the holy Qur'an. (Thanks Hatem Ben)
	
2002-08-03  Mohammed Yousif  <mhdyousif@gmx.net>

	* quran/quran/quran.cpp: synchronzied it with the new changes in the xml file by Ammer, that helped a lot speeding up the program (Thanks Ammer)

2002-08-03  ammer  <ammer@islamway.net>

	* changed the quran.xml file to be: <sura id= name=><aya id=>...</aya></sura>

0.01a:
* Replaced the 'system' call with a QProcess, it shouldn't hang after
  pressing 'Telawa' anymore.
* The button 'Eqaf' now works.
* XML file containing the Holy Qur'an text has been added, now you can
  see the verses' text while having the program read the audio files
  at the same time.
* a non-working searching algorithm has been added.
0.01:
First Release
