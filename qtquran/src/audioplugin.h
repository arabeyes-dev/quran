/************************************************************************
 * $Id$                                                                                                                 *
 *                                                                                                                          *
 * ------------                                                                                                          *
 * Description:                                                                                                   *
 * ------------                                                                                                          *
 *  Copyright (c) 2005, Arabeyes, Mohammed Yousif                                 *
 *                                                                                                                          *
 *  audioplugin.h                                                                                               *
 *  The API to be used by all audio plugins                                                   *
 *                                                                                                                          *
 * -----------------                                                                                                    *
 * Revision Details:    (Updated by Revision Control System)                     *
 * -----------------                                                                                                    *
 *  $Date$                                                                                                           *
 *  $Author$                                                                                                        *
 *  $Revision$                                                                                                     *
 *  $Source$                                                                                                       *
 *                                                                                                                          *
 ************************************************************************/
 
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                                                 *
 *                                                                                                                                *
 ***************************************************************************/

#ifndef AUDIOPLUGIN_H
#define AUDIOPLUGIN_H

#include <quran.h>

int audio_open(void);
int PlayAudioCallback(quran_audio *);
void audio_close(void);

#endif /* AUDIOPLUGIN_H */
