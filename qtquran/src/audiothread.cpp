/************************************************************************
 * $Id$                                                                                                                 *
 *                                                                                                                          *
 * ------------                                                                                                          *
 * Description:                                                                                                   *
 * ------------                                                                                                          *
 *  Copyright (c) 2002-2005, Arabeyes, Mohammed Yousif                       *
 *                                                                                                                          *
 *  audiothread.cpp                                                                                          *
 *  The thread responsible for audio playing                                              *
 *                                                                                                                          *
 * -----------------                                                                                                    *
 * Revision Details:    (Updated by Revision Control System)                     *
 * -----------------                                                                                                    *
 *  $Date$                                                                                                           *
 *  $Author$                                                                                                        *
 *  $Revision$                                                                                                     *
 *  $Source$                                                                                                       *
 *                                                                                                                          *
 ************************************************************************/
 
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                                                 *
 *                                                                                                                                *
 ***************************************************************************/

#include <algorithm>
#include <iostream>
#include <vector>
#include <cstdlib>

#include <dirent.h>
#include <sys/stat.h>
#include <dlfcn.h>

#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>

#include <quran.h>

#include <qregexp.h>
#include <qthread.h>
#include <qapplication.h>
#include <qmessagebox.h>

#include "../config.h"

#include "general.h"
#include "qtquran.h"
#include "audiothread.h"


static const QString  AUDIO_PLUGINS_DIR = QTQURAN_SHARE_DIR "/plugins/audio";
/*
  Most of this ALSA code is from the turorials @ alsa-project.org
  TODO: the C stuff needs to be replaced
*/
/*
  snd_pcm_t * alsa_device = 0;
  snd_pcm_hw_params_t * hw_params = 0;
  int hwparametersSet = 0;


  bool audio_open() {
  int err = -1;
  if ((err = snd_pcm_open (&alsa_device, "plughw:0,0", SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK )) >= 0)
  if ((err = snd_pcm_nonblock(alsa_device, 0)) >= 0)
  return true; // success
  
  switch (err) {
  case -EBUSY:
  fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0",  "the resources are not available");
  break;
  case -EAGAIN:
  fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0", "the ring buffer is either full (playback) or empty (capture).");
  break;
  default:
  fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0", snd_strerror (err));
  break;
  }
  return false;
  }



  void audio_close() {
  snd_pcm_hw_params_free (hw_params);
  hw_params = 0;
  snd_pcm_close (alsa_device);
  alsa_device = 0;
  }



  int PlayAudioCallback(quran_audio * qal) {
  if (!qal)
  return 1;
  
  if (hwparametersSet != 1) {
  hwparametersSet = 1;
  int err = -1;
    
  if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)  {
  fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n", snd_strerror (err));
  exit (1);
  }
    
  if ((err = snd_pcm_hw_params_any (alsa_device, hw_params)) < 0)  {
  fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n", snd_strerror (err));
  exit (1);
  }
    
  if ((err = snd_pcm_hw_params_set_access (alsa_device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)  {
  fprintf (stderr, "cannot set access type (%s)\n", snd_strerror (err));
  exit (1);
  }
    
  if ((err = snd_pcm_hw_params_set_format (alsa_device, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)  {
  fprintf (stderr, "cannot set sample format (%s)\n", snd_strerror (err));
  exit (1);
  }
  qDebug("Rate: %d", qal->rate);
  if ((err = snd_pcm_hw_params_set_rate_near (alsa_device, hw_params, (unsigned int *)&qal->rate, 0)) < 0)  {
  fprintf (stderr, "cannot set sample rate (%s)\n", snd_strerror (err));
  exit (1);
  }
  qDebug("Channels: %d", qal->channels);
  if ((err = snd_pcm_hw_params_set_channels (alsa_device, hw_params, qal->channels)) < 0)  {
  fprintf (stderr, "cannot set channel count (%s)\n", snd_strerror (err));
  exit(1);
  }
    
  //if (snd_pcm_hw_params_set_periods(alsa_device, hw_params, 2, 0) < 0) {
  //      fprintf(stderr, "Error setting periods (%s).\n",  snd_strerror (err));
  //    exit (1);
  //}
    
  // Set buffer size (in frames). The resulting latency is given by
  // latency = periodsize * periods / (rate * bytes_per_frame)
  snd_pcm_uframes_t buffersize = (8192*2) >> 2;
  if ((err = snd_pcm_hw_params_set_buffer_size_near(alsa_device, hw_params, &buffersize)) < 0) {
  fprintf(stderr, "Error setting buffersize (%s).\n", snd_strerror (err));
  exit(1);
  }
    
  if ((err = snd_pcm_hw_params (alsa_device, hw_params)) < 0)  {
  fprintf (stderr, "cannot set parameters (%s)\n", snd_strerror (err));
  exit (1);
  }
    
    
  if ((err = snd_pcm_prepare (alsa_device)) < 0)  {
  fprintf (stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror (err));
  exit (1);
  }
  }
  
  int bytes = 0;
  if ((bytes = snd_pcm_writei (alsa_device, qal->buffer, qal->frame_size)) != (int)qal->frame_size)  {
  if (bytes >= 0) {
  //qDebug("Wrote %d bytes of %d bytes.", bytes, qal->frame_size);
  } else {
  switch (bytes) {
  case -EBADFD:
  fprintf (stderr, "PCM is not in the right state (SND_PCM_STATE_PREPARED or SND_PCM_STATE_RUNNING)\n");
  break;
  case -EPIPE:
  fprintf (stderr, "An underrun occurred\n");
  break;
  case -ESTRPIPE:
  fprintf (stderr, "a suspend event occurred (stream is suspended and waiting for an application recovery)\n");
  break;
  default:
  fprintf (stderr, "write to audio interface failed (%s)\n", snd_strerror (bytes));        
  break;
  }
  int err = -1;
  if ((err = snd_pcm_prepare (alsa_device)) < 0)  {
  fprintf (stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror (err));
  exit (1);
  }
  }
  }
  
  return 0;
  }
*/


void * audioPluginHandle = 0;


typedef int (*audio_open_t)();
typedef int (*PlayAudioCallback_t)(quran_audio *);
typedef void (*audio_close_t)();

audio_open_t audio_open = 0;
PlayAudioCallback_t PlayAudioCallback = 0;
audio_close_t audio_close = 0;

bool audioThread::initializeAudioPlugin() {
  DIR * dp;
  struct dirent * ep;
  dp = opendir (AUDIO_PLUGINS_DIR + "/" + audioPlugin);
  QString currentPluginDir = AUDIO_PLUGINS_DIR + "/" + audioPlugin;
  QString pluginfilename = "";
  if (dp != 0) {
    while ((ep = readdir(dp))) {
      QString candidatefilename(ep->d_name);
      struct stat buf;
      if (stat (currentPluginDir + "/"  + candidatefilename, &buf) != 0)
        continue;
      
      // If it's not a regular file, skip it.
      if (S_ISREG (buf.st_mode) == 0)
        continue;
      
      QRegExp rx( "^libqtquran_(\\w+)\\.so.*$" );
      if (rx.search(candidatefilename) == 0) {
        if (rx.cap(1) == audioPlugin) {
          pluginfilename = rx.cap(0);
          break;
        }
      }
    }
    closedir (dp);
  }
  
  if (pluginfilename == "") {
    QString title = trUtf8("The \"%1\" Audio Plugin Cannot Be Found").arg(audioPlugin);
    QString msg = trUtf8("The \"%1\" audio plugin cannot be found in \"%2\"!").arg(audioPlugin).arg(currentPluginDir);
    WarningMessageBox(title, msg);
    return false;
  }
  
  audioPluginHandle = dlopen(currentPluginDir + "/"+ pluginfilename, RTLD_LAZY);
  if (!audioPluginHandle) {
    QString title = trUtf8("The \"%1\" Audio Plugin Cannot Be Loaded").arg(audioPlugin);
    QString msg = trUtf8("The \"%1\" audio plugin cannot be loaded from the file \"%2\"!\n%3").arg(audioPlugin).arg(currentPluginDir + "/" + pluginfilename).arg(dlerror());
    WarningMessageBox(title, msg);
    return false;
  }
  
  audio_open = (audio_open_t) dlsym(audioPluginHandle, "audio_open");
  if (!audio_open) {
    QString title = trUtf8("Error Loading symbols").arg(audioPlugin);
    QString msg = trUtf8("Cannot load symbol  \'%1\' from the \"%2\" audio plugin from\nthe file %3\n%4").arg("audio_open").arg(audioPlugin).arg(currentPluginDir + "/" + pluginfilename).arg(dlerror());
    WarningMessageBox(title, msg);
    dlclose(audioPluginHandle);
    return false;
  }
  
  audio_close = (audio_close_t) dlsym(audioPluginHandle, "audio_close");
  if (!audio_close) {
    QString title = trUtf8("Error Loading symbols").arg(audioPlugin);
    QString msg = trUtf8("Cannot load symbol  \'%1\' from the \"%2\" audio plugin from\nthe file %3\n%4").arg("audio_close").arg(audioPlugin).arg(currentPluginDir + "/" + pluginfilename).arg(dlerror());
    WarningMessageBox(title, msg);
    dlclose(audioPluginHandle);
    return false;
  }
  
  PlayAudioCallback = (PlayAudioCallback_t) dlsym(audioPluginHandle, "PlayAudioCallback");
  if (!PlayAudioCallback) {
    QString title = trUtf8("Error Loading symbols").arg(audioPlugin);
    QString msg = trUtf8("Cannot load symbol  \'%1\' from the \"%2\" audio plugin from\nthe file %3\n%4").arg("PlayAudioCallback").arg(audioPlugin).arg(currentPluginDir + "/" + pluginfilename).arg(dlerror());
    WarningMessageBox(title, msg);
    dlclose(audioPluginHandle);
    return false;
  }
  
  return true;
}

void audioThread::run() {
  if (audioPluginHandle == 0) {
    if (initializeAudioPlugin() == false)
      stopReading = true;
  }
  
  bool basmalla = true;
  while (!stopReading) {
    qApp->lock();
    bool readBasmalla = basmalla && q->getCurrentAya()==0 && q->getCurrentSura()!=0 && q->getCurrentSura()!=8;
    int sura = q->getCurrentSura()+1;
    int aya = q->getCurrentAya()+1;
    qApp->unlock();
    if ( readBasmalla ) {
      read(1,1);
      basmalla = false;
    }
    if (!read(sura, aya))
      break;
    qApp->lock();
    bool endOfWord = !q->SeekNextAya();
    qApp->unlock();
    if (endOfWord)
      break;
    basmalla = true;
    //qApp->processEvents();
  }
  q->EnableBtnRead();
}

bool audioThread::read(int sura, int aya) {
  qa = quran_audio_open_index(q->getCurrentLang(), q->getCurrentRecitor(), sura, aya, q->getContext());
  if (qa == NULL) {
    QString title = trUtf8("Verse Audio file not found");
    QString msg = trUtf8("Error opening the audio file for verse %1 from chapter %2").arg(aya).arg(sura);
    WarningMessageBox(title, msg);
    return false;
  }
  
  if (audio_open() == 0) {
    int ret = true;
    if (quran_audio_play(qa, PlayAudioCallback) != 0) {
	QString title = trUtf8("Error playing audio file");
        QString msg = trUtf8("Couldn't play the audio file for this verse!\n"
	                     "This might occur because of a running sound server (i.e. aRts, if you\n"
			     "are using KDE) in that case you should shut down the sound server.");
	WarningMessageBox(title, msg);
	ret = false;
    }
    audio_close();
    quran_audio_close(qa, q->getContext());
    qa=NULL;
    return ret;
  }
  return false;
}
