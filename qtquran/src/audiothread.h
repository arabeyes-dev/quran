/************************************************************************
 * $Id$                                                                                                                 *
 *                                                                                                                          *
 * ------------                                                                                                          *
 * Description:                                                                                                   *
 * ------------                                                                                                          *
 *  Copyright (c) 2002-2005, Arabeyes, Mohammed Yousif                       *
 *                                                                                                                          *
 *  audiothread.h                                                                                               *
 *  The thread responsible for audio playing                                              *
 *                                                                                                                          *
 * -----------------                                                                                                    *
 * Revision Details:    (Updated by Revision Control System)                     *
 * -----------------                                                                                                    *
 *  $Date$                                                                                                           *
 *  $Author$                                                                                                        *
 *  $Revision$                                                                                                     *
 *  $Source$                                                                                                       *
 *                                                                                                                          *
 ************************************************************************/
 
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                                                 *
 *                                                                                                                                *
 ***************************************************************************/

#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H

#include <quran.h>
#include <qthread.h>
#include <qobject.h>
#include <qstring.h>

class QuranApp;

class audioThread : public QObject, public QThread {
  Q_OBJECT
 public:
  quran_audio * qa;
  QuranApp * q;
  bool stopReading;
  QString audioPlugin;
      
 protected:
  void run();
  bool read(int, int);
  bool initializeAudioPlugin();
};

#endif // AUDIOTHREAD_H
