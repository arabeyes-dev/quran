#define ALSA_PCM_NEW_HW_PARAMS_API
#include <alsa/asoundlib.h>

#include "../../../audioplugin.h"

/*
  Most of this ALSA code is from the turorials @ alsa-project.org
*/

snd_pcm_t * alsa_device = 0;
snd_pcm_hw_params_t * hw_params = 0;
int hwparametersSet = 0;

void setHWParameters(quran_audio *);

void setHWParameters(quran_audio * qal) {
    int err = -1;
    
    if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)  {
      fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_hw_params_any (alsa_device, hw_params)) < 0)  {
      fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_hw_params_set_access (alsa_device, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)  {
      fprintf (stderr, "cannot set access type (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_hw_params_set_format (alsa_device, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)  {
      fprintf (stderr, "cannot set sample format (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_hw_params_set_rate_near (alsa_device, hw_params, (unsigned int *)&qal->rate, 0)) < 0)  {
      fprintf (stderr, "cannot set sample rate (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_hw_params_set_channels (alsa_device, hw_params, qal->channels)) < 0)  {
      fprintf (stderr, "cannot set channel count (%s)\n", snd_strerror (err));
      exit(1);
    }
    /*
    if (snd_pcm_hw_params_set_periods(alsa_device, hw_params, 2, 0) < 0) {
      fprintf(stderr, "Error setting periods (%s).\n",  snd_strerror (err));
      exit (1);
    }
    */
    
    /* Set buffer size (in frames). The resulting latency is given by */
    /* latency = periodsize * periods / (rate * bytes_per_frame)     */
    snd_pcm_uframes_t buffersize = (8192*2) >> 2;
    if ((err = snd_pcm_hw_params_set_buffer_size_near(alsa_device, hw_params, &buffersize)) < 0) {
      fprintf(stderr, "Error setting buffersize (%s).\n", snd_strerror (err));
      exit(1);
    }
    
    if ((err = snd_pcm_hw_params (alsa_device, hw_params)) < 0)  {
      fprintf (stderr, "cannot set parameters (%s)\n", snd_strerror (err));
      exit (1);
    }
    
    if ((err = snd_pcm_prepare (alsa_device)) < 0)  {
      fprintf (stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror (err));
      exit (1);
    }
}


int audio_open() {
  int err = -1;
  hwparametersSet = 0;
  if ((err = snd_pcm_open (&alsa_device, "plughw:0,0", SND_PCM_STREAM_PLAYBACK, SND_PCM_NONBLOCK )) >= 0)
    if ((err = snd_pcm_nonblock(alsa_device, 0)) >= 0)
      return 0; /* success */
  
  switch (err) {
  case -EBUSY:
    fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0",  "the resources are not available");
    break;
  case -EAGAIN:
    fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0", "the ring buffer is either full (playback) or empty (capture).");
    break;
  default:
    fprintf (stderr, "cannot open audio device %s (%s)\n", "plughw:0,0", snd_strerror (err));
    break;
  }
  return 1;
}

int PlayAudioCallback(quran_audio * qal) {
  if (!qal)
    return 1;
  
  if (hwparametersSet != 1) {
    hwparametersSet = 1;
    setHWParameters(qal);
  }
  
  int bytes = 0;
  if ((bytes = snd_pcm_writei (alsa_device, qal->buffer, qal->frame_size)) != (int)qal->frame_size)  {
    if (bytes >= 0) {
      printf("Wrote %d bytes of %d bytes.", bytes, qal->frame_size);
    } else {
      switch (bytes) {
      case -EBADFD:
        fprintf (stderr, "PCM is not in the right state (SND_PCM_STATE_PREPARED or SND_PCM_STATE_RUNNING)\n");
        break;
      case -EPIPE:
        fprintf (stderr, "An underrun occurred\n");
        break;
      case -ESTRPIPE:
        fprintf (stderr, "a suspend event occurred (stream is suspended and waiting for an application recovery)\n");
        break;
      default:
        fprintf (stderr, "write to audio interface failed (%s)\n", snd_strerror (bytes));        
        break;
      }
      int err = -1;
      if ((err = snd_pcm_prepare (alsa_device)) < 0)  {
        fprintf (stderr, "cannot prepare audio interface for use (%s)\n", snd_strerror (err));
        exit (1);
      }
    }
  }
  return 0;
}

void audio_close() {
  snd_pcm_hw_params_free (hw_params);
  hw_params = 0;
  snd_pcm_close (alsa_device);
  alsa_device = 0;
}
