#include <stdlib.h>
#include <sys/soundcard.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "../../../audioplugin.h"

FILE * fout = 0;
int audio_fd = -1;
int opened = 0;

int audio_open() {
  opened = 0;
  fout = 0;
  audio_fd = -1;
  return 0;
}

void audio_close() {
  if (fout != NULL)
    fclose(fout);
  if (audio_fd < 0)
    close(audio_fd);
}

int PlayAudioCallback(quran_audio *qal) {
  if (!qal)
    return 1;
  if (!opened) {
    int rate, format, stereo;

    audio_fd = open("/dev/dsp",O_WRONLY, O_NONBLOCK);
    if (audio_fd < 0) {
	perror("Cannot open /dev/dsp. Please stop any running sound server (i.e. arts) and try again");
	return 1;
    }
    opened = 1;
    rate = qal->rate;
    format=AFMT_S16_LE;
    if (ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format)==-1) {
	perror("SNDCTL_DSP_SETFMT");
	close(audio_fd);
	exit(1);
    }
    stereo = (qal->channels == 2) ? 1 : 0;
    if (ioctl(audio_fd, SNDCTL_DSP_STEREO, &stereo) == -1) {
	perror("SNDCTL_DSP_STEREO");
	close(audio_fd);
	exit(1);
    }
    if (ioctl(audio_fd, SNDCTL_DSP_SPEED, &rate) == -1) {
	perror("SNDCTL_DSP_SPEED");
	close(audio_fd);
	exit(1);
    }
    fout = fdopen(audio_fd, "w");
  }
  fwrite(qal->buffer, sizeof(short), qal->channels * qal->frame_size, fout);
  return 0;
}
