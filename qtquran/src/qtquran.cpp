/*$Id$*******************
  qtquran.cpp  -  implementation for QuranApp class
  -------------------
  begin                : Fri Jul 12 15:53:41 EEST 2002
  copyright           :  (C) Copyright 2002, Arabeyes, by Mohammed Yousif
  email                : mhdyousif@gmx.net

  $Date$
  $Author$
  $Revision$
  $Source$
***************************************************************************/

/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify                              *
 *   it under the terms of the GNU General Public License as published by                           *
 *   the Free Software Foundation; either version 2 of the License, or                                  *
 *   (at your option) any later version.                                                                              *
 *                                                                                                                                *
 ***************************************************************************/
#include <iostream>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <qapplication.h>
#include <qtextcodec.h>
#include <qlabel.h>
#include <qaccel.h>
#include <qvariant.h>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <qmime.h>
#include <qdragobject.h>
#include <qlayout.h>
#include <qmessagebox.h>
#include <qimage.h>
#include <qtextbrowser.h>
#include <qvaluelist.h>
#include <qthread.h>
#include <quran.h>
#include <qwidget.h>
#include <stdlib.h>

#include "qtquran.h"
#include "searchform.h"


void QuranApp::Read()  {
  if ( !cbrecitor->count() || !ctx->libquran.avail_langs[cblang->currentItem()].num_audio_packages ) {
    qWarning("No such recitor!");
    Stop();
    return;
  }
  
  QString sura_n, aya_n;
  btnRead->setEnabled(0);
  btnStop->setEnabled(1);
  athread.stopReading = false;
  sura_n = sura_n.setNum( cbsura->currentItem() + 1 );
  aya_n = aya_n.setNum( cbaya->currentItem() + 1 );
  if ( !cbrecitor->count() || !ctx->libquran.avail_langs[cblang->currentItem()].num_audio_packages ) {
    qWarning("Error Opening this verse audio file!");
    Stop();
    return;
  }
  if (!athread.running())
    athread.start();
}

void QuranApp::EnableBtnRead()  {
  btnRead->setEnabled(1);
  btnStop->setEnabled(0);
}

QuranApp::QuranApp(QWidget *parent, const char *name) : mainwindow(parent, name), valid(false) {

  ctx = new libquran_ctx;
  ctx->initialized = 0;

  int ii=quran_init(ctx);

  if (ii && ii!=-7 || !ctx->initialized  ) {  //if failed but not because audio dir is invalid
    qFatal("Error calling quran_init(), Return Code:%d",ii);
    return;
  }

  pixtopleftcorner.load(QTQURAN_SHARE_DIR "/pic/topleftcorner.xpm");
  pixtoprightcorner.load(QTQURAN_SHARE_DIR "/pic/toprightcorner.xpm");
  pixbottomleftcorner.load(QTQURAN_SHARE_DIR "/pic/bottomleftcorner.xpm");
  pixbottomrightcorner.load(QTQURAN_SHARE_DIR "/pic/bottomrightcorner.xpm");
  pixleftframe.load(QTQURAN_SHARE_DIR "/pic/leftframe.xpm");
  pixrightframe.load(QTQURAN_SHARE_DIR "/pic/rightframe.xpm");
  pixtopframe.load(QTQURAN_SHARE_DIR "/pic/topframe.xpm");
  pixbottomframe.load(QTQURAN_SHARE_DIR "/pic/bottomframe.xpm");
  topframe->setPaletteBackgroundPixmap(pixtopframe);
  bottomframe->setPaletteBackgroundPixmap(pixbottomframe);
  if ( ((QString)QTextCodec::locale()).startsWith("ar")) {
    //for RTL languages, mirror the UI
    //Notice that mirroring the UI will also reverse the order of
    //all images, thus they have to be reversed (reversing the reverse :-)

    topleftcorner->setPixmap(pixtoprightcorner);
    toprightcorner->setPixmap(pixtopleftcorner);
    bottomleftcorner->setPixmap(pixbottomrightcorner);
    bottomrightcorner->setPixmap(pixbottomleftcorner);
    leftframe->setPaletteBackgroundPixmap(pixrightframe);
    rightframe->setPaletteBackgroundPixmap(pixleftframe);
    qApp->setReverseLayout(TRUE);
    btnNextAya->setAccel(Qt::Key_Left);
    btnPrevAya->setAccel(Qt::Key_Right);
  } else {
    topleftcorner->setPixmap(pixtopleftcorner);
    toprightcorner->setPixmap(pixtoprightcorner);
    bottomleftcorner->setPixmap(pixbottomleftcorner);
    bottomrightcorner->setPixmap(pixbottomrightcorner);
    leftframe->setPaletteBackgroundPixmap(pixleftframe);
    rightframe->setPaletteBackgroundPixmap(pixrightframe);
    btnNextAya->setAccel(Qt::Key_Right);
    btnPrevAya->setAccel(Qt::Key_Left);
  }
  btnNextSura->setAccel(Qt::Key_Up);
  btnPrevSura->setAccel(Qt::Key_Down);

  if (!ctx->libquran.num_langs) {
    QMessageBox::critical(0, trUtf8("Fatal Error"),trUtf8("No language packages are available"), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
    return;
  }


  for (int counter=0; counter<ctx->libquran.num_langs; counter++) {
    if (ctx->libquran.avail_langs[counter].num_texts <= 0) {
      QMessageBox::critical(0, trUtf8("Fatal Error"),trUtf8("There is no text in the package for the language (%1 \"%2\").\nAt least one text is needed for each language package.\nPlease either add one text to that language package or remove it from your data directory:\n  %3").arg(ctx->libquran.avail_langs[counter].name).arg(ctx->libquran.avail_langs[counter].code).arg(ctx->libquran.data_directory), QMessageBox::Ok, QMessageBox::NoButton, QMessageBox::NoButton);
      return;
    }
  }

  d_mode = Page;
  currentStartOfPage = 0;
  displayedVerses = 20;
  setRightJustification(TRUE);
  setCaption(trUtf8("The Holy Qur'an Program - Version: ") + VERSION);
  // setCentralWidget( new QWidget( this, "qt_central_widget" ) );

  for (int counter=0; counter<ctx->libquran.num_langs; counter++)
    cblang->insertItem( trUtf8( (QString)ctx->libquran.avail_langs[counter].name ) );

  qu = NULL;
  languageChanged(0);

  QString strtmp;
  cbsura->clear();

  for (int iix=0; iix<114; iix++)
    cbsura->insertItem( QString::fromUtf8((QString)qu->sura_titles[iix]) );

  cbaya->clear();
  for (int t=1; t<8; t++)
    cbaya->insertItem( QString::number(t) );

  tlsajda->setText( trUtf8( "Sajda" ) );
  tlsajda->hide();

  //  tbQuranText->setTextFormat(Qt::RichText);
  //  tbQuranText->setWordWrap(QTextEdit::WidgetWidth);
  //  tbQuranText->setWrapPolicy(QTextEdit::AtWordBoundary);

  // signals and slots connections
  connect( tbQuranText, SIGNAL(clicked(int, int) ), this, SLOT( tbQuranTextClicked(int, int) ) );
  athread.qa = NULL;
  athread.q = this;
  valid = true;
}

QuranApp::~QuranApp() {
  quran_free();
  quran_close(qu, ctx);
}



/////////////////////////////////////////////////////////////////////
// SLOT IMPLEMENTATION
/////////////////////////////////////////////////////////////////////
void QuranApp::Stop()  {
  athread.stopReading = true;
}





void QuranApp::Refreshcbaya(int c_sura)  {
  sura_info * sura_inf = quran_sura_info(c_sura+1);
  //Set cbsura to the new selection
  if ( c_sura != cbsura->currentItem() ) cbsura->setCurrentItem(c_sura);
  //remove all items in cbaya from the last item to the first one
  for (int citem=cbaya->count()-1 ; citem >= 0 ; citem-- )
    cbaya->removeItem(citem);
  //Add items to cbaya
  for (int citem=1 ; citem <= sura_inf->max_aya ; citem++)
    cbaya->insertItem(QString::number(citem));
  free(sura_inf);
  //Set cbaya to the first item
  cbaya->setCurrentItem(0);
  currentStartOfPage = 0;
  UpdatetbQuranText();
  
  RefreshtbQuranText(0);
}

void QuranApp::UpdatetbQuranText()  {
  char * ayatext;
  char * basmalla_c = quran_read_verse(qu , 1, 1, ctx);
  QString basmalla = QString::fromUtf8(basmalla_c).stripWhiteSpace();
  free(basmalla_c);
  basmalla_c=NULL;
  QString Qsuratext="";
  
  if (d_mode == Page) {
    Qsuratext = "";
    ayaPos.clear();
    //add Basmalla to all chapters except Al-Fateha and Al-Tawba
    if (cbaya->currentItem() < displayedVerses)
      if ((cbsura->currentItem()!=0) && (cbsura->currentItem()!= 8))
	//if it's not Arabic use verse one of Al-Fateha as the Basmalla
	if ((QString)qu->lang!="ar" )  {
	  Qsuratext.append("<center>" + basmalla + "</center>");
	} else if ((QString)qu->lang=="ar")
	  Qsuratext.append(QString::fromUtf8("<center>﷽</center>"));
    
    for (int count=currentStartOfPage; count<cbaya->count() && count<currentStartOfPage+displayedVerses; count++) {
      ayatext = quran_read_verse(qu ,cbsura->currentItem()+1,count+1, ctx);
      QString num = QString::number(count+1);
      QChar parenleft = '{';
      QChar parenright = '}';
      if (((QString)qu->lang).contains("ar")) {
        for (int i=0; i<static_cast<int>(num.length()); i++) {
          ushort unival = num[i].unicode();
	  unival += 0xDFD0;
	  num[i] = QChar(unival);
        }
	parenleft = QChar(0xE00A);
	parenright = QChar(0xE00B);
      }
      Qsuratext.append((QString::fromUtf8(ayatext)).stripWhiteSpace() + parenleft + num + parenright + ' ');
      if ((cbsura->currentItem()==0) || (cbsura->currentItem()==8) || (cbaya->currentItem() >= displayedVerses))
	ayaPos.append(Qsuratext.length()-1);   //meaning that there is no Basmalla.
      else
	if ((QString)qu->lang!="ar" )  {  //if it's not Arabic use verse one of Al-Fateha as the Basmalla
	  ayaPos.append(Qsuratext.length()-basmalla.length()-17-1);
	} else if ((QString)qu->lang=="ar")
	  ayaPos.append(Qsuratext.length()-18-1);   //18 is strlen("<center>﷽</center>")
      free(ayatext);
      ayatext=NULL;
    }
  } else if (d_mode == Chapter) {
    Qsuratext = "";
    ayaPos.clear();
    //add Basmalla to all chapters except Al-Fateha and Al-Tawba
    if (cbaya->currentItem() < displayedVerses)
      if ((cbsura->currentItem()!=0) && (cbsura->currentItem()!= 8))
	//if it's not Arabic use verse one of Al-Fateha as the Basmalla
	if ((QString)qu->lang!="ar" )  {
	  Qsuratext.append("<center>" + basmalla + "</center>");
	} else if ((QString)qu->lang=="ar")
	  Qsuratext.append(QString::fromUtf8("<center>﷽</center>"));
    for (int count=0; count<cbaya->count(); count++) {
      ayatext = quran_read_verse(qu ,cbsura->currentItem()+1,count+1, ctx);
      QString num = QString::number(count+1);
      QChar parenleft = '{';
      QChar parenright = '}';
      if (((QString)qu->lang).contains("ar")) {
        for (int i=0; i<static_cast<int>(num.length()); i++) {
          ushort unival = num[i].unicode();
	  unival += 0xDFD0;
	  num[i] = QChar(unival);
        }
	parenleft = QChar(0xE00A);
	parenright = QChar(0xE00B);
      }
      Qsuratext.append((QString::fromUtf8(ayatext)).stripWhiteSpace() + parenleft + num + parenright + ' ');
      if ((cbsura->currentItem()==0) || (cbsura->currentItem()==8))
	ayaPos.append(Qsuratext.length()-1);   //meaning that there is no Basmalla.
      else
	if ((QString)qu->lang!="ar" )  {  //if it's not Arabic use verse one of Al-Fateha as the Basmalla
	  ayaPos.append(Qsuratext.length()-basmalla.length()-17-1);
	} else if ((QString)qu->lang=="ar")
	  ayaPos.append(Qsuratext.length()-18-1);   //18 is strlen("<center>﷽</center>")
      free(ayatext);
      ayatext=NULL;
    }
  }
  /*
    for (int i=0; i<Qsuratext.length(); i++)
    if (Qsuratext[i].unicode() == 0x0659)
    Qsuratext[i] = QChar(0x06E4);
    else if (Qsuratext[i].unicode() == 0x065A)
    Qsuratext[i] = QChar(0x06E7);
    else if (Qsuratext[i].unicode() == 0x065B)
    Qsuratext[i] = QChar(0x0656);
    else if (Qsuratext[i].unicode() == 0x065F)
    Qsuratext[i] = QChar(0x0675);
  */    
  //tbQuranText->setText(Qsuratext);
  SettbQuranTextWorkAroundDisplayProblems(Qsuratext);
}


void QuranApp::SettbQuranTextWorkAroundDisplayProblems(QString txt) {

  for (unsigned int i=0; i<txt.length(); i++)
    if (txt[i].unicode() == 0x0659)
      txt[i] = QChar(0x06E4);
    else if (txt[i].unicode() == 0x065A)
      txt[i] = QChar(0x06E7);
    else if (txt[i].unicode() == 0x065B)
      txt[i] = QChar(0x0656);
  //      else if (txt[i].unicode() == 0x065F)
  //        txt[i] = QChar(0x0649);
  //        txt[i] = QChar(0x06F0);

  tbQuranText->setText(txt);
}

void QuranApp::RefreshtbQuranText(int) {
  int index=-1;
  char * ayatext = quran_read_verse(qu, cbsura->currentItem()+1,cbaya->currentItem()+1, ctx);
  char * basmalla_c = quran_read_verse(qu , 1, 1, ctx);
  QString basmalla = QString::fromUtf8(basmalla_c).stripWhiteSpace();
  free(basmalla_c);
  basmalla_c=NULL;
  QString str;
  QString Qayatext = QString::fromUtf8(ayatext);
  Qayatext = Qayatext.stripWhiteSpace();
  free(ayatext);
  ayatext=NULL;
  int para = 0;

  int at = -1;
  //  Qayatext = (QString)("<a name=\"current\">") + Qayatext + QString("</a>");
  //  Qayatext.insert(0,"<a name=\"current\">");
  //  Qayatext.append("</a>");

  while (Qayatext.find('\"') != -1) {
    for (int i=0; i<static_cast<int>(Qayatext.length()); i++) {
      if ( Qayatext.at(i) == '\"' ) {
	at = i;
	break;
      }
    }
    //convert double quotes into html code
    if (at != -1 )
      Qayatext = Qayatext.replace(at, 1, "&quot;");
    at = -1;
  }
  index = -1;
  ayainfo * ayainf = quran_verse_info(cbsura->currentItem()+1, cbaya->currentItem()+1);
  QString num = QString::number(cbaya->currentItem()+1);
  QChar parenleft = '{';
  QChar parenright = '}';
  if (((QString)qu->lang).contains("ar")) {
    for (int i=0; i<static_cast<int>(num.length()); i++) {
      ushort unival = num[i].unicode();
      unival += 0xDFD0;
      num[i] = QChar(unival);
    }
    parenleft = QChar(0xE00A);
    parenright = QChar(0xE00B);
  } 
  if (d_mode == Verse) {
    QString num = QString::number(cbaya->currentItem()+1);
    QChar parenleft = '{';
    QChar parenright = '}';
    if (((QString)qu->lang).contains("ar")) {
      for (int i=0; i<static_cast<int>(num.length()); i++) {
        ushort unival = num[i].unicode();
        unival += 0xDFD0;
        num[i] = QChar(unival);
      }
      parenleft = QChar(0xE00A);
      parenright = QChar(0xE00B);
    }
    char * ayatext_p = quran_read_verse(qu ,cbsura->currentItem()+1,cbaya->currentItem()+1, ctx);
    //tbQuranText->setText(QString::fromUtf8(ayatext_p) + parenleft + num + parenright);
    SettbQuranTextWorkAroundDisplayProblems(QString::fromUtf8(ayatext_p) + parenleft + num + parenright);
    
    free(ayatext_p);
  } else if (d_mode == Page) {
    if (abs(cbaya->currentItem() - currentStartOfPage) >= displayedVerses) {
      currentStartOfPage = cbaya->currentItem() - cbaya->currentItem()%displayedVerses;
      UpdatetbQuranText();
    }
    //take the first paragraph if there is no Basmalla and take the second paragraph if there is Basmalla.
    str = tbQuranText->text(  (cbsura->currentItem()!=0) && (cbsura->currentItem()!=8) && (cbaya->currentItem() < displayedVerses)  );
    if (str.find("<span style=\"color:#ff0000\">") !=-1)
      str = str.remove(str.find("<span style=\"color:#ff0000\">"), 28);
    if (str.find("</span>") !=-1)
      str = str.remove(str.find("</span>"), 7);
    if (str.find("<font color=\"#ff0000\">") !=-1)
      str = str.remove(str.find("<font color=\"#ff0000\">"), 23);
    if (str.find("</font>") !=-1)
      str = str.remove(str.find("</font>"), 7);
    
    if ((QString)qu->lang!="ar") {  //if it's not Arabic
      if (str.find("<center>" + basmalla + "</center>") !=-1)
	str = str.remove(str.find("<center>" + basmalla + "</center>"), QString("<center>" + basmalla + "</center>").length());
    } else  {
      if (str.find("<center>﷽</center>") !=-1)
	str = str.remove(str.find("<center>﷽</center>"), 18);
    }
   
    str = str.stripWhiteSpace();
    if ( (index=str.find(parenleft + num + parenright ,0,TRUE)) != -1 ) {
      str = str.replace(index-Qayatext.length(),
			Qayatext.length()+2+(QString::number(cbaya->currentItem()+1)).length(),
			"<font color=\"#ff0000\">"+ Qayatext + parenleft +num + parenright + "</font>");
    }
    
    //add Basmalla to all chapters except Al-Fateha and Al-Tawba
    if ((cbsura->currentItem()==0) || (cbsura->currentItem()==8) || (cbaya->currentItem() >= displayedVerses))
      //tbQuranText->setText(str);
      SettbQuranTextWorkAroundDisplayProblems(str);
    else {
      para = 1;
      if (cblang->currentItem())  {  //if it's not Arabic use verse one of Al-Fateha as the Basmalla
	//tbQuranText->setText("<center>" + basmalla + "</center>" + str);
	SettbQuranTextWorkAroundDisplayProblems("<center>" + basmalla + "</center>" + str);
      } else {
	//tbQuranText->setText(QString::fromUtf8("<center>﷽</center>") + str);
	SettbQuranTextWorkAroundDisplayProblems(QString::fromUtf8("<center>﷽</center>") + str);
      }
    }

  } else if (d_mode == Chapter) {
    //take the first paragraph if there is no Basmalla and take the second paragraph if there is Basmalla.
    str = tbQuranText->text(  (cbsura->currentItem()!=0) && (cbsura->currentItem()!=8)  );
    if (str.find("<span style=\"color:#ff0000\">") !=-1)
      str = str.remove(str.find("<span style=\"color:#ff0000\">"), 28);
    if (str.find("</span>") !=-1)
      str = str.remove(str.find("</span>"), 7);
    if (str.find("<font color=\"#ff0000\">") !=-1)
      str = str.remove(str.find("<font color=\"#ff0000\">"), 23);
    if (str.find("</font>") !=-1)
      str = str.remove(str.find("</font>"), 7);
    
    if ((QString)qu->lang!="ar") {  //if it's not Arabic
      if (str.find("<center>" + basmalla + "</center>") !=-1)
	str = str.remove(str.find("<center>" + basmalla + "</center>"), QString("<center>" + basmalla + "</center>").length());
    } else  {
      if (str.find("<center>﷽</center>") !=-1)
	str = str.remove(str.find("<center>﷽</center>"), 18);
    }
    
    str = str.stripWhiteSpace();
    if ( (index=str.find(parenleft + num + parenright,0,TRUE)) != -1 ) {
      str = str.replace(index-Qayatext.length(),
			Qayatext.length()+2+(QString::number(cbaya->currentItem()+1)).length(),
			"<font color=\"#ff0000\">"+ Qayatext + parenleft + num + parenright + "</font>");
    }
    
    //add Basmalla to all chapters except Al-Fateha and Al-Tawba
    if ((cbsura->currentItem()==0) || (cbsura->currentItem()==8))
      //tbQuranText->setText(str);
      SettbQuranTextWorkAroundDisplayProblems(str);
    else {
      para = 1;
      if (cblang->currentItem())  {  //if it's not Arabic use verse one of Al-Fateha as the Basmalla
	//tbQuranText->setText("<center>" + basmalla + "</center>" + str);
	SettbQuranTextWorkAroundDisplayProblems("<center>" + basmalla + "</center>" + str);
      } else {
	//tbQuranText->setText(QString::fromUtf8("<center>﷽</center>") + str);
	SettbQuranTextWorkAroundDisplayProblems(QString::fromUtf8("<center>﷽</center>") + str);
      }
    }

  }

  
  //ensure that the start of the verse is visible
  //but only if it's not the first verse of the chapter
  //  if (cbaya->currentItem())
  //    tbQuranText->find("{"+QString::number(cbaya->currentItem()) + "}", TRUE, FALSE, TRUE);
  //ensure that the end of the verse is visible (so the verse is fully visible)
  //  tbQuranText->find("{"+QString::number(cbaya->currentItem()+1) + "}", TRUE, FALSE, TRUE);
  /* in case the verse text is too large for the widget, ensure that at least the
     the start of the verse text is displayed,
     but only if it's not the first verse of the chapter */
  //  if (cbaya->currentItem())
  //    tbQuranText->find("{"+QString::number(cbaya->currentItem()) + "}", TRUE, FALSE, TRUE);
  //  tbQuranText->removeSelection();
  //  tbQuranText->scrollToAnchor("c");
  //int a=0,b=0;
  //tbQuranText->getCursorPosition(&a,&b);
  //qDebug("Cursor is at %d", b);
  //qDebug("%d %d = %d",index,(QString::fromUtf8(Qayatext)).length(), index - (QString::fromUtf8(Qayatext)).length() );

  //ensure that the start of the verse is visible
  tbQuranText->setCursorPosition(para, index - Qayatext.length() );
  tbQuranText->ensureCursorVisible();
  //tbQuranText->ensureVisible(1+((tbQuranText->lineOfChar(1,index)))*tbQuranText->lineWidth(),0);

  //ensure that the end of the verse is visible (so the verse is fully visible)
  tbQuranText->setCursorPosition(para,index+3);
  tbQuranText->ensureCursorVisible();
  /* in case the verse text is too large for the widget, ensure that at least the
     the start of the verse text is displayed */
  tbQuranText->setCursorPosition(para,index - Qayatext.length());
  tbQuranText->ensureCursorVisible();

  str = trUtf8("Chapter ");
  str.append( cbsura->currentText() );
  tlsuran->setText(str);
  tlsuran->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignLeft ) );

  str = trUtf8("Part");
  str.append(" ");
  str.append(" " + QString::number(ayainf->juz_number));
  tljuzzn->setText(str);
  tljuzzn->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );
  str = trUtf8("Hezb");
  str.append(" " + QString::number(ayainf->hezb_number));
  tlhezbn->setText(str);
  if (ayainf->has_sajda)
    tlsajda->show();
  else
    tlsajda->hide();

  free(ayainf);
  ayainf=NULL;
}

bool QuranApp::SeekNextAya() {
  if (cbaya->currentItem() == cbaya->count() - 1 )  {     // it's the last item
    return (SeekNextSura()) ? true: false;
  }
  cbaya->setCurrentItem(cbaya->currentItem()+1);
  if (d_mode == Page)
    if (cbaya->currentItem() % displayedVerses == 0) {
      currentStartOfPage = cbaya->currentItem();
      UpdatetbQuranText();
    }
  
  RefreshtbQuranText(0);
  return true;
}

bool QuranApp::SeekNextSura() {
  if (cbsura->currentItem() == cbsura->count()-1 )
    return false;
  cbsura->setCurrentItem( cbsura->currentItem() + 1 );
  //Refresh the number of items in cbaya
  Refreshcbaya(cbsura->currentItem());
  //Set cbaya->currentItem to the first item
  cbaya->setCurrentItem(0);
  RefreshtbQuranText(0);
  return true;
}

bool QuranApp::SeekPrevAya() {
  if (cbaya->currentItem() == 0)  {
    if ( SeekPrevSura() ) {
      //Set cbaya->currentItem to the last item
      cbaya->setCurrentItem(cbaya->count()-1);
      if (d_mode == Page) {
	currentStartOfPage = cbaya->currentItem() - cbaya->currentItem()%displayedVerses;
	UpdatetbQuranText();
      }
      RefreshtbQuranText(0);
    }
    return false;
  }
  cbaya->setCurrentItem( cbaya->currentItem() - 1 );
  if (d_mode == Page)
    if (cbaya->currentItem() < currentStartOfPage) {
      currentStartOfPage = cbaya->currentItem() - cbaya->currentItem()%displayedVerses;
      UpdatetbQuranText();
    }
  RefreshtbQuranText(0);
  return true;
}

bool QuranApp::SeekPrevSura() {
  if (cbsura->currentItem() == 0)
    return false;
  cbsura->setCurrentItem( cbsura->currentItem() - 1 );
  //Refresh the number of items in cbaya
  Refreshcbaya(cbsura->currentItem());
  //Set cbaya->currentItem to the first item
  cbaya->setCurrentItem(0);
  RefreshtbQuranText(0);
  return true;
}

void QuranApp::Seek(int sura_id, int aya_id)  {
  cbsura->setCurrentItem(sura_id);
  //Refresh the number of items in cbaya
  Refreshcbaya(cbsura->currentItem());
  cbaya->setCurrentItem(aya_id);
  
  currentStartOfPage = aya_id - aya_id%displayedVerses;
  //std::cout << "Start: " << currentStartOfPage<< std::endl;
  UpdatetbQuranText();
    
  RefreshtbQuranText(0);
}

void QuranApp::Search() {
  searchForm *searchF=new searchForm( qu  , ctx);
  //send it the addresses of two integer variables to fill them with the selected result data
  searchF->setAddress(&SearchSura_id, &SearchAya_id);
  searchF->show();
  connect( searchF->ListResults, SIGNAL( selectionChanged(QListViewItem*) ), searchF, SLOT( getSelectedItem() ) );
  connect( searchF->ListResults, SIGNAL( selectionChanged(QListViewItem*) ), this, SLOT( SeekSearchResult() ) );
}

void QuranApp::SeekSearchResult()  {
  Seek( SearchSura_id, SearchAya_id);
}

void QuranApp::languageChanged(int cblangIndex) {

  QString strtmp("");
  cbrecitor->clear();
  for (int counter=0; counter<ctx->libquran.avail_langs[cblangIndex].num_audio_packages; counter++) {
    strtmp = QString::fromUtf8(ctx->libquran.avail_langs[cblangIndex].avail_audio_packages[counter].author);
    if (!strtmp.isEmpty())
      cbrecitor->insertItem(strtmp.stripWhiteSpace());
    else
      cbrecitor->insertItem(trUtf8("Unknown Recitor") );
  }

  if (cbrecitor->count()) {
    btnRead->setEnabled(1);
    btnStop->setEnabled(0);
    cbrecitor->setEnabled(1);
  } else {
    btnRead->setEnabled(0);
    btnStop->setEnabled(0);
    cbrecitor->setEnabled(0);
  }

  cbtext->clear();
  for (int counter=0; counter<ctx->libquran.avail_langs[cblangIndex].num_texts; counter++) {
    strtmp = QString::fromUtf8(ctx->libquran.avail_langs[cblangIndex].avail_texts[counter].author);
    if (!strtmp.isEmpty())
      cbtext->insertItem(strtmp.stripWhiteSpace());
    else
      cbtext->insertItem(trUtf8("Unknown Text") );
  }

  if (cbtext->count()) {
    btnNextAya->setEnabled(1);
    btnNextSura->setEnabled(1);
    btnPrevAya->setEnabled(1);
    btnPrevSura->setEnabled(1);
    cbtext->setEnabled(1);
    cbsura->setEnabled(1);
    cbaya->setEnabled(1);
    btnSearch->setEnabled(1);
    btndisplaymode->setEnabled(1);
    tbQuranText->setEnabled(1);
    textChanged(0);
  } else {
    btnNextAya->setEnabled(0);
    btnNextSura->setEnabled(0);
    btnPrevAya->setEnabled(0);
    btnPrevSura->setEnabled(0);
    cbtext->setEnabled(0);
    btnSearch->setEnabled(0);
    btndisplaymode->setEnabled(0);
    cbsura->setEnabled(0);
    cbaya->setEnabled(0);
    cbsura->clear();
    cbaya->clear();
    tbQuranText->clear();
    tbQuranText->setEnabled(0);
    if (qu)
      quran_close(qu, ctx);
    qu = NULL;
  }
}

void QuranApp::textChanged(int cbtextIndex) {

  int tempaya=0;
  int tempsura=0;

  if (qu)
    quran_close(qu, ctx);
  //use the font 'ArabeyesQr' for Arabic
  QFont tbQuranText_font(  tbQuranText->font() );
  tbQuranText_font.setFamily( "ArabeyesQr" );
  tbQuranText_font.setPointSize( 30 );
  if (ctx->libquran.avail_langs[cblang->currentItem()].code != (QString)"ar")  { //if not arabic, then use the default font
    tbQuranText_font.setFamily( QFont().family());
    tbQuranText_font.setPointSize( 13 );
  }
  tbQuranText->setFont( tbQuranText_font );

  qu=NULL;
  qu = quran_open_index(cblang->currentItem(), cbtextIndex, ctx);
  if (qu == NULL) {
    qFatal("Error calling quran_open: %d", ctx->error_code);
    languageChanged(0);
    return;
  }
  tempsura = cbsura->currentItem();
  tempaya = cbaya->currentItem();
  cbsura->clear();
  for (int i=0; i<114; i++)
    cbsura->insertItem( QString::fromUtf8((QString)qu->sura_titles[i]) );
  cbsura->setCurrentItem(tempsura);
  tbQuranText->clear();
  Refreshcbaya(cbsura->currentItem());
  cbaya->setCurrentItem(tempaya);
  RefreshtbQuranText(0);
}

void QuranApp::tbQuranTextClicked(int para, int pos) {
  if ( d_mode==Verse)
    return;
  if (d_mode == Chapter && para==0 && (cbsura->currentItem()!=0) && (cbsura->currentItem()!=8))
    return;
  if (d_mode == Page && para==0 && (cbsura->currentItem()!=0) && (cbsura->currentItem()!=8) && (cbaya->currentItem()<displayedVerses))
    return;
  if ( ayaPos.isEmpty()) {
    qWarning("ayaPos is empty!");
    return;
  }
  QValueList<int>::iterator it;
  int count=0;
  for (it = ayaPos.begin(); it != ayaPos.end(); ++it) {
    if (pos<=*it) {
      cbaya->setCurrentItem((d_mode==Chapter)?count:currentStartOfPage+count);
      RefreshtbQuranText(0);
      return;
    }
    count++;
  }
  return;
}

void QuranApp::switchDisplayMode() {
  if (d_mode==Verse)
    d_mode=Chapter;
  else if (d_mode==Chapter)
    d_mode=Page;
  else
    d_mode=Verse;
  int currentaya=cbaya->currentItem();
  Refreshcbaya(cbsura->currentItem());
  cbaya->setCurrentItem(currentaya);
  RefreshtbQuranText(0);
}

void QuranApp::About() {
  QMessageBox::about(this,trUtf8("The Holy Qur'an..."),
		     trUtf8("The Holy Qur'an Program\n Version ") + VERSION + trUtf8("\n(c) Copyright 2002-2005 under the GPL license, Arabeyes http://www.arabeyes.org\nby Mohammed Yousif <mhdyousif@gmx.net>") );
}

void QuranApp::exit()  {
  qApp->quit();
}

void QuranApp::exit(int code = 0)  {
  qApp->exit(code);
}
int QuranApp::getCurrentLang() {
  return cblang->currentItem();
}

int QuranApp::getCurrentRecitor() {
  return cbrecitor->currentItem();
}

int QuranApp::getCurrentSura() {
  return cbsura->currentItem();
}

int QuranApp::getCurrentAya() {
  return cbaya->currentItem();
}

int QuranApp::getNumberOfAyas() {
  return cbaya->count();
}

libquran_ctx * QuranApp::getContext() {
  return ctx;
}

void QuranApp::setSettings(Settings & new_settings) {
  settings = new_settings;
  settings.ReadSettings();
  athread.audioPlugin = settings.audioPlugin;
}
