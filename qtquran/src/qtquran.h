/***************************************************************************
$Id$
                          qtquran.h  -  declaration for QuranApp, audioThread
                             -------------------
    begin                : Fri Jul 12 15:53:41 EEST 2002
    copyright            :  (C) Copyright 2002, Arabeyes, Mohammed Yousif
    email                : mhdyousif@gmx.net

$Date$
$Author$
$Revision$
$Source$ 
***************************************************************************/

/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify                              *
 *   it under the terms of the GNU General Public License as published by                           *
 *   the Free Software Foundation; either version 2 of the License, or                                  *
 *   (at your option) any later version.                                                                              *
 *                                                                                                                                *
 ***************************************************************************/

#ifndef QTQURAN_H
#define QTQURAN_H

#include <qvariant.h>
//#include <qmainwindow.h>
#include <quran.h>
#include <qwidget.h>
#include <qvaluelist.h>
#include <qpixmap.h>

// application specific includes
#include "audiothread.h"
#include "settings.h"
#include "mainwindow.h"

/**
 * This Class is the base class for your application. It sets up the main
 * window and providing a menubar, toolbar
 * and statusbar. For the main view, an instance of class QuranView is
 * created which creates your view.
 */

class QuranApp : public mainwindow
{
  Q_OBJECT

    public:
  QuranApp(QWidget* parent=0, const char *name=0);
  ~QuranApp();
  void setSettings(Settings &);


  public slots:
    void Read();
  void Refreshcbaya(int);
  void RefreshtbQuranText(int);
  void UpdatetbQuranText() ;
  void Stop();
  void About();
  bool SeekNextAya();
  bool SeekNextSura();
  bool SeekPrevAya();
  bool SeekPrevSura();
  void SeekSearchResult();
  void Search();
  void languageChanged(int);
  void textChanged(int);
  void EnableBtnRead();
  void tbQuranTextClicked(int, int);
  void switchDisplayMode();
  void exit();
  void exit(int);
  int getCurrentLang();
  int getCurrentRecitor();
  int getCurrentSura();
  int getCurrentAya();
  int getNumberOfAyas();
  void SettbQuranTextWorkAroundDisplayProblems(QString);
  libquran_ctx * getContext();
  
  bool valid;

 private:

  quran * qu;
  QString * SuraName;
  QString QuranLang;
  QValueList<int> ayaPos;
  libquran_ctx * ctx;
  audioThread athread;
  enum DisplayMode { Verse, Chapter, Page };
  DisplayMode d_mode;
  int displayedVerses;
  int currentStartOfPage;
    
  Settings settings;

  QPixmap pixtopleftcorner;
  QPixmap pixtoprightcorner;
  QPixmap pixbottomleftcorner;
  QPixmap pixbottomrightcorner;
  QPixmap pixleftframe;
  QPixmap pixrightframe;
  QPixmap pixtopframe;
  QPixmap pixbottomframe;

  //For Search Results
  int SearchSura_id;
  int SearchAya_id;

  QString GetAyaText(int ,int);
  void Seek(int ,int );
};

#endif // QURAN_H
