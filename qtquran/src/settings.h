/************************************************************************
 * $Id$                                                                                                                 *
 *                                                                                                                          *
 * ------------                                                                                                          *
 * Description:                                                                                                   *
 * ------------                                                                                                          *
 *  Copyright (c) 2002-2005, Arabeyes, Mohammed Yousif                       *
 *                                                                                                                          *
 *  settings.h                                                                                                       *
 *  For Reading/Saving program settings                                                      *
 *                                                                                                                          *
 * -----------------                                                                                                    *
 * Revision Details:    (Updated by Revision Control System)                     *
 * -----------------                                                                                                    *
 *  $Date$                                                                                                           *
 *  $Author$                                                                                                        *
 *  $Revision$                                                                                                     *
 *  $Source$                                                                                                       *
 *                                                                                                                          *
 ************************************************************************/
 
/***************************************************************************
 *                                                                                                                                *
 *   This program is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or          *
 *   (at your option) any later version.                                                                 *
 *                                                                                                                                *
 ***************************************************************************/
 
#ifndef SETTINGS_H
#define SETTINGS_H

#include <qstring.h>

class Settings {
    public:
      Settings();
      bool ReadSettings();
      bool SaveSettings();
      
      QString audioPlugin;
    
    protected:
      void setDefaultAudioPlugin();
      void setDefaultSettings();
      QString confFile;

};

#endif // SETTINGS_H
